
//#include <stddef.h>

//#include <string.h>

#include <stddef.h>
#include <string.h>
//#include <stdbool.h>

#include "App.h"
#include "GenericTCPClient.h"
#include "APL_Fun_TCP.h"
#include "API_MW.h"
#include "APL_Fun_MW.h"

//#include "MIWI.h"  se encuentra en APL_FUN
//#include "APLICACION.h"

/*    Variables de estados y semaforos de Aplicacion */
 BOOL Nw_Frm_ASO =   FALSE;
 BOOL ALRDY_Frm_ASO= FALSE;
 BOOL Nw_Frm_CTRL_MW=FALSE;
 BOOL Nw_Frm_ASO_PN= FALSE;

 BOOL Buff_NRM_MW;                     //bandera buffer rx en capacidad de transmision supera limite para envios modo Normal
 BOOL Buff_RTCC_MW;
 BOOL Buff_CTRL_MW;
 BOOL Buff_CTRL_TCP;
 BOOL Buff_ASO_PN_MB;
 BOOL Buff_ASO_PN_TCP; 
/*    Semaforos                                     */
 BOOL Sem_TCP_DONE;                   //bandera de APP rx en capacidad de transmision 1 indica libre- 0 ocupado
 BOOL Sem_MB_DONE;                    //bandera de APP rx en capacidad de transmision 1 indica libre- 0 ocupado
 BOOL Sem_MIWI_DONE;                  //bandera que indica el estado de tramiento del paquete recibido  1 indica libre(resultado)- 0 ocupado(tramiento))
 BOOL Sem_ModBus_Free;                //bandera de Modbus libre
 BOOL Sem_Free_App;                   //bandera de APP regis
 BOOL Sem_Free_App1;                  //bandera de APP1 regis
 BOOL Sem_Tx_CTRL_MB;   
 BOOL Sem_Resp_CTRL_MB;
 
 
 /*----Buffer y bases---------------*/

 APP_t_Rdcd_Complet Buffer_Nrm;                   //buffer Recepcion MIWI, por lo gral tramas Norm tipo 1

 WORD               Base_Slot[BASE_SLT_LNGTH][2]; //[ID][NEW-Slot] buffer Recepcion MIWI para cambioos de slot

 Gen_Buffer_t       Buffer_Aso[BUFFER_LNGTH];     //buffer asociacion recepcion-transmision tcp-ip 

 Gen_Buffer_t       Buffer_PN[N_PAN];             //buffer asociacion PAN 
 
 Ctrl_Buffer_t      Ctrl_Buffer_Q[N_PAN];         //buffer Recepcion QUERY tramas TIPO Control gral

 Ctrl_Buffer_t      Ctrl_Buffer_R[N_PAN];         //buffer Transmision RESPONStramas TIPO Control gral

 APP_TK_t           TK_Buffer_Nrm[N_PAN];         //beffer token tramas normales

 Table_ID_AddMB_t   Table_ID_AddMB;               //Tabla de ID vs ADDmodbus, dispositivos escalvos
//----------------------------------
 
 
/******************************************************************************
 *Function:
 *  void Serv_App
 * Summary:
 *   programa principal en base de estados 
 *
 * Description:
 *	Controla el flujo de programa segun los estados que se encuentran cada servicio
 * modbus, miwi y tcp
 * 
 *
 *  Precondition:
 *   Requiere biblioteca Modbus.h 
 *
 * Parameters:
 *  None
 *
 * Returns:
 *   None 
 ******************************************************************************/
void Serv_APP(void){
    
    BYTE *p= NULL;
    BYTE *p1=NULL;
    BYTE  i;  
    static BOOL Chk_Prty_Avbl= FALSE;/*iniciada en cero*/
    static BOOL Actv_Frm_Aso=  FALSE;/*iniciada en cero*/
    //static bool Nw_Frm_ASO =   FALSE;
    //static bool Nw_Frm_CTRL=   FALSE;

    static BOOL init=           FALSE;
    static BOOL Q_CTRL_TCP=     FALSE;
    static BOOL st=             FALSE;
    static BOOL ALRDY_Frm_ASO=  FALSE;
    //static BOOL Nw_Frm_ASO   =  FALSE;
    static BOOL sm=             FALSE;
    
    
    //---------------------------------------------
    //-----------------TCP_IP---------------------
    //---------------------------------------------    
    // Instancias:
    //Ingreso cuando el buffer esta lleno, cuando hay tramas
    // de time y asociacion estas ultimas tienen prioridad
    //( Buff_Rx_MiWi |(App1.Tipo==2)||(App1.Tipo==3) )| & 
    
    if (!init)
        /* En caso de primer incio verifico si es por falla o primer asociacion */
        if (Set_From_NVM())
             init= TRUE;       
    
    if ( !Sem_TCP_DONE )
    {             
    //tiene que haber un semaforo que indique el fin DONE de TCP para que no evolucione o ingrese aqui
    // Debe ser llamada luego que se identifica que tipo de Paquete enviar
    // Identico si hay necesidad de transmitir, estados permanentes internos para seguir
    /*leo buffer de recepcion miwi e untercambio las variables a App*/        
    //------------------------- STACK    
        if (!Chk_Prty_Avbl)
        {                        
            if ( Proc_First_TCP(init) )                  //tiene que haber datos de MB
            {
                Chk_Prty_Avbl= TRUE;  
                PASS.Sec= GW_Sec_TCP;
                Set_App_From_APP_TCP();
            }            
            
        }
        else //(st==1)
        {                
            switch (TCP_Client())
            {
                case DONE_st:
                    // seteado App.Tipo puedo enviar consula al servidor luego en DONE la RESPUESTA ESTA AMACENADA EN APP1                                                  
                    if( ((App.Tipo == App.Tipo_st)&&App.Tipo) ||       // filtro secuencia y respuesta !nOp
                        (!App.Tipo_st && (App.Tipo == OP_CONTROL)) ||
                        (!App.Tipo && (App.Tipo_st == OP_CONTROL)) )   // De ser nOp inicial en GW el serv solicite algo
                    {                                            
                        if (App.Tipo == OP_CONTROL)                    // Solicitud por Srv
                            Q_CTRL_TCP= TRUE;
                        
                        if (!App.Tipo && (App.Tipo_st == OP_CONTROL)) 
                            App.Tipo= App.Tipo_st;                     // estados previo: Permite entrar al lazo solo hubo por MB
                        
                        Sem_TCP_DONE = TRUE;
                        Chk_Prty_Avbl= FALSE;
                    }
                    else
                        Chk_Prty_Avbl= FALSE;
                  break;
                  
                case ERR_st:
                        Chk_Prty_Avbl= FALSE;
                  break;
                  
                default :
                    Chk_Prty_Avbl= TRUE;                        
            }
        }
    }
    else 
        if(Sem_TCP_DONE)
        {   //Sem_TCP_DONE 1: libre(servicio anterior efectuado listo para transmitir u 
            //operar sobre los datos recibidos) 0:ocupado(endesarrollo)
            /*Entrar aqui indica que se todos los datos del servicio TCPIP son validos
             * Seguido de procesa los datos recibidos y se efectuan las operaciones correspondientes
             * segun sea necesario para comunicarlas a los servicios Miwi o para operar sorbe el GateWay
             */
            /*las variables a App pasan el buffer de TX miwi */
            //------------------------- APP--------------------------        
            switch (App.Tipo)
            {   //informacion desde TCP-IP a MIWI
                case OP_NORMAL:         //-----------NORMAL 
                    /*se enviaron recibieron los datos envidos de los medidores al servidor remoto*/
                    // incremento la secuencia nada mas
                    // Envia el buffer(n) y ahora limpio el (n+1)
                    // limpio el bufferRX, solo aquellos enviados el resto se hubica en cola.
                    Clear_Buffer_Norm_TCP();
                    Put_TK_BufferTK_TCP();
                    Buff_NRM_MW= FALSE;
                    
                break;
                case OP_ASOCIACION:     //-----------ASOCIACION
                    /*Se asocio el medidor al servidor. llegaron los datos para 
                     * enviar al medidor*/
                    //Recibo ID que envia el servidor
                    Put_Buffer_Aso_TCP();
                    
                break;
                                
                case OP_TIME:           //-----------TIME
                    /*El servidor respondio ante la solicitud de ajuste de RTCC 
                     * local llegaron los datos para el GW*/
                    //---------------Paso de App.DateTx => GW_RTCC;
                    
                    RTCC_Set();                        // actualiza el RTCC                    
                    
                break;
                
                case OP_ASOCIACION_PAN: //-----------Asociacion PAN
                    
                    Put_Buffer_PN_TCP();
                                                       
                break;                
                            
                case OP_CONTROL:        //-----------CONTROL
                    /*El servidor solicito ajuste de slots medidor*/ 
                    //Discrimino si es que viene desde una transmision o recepcion                                          
                    //@TODO ( PARA VERSION PORSTERIOR ) IMPLEMENTAR esta seccion de codigo segun el ID colocar en buffer correspondiente
                    if ( Buff_CTRL_MW )
                    {   //Hubo trama desde MB debo actualizar el buffer
                        // borro Trama MB enviada al GW por PN
                        if ( !UpDate_Buffer_Ctrl_MW(&Ctrl_Buffer_R[Indx_from_IdPan( highByte(App.ID) )] ) )
                            Buff_CTRL_MW= FALSE;               // Ya no quedan tramas CTRL desde MB
                    } 
                    
                    if(Q_CTRL_TCP)                              // Arriba Trama desde Srv
                    {    
                        if ( (App.ID == PASS.Id_GW.Val) && (PASS.TK_GW.Val == App.TKGW) ) 
                        {                                       // El control es para GW    
                            APP_Ctrl_TCP();
                        }else                                   // EL control es para otro
                        {                                       // arrib� trama desde TCP, debo escribir                             
                            Put_Buffer_Ctrl_TCP();               //Retorno Index                            
                            Buff_CTRL_TCP= TRUE;                // indico a MB_APP arribado de trama desde Srv
                        }  
                        Q_CTRL_TCP= FALSE;
                    }    
                break; 
                
                case OP_ASOCIACION_GW:  //-----------Asociacion GW
                    /*Se solicito una asociacion de GW llegaron los datos para el GW*/ 
                    PASS.Id_GW.Val = App.ID_GW_AUT;    //ID de asociacion al servidor
                    PASS.TK_SRV.Val= App.TK_SrvGw;     //Llave de asociacion fija inicial a APP servidor
                                                        //Se guarda MNV por reboot                    
                    strcpy(PASS.Pass_SRV,App.Pass_SRV); //Llave Token dinamica  
                    Set_ASO_NVM();                      //salvo variables MNV
                    init= TRUE;
                    
                break;
            }            
            App.Tipo=     nOP;
            Sem_Free_App= TRUE; // libero el recurso del registro             
            Sem_TCP_DONE= FALSE;// puedo entrar al servicio de escucha tcp
            GW_Sec_TCP++;
        }
    //-------------------------------------------------------------------
    //---------------------MIWI-LOCAL------------------------------------ 
    //-------------------------------------------------------------------
            
    if(!Sem_MIWI_DONE)
    {   //ingresa siempre aqui, verifico estado del buffersMiWi
    //------------------------- STACK -------------------------------------                   
        if (Proc_Req_First_MW(Serv_MW(RECB))) //Escucho si hay tramas nuevas en bufferMiWi devuelvo App1.#                                                       
            Sem_MIWI_DONE= TRUE;        
    }
    else 
        if(Sem_MIWI_DONE && App1.Tipo)
        { 
        //------------------------- APP---------------------------------------
            switch (App1.Tipo)
            {  //informacion desde MIWI a ModBus       
                case OP_NORMAL: //-----------NORMAL
                    //-----------COLOCO LAS TRAMAS RECIBIDAS EN EL BUFFER    
                    if (Put_Buffer_Nrm_MW() > ALRM_BFFR)
                        Buff_NRM_MW= TRUE;    //alarma que indica a tcp-ip que debe transmitir este buffer 
                    //-----------REQUIERO RESPONDER AL MEDIDOR LA LLEGADA DE LOS DATOS CON EL 
                    ///SETEO DEL RTCC DEL MEDIDOR 
                    //-----------Y DE SLOT DE SER REQUERIDO VERIFICO EN UNA LISTA DE ID-SLOT
                    if (Resp_MIWI_NORM_MW())
                    {
                        App1.Tipo= nOP;
                        Sem_Free_App1= TRUE;// libero el recurso del registro 
                        Sem_MIWI_DONE= FALSE; //TOMO EL RECURSO, ASEGURO UN NUEVO PROCESO
                    }
                    // reserve el recurso ahora debo transmitir por miwi seleccionando el estado segun App1.tipo
                break;
                case OP_ASOCIACION: //---------- ASOCIACION
                // elevo la trama directamente a ModBuss por ser asociacion.
                // Todo App1.# tiene la informacion para ello
                // Esto va en el buffer    
                    if (Nw_Frm_ASO)
                    {
                        Nw_Frm_ASO= FALSE;
                        Put_Buffer_Aso_MW(); // coloco trama nueva en buffer, luego APPMB Verifica
                        //no modifico Sem_MIWI_DONE ni tipo la proxima iteracion entro directo a este caso
                    }                     
                    if (Chk_Frm_Rx_ASO_Buffer_MW()) 
                     // existe una trama previa
                        if(Resp_MIWI_ASOC_MW())
                        {// servicio de respuesta si hay trama de respuesta
                            App1.Tipo=     nOP;
                            Sem_Free_App1= TRUE;  //libero el recurso del registro 
                            Sem_MIWI_DONE= FALSE; //libero EL RECURSO, ASEGURO UN NUEVO PROCESO
                        }                                 
                break;
               
                case OP_TIME: //-----------TIME
                    
                    Put_Buffer_TIME_MW();
                    
                break;
                
                case OP_CONTROL: //-----------CONTROL
                    /*La trama MIWI App1 se coloca en buffer 
                     Si es Buff_CTRL_MB puedo discriminar si es por MIWI o MB*/
                    //@TODO MODIFICAR LO Q SEA PARA QUE NO ENVIE LA TRAMA CTRL SI EL DISPOSITOVO ES RFD O EN SLEEP
                    //DEBERIA GUARDAR EN MEMORIA SST LA LISTA DE DISPOSITIVOS CAPACES DE MANER TRAMAS CTRL SIN SLOT
                    //ADEMAS AGREGAR LA BASE DE DATOS DE ID COMO KEY Y DATOS AddSHORT O Addlong para indi o direct
                    
                    // Discrimino para que no acapare el Medio
                    if (Nw_Frm_CTRL_MW)
                    {   //arribo trama desde MIWI
                        Put_Buffer_Ctrl_MW(&Ctrl_Buffer_R[LCL_MiWi],&App1);
                        Resp_MIWI_CTRL_MW(&Ctrl_Buffer_Q[LCL_MiWi],&App1);                                     // Respondo a lo llegado desde MIWI
                        Buff_CTRL_MW=   TRUE;                                   //indico a MB arribo de trama
                        Nw_Frm_CTRL_MW= FALSE;
                    }
                    if (Buff_CTRL_TCP)
                    {   //arribo trama desde TCP
                        if (Tx_MIWI_CTRL_MW(&Ctrl_Buffer_R[LCL_MiWi],&App1))                                   //transmito lo que haya llegado de MB a redMIWI Solo si nOP                        
                            if (!UpDate_Buffer_Ctrl_MW(&Ctrl_Buffer_Q[LCL_MiWi])) //ajusto el enviado incremento el indice                            
                                Buff_CTRL_TCP= FALSE;                            //Ya no quedan tramas CTRL desde MB
                    }                                           

                break;    
                
            }
             GW_Sec_MW++;
            //App1.Tipo=0;
            //Sem_Free_App1=true;// libero el recurso del registro 
            //Sem_MIWI_DONE=false; //TOMO EL RECURSO, ASEGURO UN NUEVO PROCESO
        }
            
        
}//end APP_SERV
