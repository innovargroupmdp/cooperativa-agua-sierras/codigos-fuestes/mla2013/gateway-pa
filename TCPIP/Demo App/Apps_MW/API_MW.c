
#include <string.h>
#include <stdio.h>

//#include "system.h"
//#include "gtypedefs.h"

//#include "symbol.h"
//#include "delay.h"
#include "GenericTypeDefs.h"
#include "MiWi.h"
//variables globales
#include "App.h"
//App que intercambia tramas modbus para presentar a Miwi 
//#include "APP_MIWI_ModBus.h"
//servidor maestro modbus que se encarga de establecer la 
//comunicacion entre los diferentes dispositivos de la red modbus
//#include "Modbus_S.h"
//Se encarga de establecer la conexion administras las secuencias entre PAM y el gateWay

//#include "MAC_EEProm.h"

#include "API_MW.h"
#include "APL_Fun_MW.h"
#include "MCHP_API.h"

//solo por las define de OP_

void Ini_MW(void)
{
    //myDeviceType=DeviceCoordinator;
    
    //HardwareInit();
    InitSymbolTimer();

    /*******************************************************************/
    //  Almacena la direccion EUI en  myLongAddress
    /*******************************************************************/
    //@TODO Completar esta funcion
    //Read_MAC_Address();//MODIFICAR PARA QUE SE ESCRIBE  DESDE CONFAPP
          
    //MiApp_SetAddressPan(myAddr.bytes, 0x5533);
    /*indico si quiero recuperar red desde EEPROM*/
    MiApp_ProtocolInit(FALSE);
    MiApp_SetChannel(MW_CHANNEL);
    MiApp_ConnectionMode(ENABLE_ALL_CONN);
    /*******************************************************************/
    // Create a New Network
    /*******************************************************************/
    MiApp_StartConnection(START_CONN_DIRECT,0x03800000,6);
    //MiApp_InitSleepRFDBuffers(sleepRFDData,sizeof(sleepRFDData),10);//Matches sleepRFDData[] declaration
    //Enable network level security (does not impact User Level Security)
    //MiApp_SetNetworkSecure(FALSE);
           
    /*******************************************************************/
    // Wait for a Node to Join Network then proceed to Demo's
    /*******************************************************************/               
    
    while(!ConnectionTable[0].status.bits.isValid)
    {
        if(MiApp_MessageAvailable())
            MiApp_DiscardMessage();
    }    
	        
}    

BOOL FunRx_MW(void)
{     
    //uint8_t *p;    
    //DIRECCION ID O MAC en la asociacion
    //llama al main de ModbusM la secuencia es:
    
    /**************************************************************************
     * primero: consulta
      segundo: llegada y espera de datos
      Tercero: recepcion
      cuarto: presentacion de buffer modbus a capa superior miwi almaceno N tramas entrantes
      quinto: Trasmitir datos de capa superio
      Sexto: escucchar recepcion de ack
    ****************************************************************************/  
    if(MiApp_MessageAvailable())
    {        
        //DIRECCION ID O MAC en la asociacion
        //El primer  dato 0 es reservado
        
        if (Set_App1_From_MW())
        {    
            MiApp_DiscardMessage();            
            return(TRUE);
        }        
    }    
  
    MiApp_DiscardMessage();                                // solo con fines de iniciacion Elimina dato de iniciacion de request
    App1.Tipo= nOP;
    App1.ID=   0;
    return(FALSE);                      
    
}
//------------------------------------------------------------------------------
/*!
  Function:
	BOOL FunTx(void)

 @brief  Description:
	Writes a single byte to a TCP socket.

  Precondition:
	TCP is initialized.

  Parameters:
 @param c1 the first argument.
 @param c2 the second argument.
	hTCP - The socket to which data is to be written.
	byte - The byte to write.
 @return
 Return Values:
	TRUE - The byte was written to the transmit buffer.
	FALSE - The transmit buffer was full, or the socket is not connected.
*/
//------------------------------------------------------------------------------
BOOL FunTx_MW(void)
{
    BYTE *p,i;
    WORD A;
    
    //ROM addr_t coord={0};
    // AddMIWI direccion de destino
    //memset(AddMIWI,AddMIWI,sizeof(AddMIWI));

    // TAREA REVISAR SI SIMPLIFIC TIPO
    
    MiApp_FlushTx();
    MiApp_WriteData(mUserDataType);
    /*******************************************************-HEADER-*/    
    MiApp_WriteData(highByte(App1.ID ));                    // Identificador hacia el mimia
    MiApp_WriteData(lowByte(App1.ID));                      
    /*-DATA OF LAYER-*/
    MiApp_WriteData((BYTE)App1.Tipo);
     
  
        
    switch (App1.Tipo) 
    {    //indica longitud total de payload MB      
        case nOP:
            /*------------TRAMA CORTA UNICO  ENVIO----------------------------*/                                           
                            
        break;    
        case OP_NORMAL:
            
            /*------------TRAMA CORTA UNICO  ENVIO----------------------------*/ 
            /************************************************-DATA OF LAYER---*/            
            MiApp_WriteData(highByte(App1.TOKEN));                  // Proximo tk hacia el serv
            MiApp_WriteData(lowByte(App1.TOKEN));                   //    
            MiApp_WriteData(App1.Secuencia);                        // Secuencia para validar al mimia
            MiApp_WriteData(highByte(App1.Slot));
            MiApp_WriteData(lowByte(App1.Slot));
            p= &App1.DateTx.Seg;

            for(i=0;i<3;i++)    
                MiApp_WriteData(*(p++));    //proceso primer paquete X bytes en total el buffer
                        
        break;  
        
        case OP_ASOCIACION:  //tanto FFD , monitores   
            /************************************************-DATA OF LAYER---*/            
            MiApp_WriteData(highByte(App1.TOKEN));                  // Proximo tk hacia el serv
            MiApp_WriteData(lowByte(App1.TOKEN));                   //    
            MiApp_WriteData(App1.Secuencia);                        // Secuencia para validar al mimia
            MiApp_WriteData(highByte(App1.Slot));
            MiApp_WriteData(lowByte(App1.Slot));
            p= &App1.DateTx.Seg;                        
            for(i=0;i<6;i++)                
                MiApp_WriteData(*(p++));    //proceso primer paquete X bytes en total el buffer
                
            /*-DATA Of APPLICATION-*/                
            MiApp_WriteData(highByte(App1.ID_AUT));             // identificacion de asociacion
            MiApp_WriteData(lowByte(App1.ID_AUT));
            
        break;
        
        case OP_TIME:
            /************************************************-DATA OF LAYER---*/ 
            MiApp_WriteData(highByte(App1.TKMM));                  // Proximo tk hacia el serv
            MiApp_WriteData(lowByte(App1.TKMM));                   //    
            MiApp_WriteData(App1.Secuencia);                        // Secuencia para validar al mimia
            p= &App1.DateTx.Seg;
            for(i=0;i<6;i++)    
                MiApp_WriteData(*(p++));    //proceso primer paquete X bytes en total el buffer

        break;
        
        case OP_ASOCIACION_PAN: //SOLO COOR-PAN
                
                return (FALSE);
             
        break; 
        
        case OP_CONTROL:
            /************************************************-DATA OF LAYER---*/ 
                /*-DATA Of APPLICATION-*/ 
            MiApp_WriteData(highByte(App1.TOKEN));                  // Proximo tk hacia el serv
            MiApp_WriteData(lowByte(App1.TOKEN));                   //    
            MiApp_WriteData(highByte(App1.TKMM));               // token hacia el MIMIA desde Srv
            MiApp_WriteData(lowByte(App1.TKMM));
            p= &App1.SubTipo;                           
            //MiApp_WriteData((BYTE)App1.SubTipo);
            //MiApp_WriteData((BYTE)App1.DatCtrl.Index);
            for (i=0;i<=(App1.DatCtrl.Index+2);i++)
                MiApp_WriteData(*p++);//<-(20)              
                
        break; 
        
        default:
            return (FALSE);
    }      
    
    if (App1.PermanentAdd)
    
        if(MiApp_UnicastAddress(App1.Add, App1.PermanentAdd, FALSE)) //si se requiere Long short Add o si es Drecto o indirecto
            
            return(TRUE);
    
    return(FALSE);
     
}

/*****************************************************************************
*  Function:
*	BOOL Serv_MIWI(void)
*
*  Description:
*	Writes a single byte to a TCP socket.
*
*  Precondition:
*	TCP is initialized.
*
*  Parameters:
* @param c1 the first argument.
* @param c2 the second argument.
*	hTCP - The socket to which data is to be written.
*	byte - The byte to write.
* @return
* Return Values:
*	TRUE - The byte was written to the transmit buffer.
*	FALSE - The transmit buffer was full, or the socket is not connected.
*****************************************************************************/
BOOL Serv_MW(MIWI_State_t State){ // DA EVOLUVION AL STACK
    /*--------------Almaceno el ADDRES--------------------------------*/
    // Deberia saber a quien consulto en primer instancia solo un esclavo de haber mas es un broadcast y luego escuchar respuestas aleatorias diferidas
    // uso una ADD modbus valida de la PAN 0000 sin uso
    // 01:master; 02:PAN; 
    // A0:LORA0; 
    // B0:plc0; B1:plc1 ,B2:plc2
    // memcpy(AddMIWI,rxMessage[0].Adress,Length_Add); 
    // Cx:libre 
    /*--------------extraigo campos de la trama-----------------------*/
    switch(State){ 
        case RECB:
            /*-----Consulto a MIWI y almaceno las tramas segun corresponda----*/            
            if(FunRx_MW()){ //debe haber un estado interno y luego escucho dentro de funrx

                //Caso de transmicion y recepcion completas
                //Sem_MIWI_DONE=TRUE; //tomo el recurso para operarlo

                return(TRUE);
            }                           
        break;
        case TRANS_W:
            //TRANSMITE LOS DATOS DIRECTO DEL PAN AL COOR ENTONCES DEBO EN FUNTX COLOCAR LA TRAMA MIWI EN MODBUS
            //transmito tramas de consulta de datos en el PAN, pueden ser varias o en secuencias segun la aplicacion.
            if(FunTx_MW())
            //proximo estado
            //   State=RECB;
                return(TRUE);
            
        break;    
    }
    return(FALSE);
}