/* 
 * File:   MIWI.h
 * Author: cba
 *
 * Created on April 9, 2018, 9:52 AM
 */

#ifndef MIWI_H
#define	MIWI_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* MIWI_H */
/************************ HEADERS **********************************/

/************************ DEFINITIONS ******************************/
#define VER_MAJOR  2
#define VER_MINOR  0

/************************ DATA TYPE *******************************/
typedef enum  MIWI_State_t_Def{
    RECB=0,
    TRANS_W
}MIWI_State_t; // estado inicial al inicio
/************************ EXTERNAL VARIABLES **********************/


/***********************Declaracion de Prototipos******************/
void Ini_MW(void);
BOOL FunRx_MW(void);
BOOL FunTx_MW(void);
BOOL Serv_MW(MIWI_State_t);
