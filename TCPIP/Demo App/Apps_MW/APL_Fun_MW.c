/*
 * File:   APL_Fun.c
 * Author: cba
 *
 * Created on 23 de mayo de 2018, 11:39
 */


#include <stddef.h>
#include <string.h>

#include "GenericTypeDefs.h"
#include "../App.h"
#include "WirelessProtocols/MCHP_API.h"
#include "API_MW.h"
#include "APL_Fun_MW.h"  // funciones especificas
#include "APL_Fun_TCP.h" // funciones genericas

//#include "APP_MIWI_ModBus.h"
//#include "../modbus_App.h"

extern BOOL Nw_Frm_ASO;                
extern BOOL Nw_Frm_CTRL_MW;

//APP_t_Rdcd_Complet Buffer_Nrm;

//@todo Blanqueo de los buffer.

/*******************************************************************************
 * Funcion que setea la variable App1 desde RX MIWI,
 * 
 ******************************************************************************/
BOOL Set_App1_From_MW(void){
        
    BYTE i;
        
    /*-HEADER-*/
    App1.ID=bytesToWord(rxMessage.Payload[1],rxMessage.Payload[2]);    //bytesToWord
    /*-DATA OF LAYER-*/ 
    App1.Tipo=rxMessage.Payload[3];          

    switch(App1.Tipo)
    {   
        case OP_NORMAL:
            /* Caso que haya trama LARGA valida  recibida modbus*/
            //App1.ID=    bytesToWord( Rx_Buffer_MODMIWI.Message[k].Payload[4],Rx_Buffer_MODMIWI.Message[k].Payload[5]);
            App1.TOKEN= bytesToWord( rxMessage.Payload[4],rxMessage.Payload[5]);
            App1.Secuencia=    rxMessage.Payload[6]; 
            App1.Nro_intentos= rxMessage.Payload[7];
            App1.Noise=        rxMessage.Payload[8];
            App1.Signal=       rxMessage.Payload[9];
            App1.Bat=          rxMessage.Payload[10];
            App1.Slot=  bytesToWord(rxMessage.Payload[1],rxMessage.Payload[12]);              
            App1.DateTx.Seg=   rxMessage.Payload[13];
            App1.DateTx.Min=   rxMessage.Payload[14];
            App1.DateTx.Hou=   rxMessage.Payload[15];
            /*-DATA Of APPLICATION-*/ 
            App1.MedidaRE.DateMesu.Hou= rxMessage.Payload[16]; 
            App1.MedidaRE.DateMesu.Day= rxMessage.Payload[17];
            App1.MedidaRE.DateMesu.Mon= rxMessage.Payload[18];
            App1.MedidaRE.Mesure= bytesToWord(rxMessage.Payload[19],rxMessage.Payload[20]);

            App1.MedidaDif.Index=          rxMessage.Payload[21];
            
            for (i=0; (i < TOMAS)&&(i<= App.MedidaDif.Index); i++)
                App1.MedidaDif.MedidaD[i]= rxMessage.Payload[22 + i];                       
                            
            //TOTAL BYTES ENVIADOS 14+ TOMAS =APROX 38 QUEDAN {127-HEADRER}= XBEE_PAYLOAD
                        
        break; 
        
        case OP_ASOCIACION:
            /* Caso que haya trama CORTA valida recibida modbus*/
            //App1.ID=      ID MAC 
            App1.TKMM=      bytesToWord( rxMessage.Payload[4],rxMessage.Payload[5]);
            App1.Secuencia= rxMessage.Payload[6];            //para que en cuanto responda, machee
            /*-DATA OF LAYER-*/ 
            /*-DATA Of APPLICATION-*/ 
        break;
        case OP_TIME: 
            /* Caso que haya trama CORTA valida recibida modbus*/
            //App1.ID= bytesToWord( Rx_Buffer_MODMIWI.Message[k].Payload[4],Rx_Buffer_MODMIWI.Message[k].Payload[5]);
            App1.TKMM=      bytesToWord( rxMessage.Payload[4],rxMessage.Payload[5]);
            App1.Secuencia= rxMessage.Payload[6];           //para que en cuanto responda, machee
            //Rx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PN;          
            /*-DATA OF LAYER-*/                     
            /*-DATA Of APPLICATION-*/          
            
        break;  
        case OP_ASOCIACION_PAN:
            /* Caso que haya trama CORTA valida recibida modbus*/             
            App1.Tipo= 0xff;
            App1.ID=   0;
        break;  
        case OP_CONTROL:
            /* Caso que haya trama LARGA valida  recibida modbus*/
            App1.ID=    bytesToWord( rxMessage.Payload[4],rxMessage.Payload[5]); 
            App1.TOKEN= bytesToWord( rxMessage.Payload[6],rxMessage.Payload[7]); 
            App1.TKMM=  bytesToWord( rxMessage.Payload[8],rxMessage.Payload[9]);                                 

            App1.SubTipo=       rxMessage.Payload[10];
            App1.DatCtrl.Index= rxMessage.Payload[11];

            for (i=0 ; (i < CTRL_LNGTH) && (i <= App.DatCtrl.Index) ; i++)
                App1.DatCtrl.Data[i]= rxMessage.Payload[12 + i];
                      
        break;      
        case nOP:                

            App1.ID= 0;
                        
        default: 

            App1.Tipo= 0xff;
            App1.ID=   0;
                                
    }
    
    if ( App1.ID== 0 || App1.Tipo== 0xff )
        return (FALSE);
    else 
    {
        App1.PermanentAdd= !rxMessage.flags.bits.repeat;          // EL mensaje provienen de un repetidor 

        if (rxMessage.flags.bits.altSrcAddr)                      // Direccion indirecta mediante PINID 2 Bytes    
            memcpy(App1.Add,rxMessage.SourceAddress,2);           //solo para PANCO en el gateway no (Comunicacion indirecta)
        else
            memcpy(App1.Add,rxMessage.SourceAddress,ADDRESS_LEN); //Direccion larga para comunicacion directa
        return (TRUE);
    }
}    

/* Agregados para el caso de gateway pan*/

BOOL Proc_Req_First_MW(BOOL recb){
    
    if (recb)
    {    
        if( App1.Tipo == OP_ASOCIACION )// estado de trama nueva App1 2
            Nw_Frm_ASO= TRUE;        
        if( App1.Tipo == OP_CONTROL )// estado de trama nueva App1 2
            Nw_Frm_CTRL_MW= TRUE;
        
        return(TRUE);
    }            
    else
    {
        //------------Consideraciones para ingreso en Caso 2---------------------        
        if(Chk_Frm_Rx_ASO_Buffer_MW()) //&& !Sem_MIWI_DONE)
        {// Verifico que el servicio no este ocupado App1.tipo=0
            App1.Tipo= OP_ASOCIACION;              // App1 en blanco y el ST1=1 indicando trama Asoc pendiente
            return(TRUE);        
        }else 
            if(Buff_CTRL_TCP)
            {   
                App1.Tipo= OP_CONTROL;             // App1 en blanco        
                return(TRUE);
            }                            
    } 
    return(FALSE);    
}


void Search_Control_MW(void){
               
    BYTE i=0;        
   
    // Uso indice ID=0 indica elemento vector sin uso.
    while(Base_Slot[i][0]!=App1.ID && i<(BASE_SLT_LNGTH-1) && Base_Slot[i][0]!=0 ) 
        i++;
    
    if (Base_Slot[i][0]!=App1.ID) { // descarto el elemento actual si llega al final
        App1.Slot=0;// no habra cambios
        return;    
    }
        
    App1.Slot=Base_Slot[i][1]; // elemento actual filtrado

    // limpio la base y ordeno
        
    while(i<49){ //|| BaseSlot[i][1]!=0) { // caso de ser cualquiera

        Base_Slot[i][0]=Base_Slot[i+1][0];
        Base_Slot[i][1]=Base_Slot[i+1][1];        
        i++;
    }
    Base_Slot[i][0]=0;          // caso de ser el ultimo elemento 49
    Base_Slot[i][1]=0;
    
    return;
    
} 
void Put_Base_MW(void){    
    BYTE i=0;        
    
    while(Base_Slot[i][0]!=0 && i<(BASE_SLT_LNGTH-1))
        i++;
       
    if (Base_Slot[i][0]==0)
    {
        Base_Slot[i][0]=App.DatCtrl.Data[0];//ID;
        Base_Slot[i][1]=App.DatCtrl.Data[1];//Slot;
    }
} 

/*------------------------------------------------------------------------------ 
 *              Tratamiento de buffers de tramas normales
 * ---------------------------------------------------------------------------*/

/*               Coloca tramas llegadas de MIWI normales en Buffer            */

BYTE Put_Buffer_Nrm_MW(void){
    
    BYTE *p=NULL;
    BYTE *p1=NULL;
    BYTE i,j;
                
    j= Buffer_Nrm.point;
    /*-HEADER-*/
    Buffer_Nrm.Buffer[j].ID=    App1.ID;
    Buffer_Nrm.Buffer[j].TOKEN= App1.TOKEN;
    /*-DATA OF LAYER-*/
    Buffer_Nrm.Buffer[j].Slot=  App1.Slot;
    
    p=  &App1.Nro_intentos;
    p1= &Buffer_Nrm.Buffer[j].Nro_intentos;

    for(i=0;i<5;i++){                    
        *p1= *p;
        p++;
        p1++;
    }

    Buffer_Nrm.Buffer[j].DateTx.Seg= App1.DateTx.Seg;
    Buffer_Nrm.Buffer[j].DateTx.Min= App1.DateTx.Min;
    Buffer_Nrm.Buffer[j].DateTx.Hou= App1.DateTx.Hou;
    
    //p=&App1.DateTx.Seg; 
    //p1=&BufferRX[j].DateTx.Seg; //GW_RTCC.Seg;
    //solo seg min hour
    //for(i=0;i<3;i++){ 
    //    *p1=*p;
    //    p++;
    //    p1++;
   // }
    
    Buffer_Nrm.Buffer[j].MedidaRE.Mesure=       App1.MedidaRE.Mesure; //ultima medida
    Buffer_Nrm.Buffer[j].MedidaRE.DateMesu.Hou= App1.MedidaRE.DateMesu.Hou; //ultima medida
    Buffer_Nrm.Buffer[j].MedidaRE.DateMesu.Day= App1.MedidaRE.DateMesu.Day; //ultima medida
    Buffer_Nrm.Buffer[j].MedidaRE.DateMesu.Mon= App1.MedidaRE.DateMesu.Mon; //ultima medida
    
    p=  App1.MedidaDif.MedidaD;
    p1= Buffer_Nrm.Buffer[j].MedidaDif.MedidaD;
    
    for (i = 0; (i < TOMAS) && (i <= App1.MedidaDif.Index); i++) 
    {
        *p1= *p;
        p++;
        p1++;
    }
    
    return(++Buffer_Nrm.point);// incremento el buffer.   
}  

BOOL Resp_MIWI_NORM_MW(void){ // case 1
    /*LOS DATOS DE RTCC Y DE SLOT SON AGREGADOS AQUI PARA TRANSMITIR POR MIWI*/
    static BOOL s= TRUE;
    
    if (s)
    {
        s--;
        App1.TOKEN= Get_TK_BufferTK_MW((BYTE)(App1.ID>>8));
        RTCC_Get(&App1.DateTx.Seg); //Ubica todos los datos de tiempo en App1.DateTx para ser transmitidos a MiWi
        Search_Control_MW();//busca cambios en el Slot de comunicacion y los guarda en App1.slot, acomoda BaseSlot.
    }    
    if (Serv_MW(TRANS_W))           //Responde a la subida  toma datos a app1
    {   
        s++;
        return(TRUE);
    }
    return(FALSE);
}

BOOL Resp_MIWI_ASOC_MW(void){
    
    BYTE j=0;                   
    while(j < BUFFER_LNGTH)
    { 
        if ( Buffer_Aso[j].Send && Buffer_Aso[j].Rece && ( Buffer_Aso[j].ID_AUT ) )
        { //consulta al buffer
            
            App1.ID=        Buffer_Aso[j].Element.ID_AUI; 
            App1.TOKEN=     Buffer_Aso[j].Element.TK;
            //tipo ya esta asignado
            App1.Secuencia= Buffer_Aso[j].Element.Secuencia;// secuencia de medidor
            App1.Slot=      Buffer_Aso[j].Element.Slot;
            App1.ID_AUT=    Buffer_Aso[j].ID_AUT;//id que envia el servidor que antes resultaba 0
            RTCC_Get(&App1.DateTx.Seg);              // fecha en App1.DAte
            App1.PermanentAdd= Buffer_Aso[j].PermanentAdd; 
            memcpy(App1.Add,Buffer_Aso[j].Add,ADDRESS_LEN);      
            //Buffer[j].Rece=true;
            //AddMIWI=Buffer[j].Add;
            //rireccopm de respuesta
            //memcpy(AddMIWI,Buffer[j].Add,ADDRESS_LEN);// machea ID-ADD incial con la ID del medidor brindada
            
            //Buffer[j].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1

            Serv_MW(TRANS_W);          //Responde a la asociacion  toma los datos a app1
            
            //--------blanqueo-----------------------------            
                        
            Buffer_Aso[j].Send=     FALSE; 
            Buffer_Aso[j].Rece=     FALSE;
            Buffer_Aso[j].Sec_GW=   0; 
            Buffer_Aso[j].ID_AUT=   0;
            Buffer_Aso[j].Element.ID_AUI=    0;
            Buffer_Aso[j].Element.Slot=      0;
            Buffer_Aso[j].Element.TK=        0;
            Buffer_Aso[j].Element.Secuencia= 0;// secuencia de medidor
            Buffer_Aso[j].PermanentAdd= 0; 
        //  Buffer[j].Add={0,0,0,0,0,0,0,0};
        //  Buffer[j].Add={{0}}; //sale de aplicacion MIWI no esta en app1
            return(TRUE);
        }
        j++; 
    }
          
    return(FALSE);
}

/* Respondo a lo llegado desde MIWI.
 * Arriba una trama MiWI de Ctrl. Se requiere responder con RTCC y con lo que haya
 * hallojado para el en el buffer por parte de MB.
 */
BOOL Resp_MIWI_CTRL_MW(void *s , void *t){
    
    Ctrl_Buffer_t *p= NULL;
    APP_t *q = NULL;
    /*Punteros a Byte*/       
    BYTE *a,*b;    
    BYTE i,l;
    BYTE j= 0;
    
    p= s;     //Buffer Q o R
    q= t;     //App App1          
    /********************************BUSCO SI HAY ALGO PARA ESE ID EN Q*****/    
    while(j < BS_CTRL_LNGTH)
    {   
        /*************************************SETEO ESTRUCTURA APP1 **************/    
        if ( !p->Ctrl_Base[j].Send && (p->Ctrl_Base[j].ID == q->ID) )   
        {   //consulta al bufferQ para su ID            
            RTCC_Get(&q->DateTx.Seg); // fecha en App1.Date &App1.DateTx.Seg
            q->TOKEN =   p->Ctrl_Base[j].TKSRV;
            q->TKMM =    p->Ctrl_Base[j].TKMM;      //es un token anterior no el actual
            q->SubTipo = p->Ctrl_Base[j].SubTipo;

            /*Punteros a Byte*/       
            b = &q->DatCtrl.Index;
            a = &p->Ctrl_Base[j].DatCtrl.Index;
            
            l= *b+1;                                //(Index + carga util)

            for( i=0; i<= l; i++)
            {
                *b= *a;
                b++;
                a++;
            }                            

            if (Serv_MW(TRANS_W))                  //Responde a la asociacion  toma los datos a app1
            {                                   
                //Clear
                p->Ctrl_Base[j].Send= TRUE;
                p->Ctrl_Base[j].ID=   0;
                //Clear_BufferCtrl_MW(p);                
                return(TRUE); 
            }                                        
        }        
        j++;                     
    }          
    return(FALSE);    
}

BOOL Tx_MIWI_CTRL_MW(void *s , void *t){
    
    Ctrl_Buffer_t *p= NULL;
    APP_t         *q= NULL;
    /*Punteros a Byte*/       
    BYTE *a,*b;    
    BYTE l;
    BYTE j= 0;
    
    p= s;        //Buffer Q o R
    q= t;        //App App1     
    /*************************************SETEO ESTRUCTURA APP1 **************/
    RTCC_Get(&q->DateTx.Seg);                   // fecha en App1.Date    
    j= p->index_to_send;          // Salvo para comparar
    
    //consulta si el apuntado ya fue enviado por respuesta ID
    //En caso de Roll incrementa una posicion
    while ((p->Ctrl_Base[p->index_to_send].Send == TRUE ) 
           && (p->Ctrl_Base[p->index_to_send].ID== 0 ))
    {
        p->index_to_send++;
        
        if (p->index_to_send == BS_CTRL_LNGTH)
            p->index_to_send= 0;
        
        if (j == p->index_to_send) //verifico roll-off
        {   
            //roll-off evoluciono el puntero para que acerque a index
            p->index_to_send++;        
            if (p->index_to_send == BS_CTRL_LNGTH)
                p->index_to_send= 0;
            return(FALSE);         // no hay nada que transmitir
        }    
    }
    
    //j= p->index_to_send;        //el indice debe Trama enviada.        
    q->ID=      p->Ctrl_Base[p->index_to_send].ID;
    q->TOKEN=   p->Ctrl_Base[p->index_to_send].TKSRV;
    q->TKMM=    p->Ctrl_Base[p->index_to_send].TKMM;
    q->SubTipo= p->Ctrl_Base[p->index_to_send].SubTipo;

    /*Punteros a Byte*/      
    b= &q->DatCtrl.Index;
    a= &(p->Ctrl_Base[p->index_to_send].DatCtrl.Index);
            
    l= *b+1;                      //(Index + carga util)

    for(j= 0; j<= l; j++)
    {
        *b= *a;
        b++;
        a++;
    }                            
    
    if (Serv_MW(TRANS_W))         //Responde a la asociacion  toma los datos a app1
    {   
        p->Ctrl_Base[p->index_to_send].Send= TRUE;
        p->Ctrl_Base[p->index_to_send].ID=   0;
        return(TRUE);       
        
    }else    
        return(FALSE);
}

BOOL Serv_Buff_stat(void){
    return(FALSE);
}


/*****************************************************
*  Busca Token para grupo siguiente que 
 * complete el Buffer de norm, viene en la 
 * respuesta del servidor en NORM
******************************************************/
WORD Get_TK_BufferTK_MW(BYTE id){

    BYTE  i;
    WORD tk;
    for(i=0; i<N_PAN ; i++)
    { 
        if (TK_Buffer_Nrm[i].ID == id )
        {   
            TK_Buffer_Nrm[i].Base.Tables[ TK_Buffer_Nrm[i].Base.index_to_send ].Send= TRUE;
                        
            tk= TK_Buffer_Nrm[i].Base.Tables[ TK_Buffer_Nrm[i].Base.index_to_send ].TKSRV;
            
            TK_Buffer_Nrm[i].Base.index_to_send++;
            if (TK_Buffer_Nrm[i].Base.index_to_send == BS_TK_LNGTH)
                TK_Buffer_Nrm[i].Base.index_to_send= 0;
            
            break;
        }
    }    
    
    return(tk);
}



//@TODO implementar esta funcion
void Put_Buffer_TIME_MW(void){
    return;
}

/*******************************************************************************
 * Control de buffer generico tanto para MIWI y MB
 * para escribir tramas de TIPO Control
 ******************************************************************************/

BYTE Put_Buffer_Ctrl_MW(void *p , void *t){
        
    Ctrl_Buffer_t *q = NULL;
    APP_t *v = NULL;
    /*Punteros a Byte*/       
    BYTE *a,*b;
    
    BYTE j,l;
    
    q= p; //Buffer Q o R
    v= t; //App App1
    
    j= q->index; //ubico en lugar libre
    q->Ctrl_Base[j].Send=    FALSE;
    q->Ctrl_Base[j].ID=      v->ID;
    q->Ctrl_Base[j].TKSRV=   v->TOKEN;
    q->Ctrl_Base[j].TKMM=    v->TKMM;
    q->Ctrl_Base[j].SubTipo= v->SubTipo;
    /*Punteros a Byte*/       
    b= &App1.DatCtrl.Index;
    a= &(q->Ctrl_Base[j].DatCtrl.Index);
    
    l= *b+1;
    
    for( j=0; j<=l; j++)
    {
        *a= *b;
        b++;
        a++;
    }
    
    if (q->index < BS_CTRL_LNGTH-1)
        q->index++;
    else
        q->index=0;
          
    return(q->index);
}
/******************************************************************************
    Actualiza el buffer de Ctrl Q o R segun lo operado anteriormente de setear 
    Sem_MIWI_DONE.
 ******************************************************************************/
void UpGrade_Buffer_Ctrl_MW(void *p){
    
}

BOOL UpDate_Buffer_Ctrl_MW(void *p){
    
    Ctrl_Buffer_t *q = NULL;
    q= p;
    
    q->index_to_send++; //el indice debe Trama enviada.
    if (q->index == BS_CTRL_LNGTH)
        q->index=0;
    
    if (q->index == q->index_to_send)
        return (FALSE);  //No hay mas tramas
    /*    
    if ((q->index > q->index_send) && (q->index - q->index_send)>0) 
    {
        return(TRUE);
    }else
        if(q->index < q->index_send) // pego la vuelta arribados, enviados apunto superior. 
    {
        return(TRUE);
    }
    */
    return (TRUE);  //hay mas tramas
}

void Clr_Buffer_Ctrl_MW(void *p){

}

/*                Tratamiento de buffers de tramas ASOCIACION                  */
//                lo coloca MIWI para que luego lo tome TCP
BOOL Put_Buffer_Aso_MW(void){ 
    BYTE i=0;
    static BYTE j=0;
    //verifico si es que ya hay un pedido existente del mismo Monitor o FFDs(coordinadores)
    
    while  ( i < BUFFER_LNGTH )
    {
        if  (Buffer_Aso[i].Element.ID_AUI == App1.ID)
        {   //MAC en buffer igual al de App1 trama existente del  mismo medidor pero anterior                                       
            Buffer_Aso[i].Element.Secuencia= App1.Secuencia;// secuencia de PAN MB
            //Buffer[i].Sec_GW=0;               // secuencia de TCP-IP
            //Buffer[i].Sent=false; 
            //Buffer[i].Rece=false;
            // Buffer[i].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1
            // memcpy(Buffer[i].Add,AddMIWI,ADDRESS_LEN); //solo para PANCO en el gateway no
            return(FALSE); //reescritura
        }
        i++;
    }    

    // caso default
    
    Buffer_Aso[j].Rece=    FALSE;
    Buffer_Aso[j].Send=    FALSE;     
    Buffer_Aso[j].Sec_GW=  0;    
    Buffer_Aso[j].ID_AUT=  0;
    Buffer_Aso[j].Element.ID_AUI= App1.ID;    
    Buffer_Aso[j].Element.TK=     0;
    Buffer_Aso[j].Element.Slot=   0;               
    Buffer_Aso[j].Element.Secuencia= App1.Secuencia;
    Buffer_Aso[i].PermanentAdd=      App1.PermanentAdd;
    memcpy(Buffer_Aso[i].Add, App1.Add,ADDRESS_LEN); //solo para PANCO en el gateway no
        
    if (j<(BUFFER_LNGTH-1))
        j++;
    else
        j=0;    
    
    return(TRUE); //nueva trama sin sobre escribir        
}
/* bool Get_Buffer_MIWI(void)
 *  para casos de control no se usa en el primer proyecto
 * 
 *  */
/*
bool Get_Buffer_MIWI(void){// lo usa TCP para operar y colocar en su TCP TX
    
    uint8_t i,q=0;
    static uint8_t j=0;
    uint8_t *p=NULL,*p1=NULL;
    
    
    while  (q<Bufferlength){
    
        if  (!(*p1)){            //Sent=0
            
            App.ID=Buffer[q].Element.ID;
            p=&App.Tipo;
            p1=&Buffer[0].Element.Tipo;

            for(i=0;i<6;i++){                    
                *p=*p1;
                p++;
                p1++;
            }

            BufferRX.Buffer[j].Slot=App1.Slot;
            
            p=&App1.DateTx.Seg; 
            p1=&BufferRX.Buffer[j].DateTx.Seg; //GW_RTCC.Seg;

            for(i=0;i<6;i++){ 
                *p=*p1;
                p++;
                p1++;
            }
            
            p=&App1.MedidaRE.DateMesu.Hou;
            p1 =&BufferRX.Buffer[j].MedidaRE.DateMesu.Hou;
            
            for (i = 0; i <3 ; i++) {
                    *p= *p1 ;
                    p++;
                    p1++;
            }        

            for (i = 0; i < Tomas; i++) {

                App1.MedidaDif[i]=BufferRX.Buffer[j].MedidaDif[i];

            }
            
            
            return(true);
        }
       p1++;
    }   
    return(false);
} 
*/  

/******************************************************************************
 * Get_Tipo2_Buffer_MIWI
 * Descripcion: se ulitiliza para buscar tramas tipo 2 con envio
 * y con recepcion, en el buffer de tramas tipo 2         
 ******************************************************************************/

BOOL Chk_Frm_Rx_ASO_Buffer_MW(void){
    BYTE i=0;
    
    while(i<BUFFER_LNGTH){ // por que razon??? verificar!!!
        
        if (Buffer_Aso[i].ID_AUT && Buffer_Aso[i].Rece ){ //si no es igual a cero

            return(TRUE);
        }
        i++;        
    }
    return(FALSE);
}

   