/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H
#endif
//#include <xc.h> // include processor files - each processor file is guarded.  

#include "GenericTypeDefs.h"

BOOL Set_App1_From_MW(void);
BOOL Proc_Req_First_MW(BOOL);

void Search_Control_MW(void);
void Put_Base_MW(void);     //generico
BYTE Put_Buffer_Nrm_MW(void);
BOOL Resp_MIWI_NORM_MW(void);
BOOL Resp_MIWI_ASOC_MW(void);
BOOL Resp_MIWI_CTRL_MW(void*,void*);
BOOL Tx_MIWI_CTRL_MW(void*,void*);
BOOL Serv_Buff_stat(void);
WORD Get_TK_BufferTK_MW(BYTE);
void Put_Buffer_TIME_MW(void);
BYTE Put_Buffer_Ctrl_MW(void*,void*);
void UpGrade_Buffer_Ctrl_MW(void*);
BOOL UpDate_Buffer_Ctrl_MW(void*);
void Clr_Buffer_Ctrl_MW(void*);
BOOL Put_Buffer_Aso_MW(void);
//bool Get_Buffer_MIWI(void); No existe comparable similar
BOOL Chk_Frm_Rx_ASO_Buffer_MW(void);
//bool Get_BufferTx_MIWI(void); No existe comparable similar

#define L_SEC_GW  1
#define L_SEC_PAN 2
#define L_ID_AUI  3 
#define L_PAN_ID_AUT 4
//@todo completar las etiquetas para los indices

/* Sub codigos App.Tipo>=5;
            * 00-09: De RED:Cambio de Slot, RFD, FFD.
            * 10-19: De Monitoreo Interno: temperatura, humedad, scaneo, consumo, generacion solar, velociadad viento.
            * 20-29: De Control Interno: reset POW, reset blank, cambio PAN, cambio de canal, cambio de banda, control potencia, T de sleep.
            * 30-39: De Control Externo: Control IOs, USB, CAN, USART,Display.
            * 40-49: De Control Datos: Escritura de Variable, Lectura de Variable.
            * 50-59: De Control Programa: Control de flujo de programa(vector de punteros a funciones).
            * 60-69:
            * 70-79:
            * 80-89:
            * 90-99:
*/
#define Ctrl_NET    0
#define Ctrl_NET_SLOT   0
#define Ctrl_NET_CHG_PAN    1
#define Ctrl_NET_CHG_CHN    2
#define Ctrl_NET_CHG_BND    3
#define Ctrl_NET_CHG_PTX    4
        
#define Ctrl_VIEW   1
#define Ctrl_VIEW_TEMP  0
#define Ctrl_VIEW_SCN   1
#define Ctrl_VIEW_PWR   3
#define Ctrl_VIEW_GNS   4
#define Ctrl_VIEW_WND   5

#define Ctrl_IN     2
#define Ctrl_IN_POR     0
#define Ctrl_IN_RSTM    1
#define Ctrl_IN_T_SLP   2


#define Ctrl_OUT   3
#define Ctrl_OUT_IO    0
#define Ctrl_OUT_USB   1
#define Ctrl_OUT_CAN   2
#define Ctrl_OUT_SPI   3
#define Ctrl_OUT_I2C   4
#define Ctrl_OUT_USART 5
#define Ctrl_OUT_DSPLY 6

#define Ctrl_DAT    4
#define Ctrl_DAT_EEP   0
#define Ctrl_DAT_SST   1
#define Ctrl_DAT_FLH   2

#define Ctrl_PRG    5
#define Ctrl_PRG_PTR   0

