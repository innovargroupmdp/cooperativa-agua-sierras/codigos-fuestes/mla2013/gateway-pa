/********************************************************************
* FileName:		BSP_MiWiCardDemoBd.h
* Dependencies:    
* Processor:	PIC18	
* Complier:     Microchip C18 v3.04 or higher	
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice
*
* Copyright � 2007-2010 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute 
* Software only when embedded on a Microchip microcontroller or digital 
* signal controller and used with a Microchip radio frequency transceiver, 
* which are integrated into your product or third party product (pursuant 
* to the terms in the accompanying license agreement).   
*
* You should refer to the license agreement accompanying this Software for 
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
*********************************************************************
* File Description:
*
*  This file defines functions used for demo board hardware
*
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/09/2011    ccs       Initial revision
********************************************************************/
#ifndef __BSP_MIWI_DEMO_BD_H_

#include "Configs/HWP PICDN2_ETH97.h"

	#define __BSP_MIWI_DEMO_BD_H_

    // System Clock Frequency
    #define CLOCK_FREQ             GetSystemClock()//16000000
    
    // There are three ways to use NVM to store data: External EPROM, Data EEPROM and 
    // programming space, with following definitions:
    //      #define USE_EXTERNAL_EEPROM
    //      #define USE_DATA_EEPROM
    //      #define USE_PROGRAMMING_SPACE  
    // Each demo board has defined the method of using NVM, as
    // required by Network Freezer feature.        
    #define USE_EXTERNAL_EEPROM
    
    //#define SUPPORT_TWO_SPI
    
    // Define EEPROM_SHARE_SPI if external EEPROM shares the SPI 
    // bus with RF transceiver
    #define EEPROM_SHARE_SPI
    
        #define SW1                 1
        #define SW2                 2	

    #if defined(MRF24J40)
        // MRF24J40 Pin Definitions
        #define RFIF                INTCONbits.INT0IF
        #define RFIE                INTCONbits.INT0IE
        #define PHY_CS              EXT8_IO
        #define PHY_CS_TRIS         EXT8_TRIS
        #define RF_INT_PIN          EXT12_IO
        #define RF_INT_TRIS         EXT12_TRIS
        #define PHY_WAKE            EXT9_IO
        #define PHY_WAKE_TRIS       EXT9_TRIS
        #define PHY_RESETn          EXT10_IO
        #define PHY_RESETn_TRIS     EXT10_TRIS

    #elif defined(MRF89XA)
    	#define Data_nCS            LATAbits.LATA5
    	#define Data_nCS_TRIS       TRISAbits.TRISA5  
    	#define Config_nCS          LATDbits.LATD1
        #define Config_nCS_TRIS     TRISDbits.TRISD1 

        #define IRQ0_INT_PIN        PORTBbits.RB0
        #define IRQ0_INT_TRIS       TRISBbits.TRISB0
    	#define PHY_IRQ0            INTCONbits.INT0IF
    	#define PHY_IRQ0_En         INTCONbits.INT0IE
    	
        #define IRQ1_INT_PIN        PORTCbits.RC6
        #define IRQ1_INT_TRIS       TRISCbits.TRISC6    	             	  	
    	#define PHY_IRQ1            INTCON3bits.INT1IF
    	#define PHY_IRQ1_En         INTCON3bits.INT1IE 	
        #define PHY_RESETn          LATDbits.LATD0
        #define PHY_RESETn_TRIS     TRISDbits.TRISD0

    #endif
    
        // EEProm Pin Definitions
     	#define RF_EEnCS		    SPIFLASH_CS_IO
        #define MAC_nCS             SPIFLASH_CS_IO
        #define RF_EEnCS_TRIS	    SPIFLASH_CS_TRIS
    	
        // SPI1 Pin Definitions
        #define SPI_SDI             SPIFLASH_SDI_IO            
        #define SDI_TRIS            SPIFLASH_SDI_TRIS
        #define SPI_SDO             LATCbits.LATC5               
        #define SDO_TRIS            SPIFLASH_SDO_TRIS
        #define SPI_SCK             LATCbits.LATC3              
        #define SCK_TRIS            SPIFLASH_SCK_TRIS
        
        // SPI2 Pin Definitions
        //#define SPI_SDI2            PORTBbits.RB7               
        //#define SDI2_TRIS           TRISBbits.TRISB7
        //#define SPI_SDO2            LATBbits.LATB6               
        //#define SDO2_TRIS           TRISBbits.TRISB6
        //#define SPI_SCK2            LATBbits.LATB1                   
        //#define SCK2_TRIS           TRISBbits.TRISB1
        
        //#define SPI2SSPIF           PIR3bits.SSP2IF
        //#define SPI2WCOL            SSP2CON1bits.WCOL
        //#define SPI2SSPBUF          SSP2BUF
        
        // Switch and LED Pin Definitions
        #define SW1_PORT              BUTTON0_IO
        #define SW1_TRIS              BUTTON0_TRIS
        //#define SW2_PORT            PORTBbits.RB2
        //#define SW2_TRIS            TRISBbits.TRISB2
        
        #define LED                   LED0_IO               
        #define LED_TRIS              LED0_TRIS
        
        // External EEPROM Pin Definitions
        
		#define EE_nCS              SPIFLASH_CS_IO
        #define EE_nCS_TRIS         SPIFLASH_CS_IO
        
        
        #define TMRL                TMR0L
            
        //External SST Serial Flash Definitions
        
        //#define SST_nCS              LATAbits.LATA5
        //#define SST_nCS_TRIS         TRISAbits.TRISA5
        
        // Sensor Pin Definitions
        
        #define DHT11_TRIS                    EXT3_TRIS
        #define DHT11_LAT                     EXT3_IO
        #define DHT11_PORT                    EXT3_IO
        #define DHT11_SetHigh()               do { DHT11_LAT = 1; } while(0)
        #define DHT11_SetLow()                do { DHT11_LAT = 0; } while(0)
        #define DHT11_Toggle()                do { DHT11_LAT = ~DHT11_LAT; } while(0)
        #define DHT11_GetValue()              DHT11_PORT
        #define DHT11_SetDigitalInput()       do { DHT11_TRIS = 1; } while(0)
        #define DHT11_SetDigitalOutput()      do { DHT11_TRIS = 0; } while(0)
        
        // Analogics Pin Definitions
        
        #define AN0_TRIS                    EXT4_TRIS
        #define AN0_LAT                     EXT4_IO
        #define AN0_PORT                    EXT4_IO
        #define AN0_SetHigh()               do { AN0_LAT = 1; } while(0)
        #define AN0_SetLow()                do { AN0_LAT = 0; } while(0)
        #define AN0_Toggle()                do { AN0_LAT = ~AN0_LAT; } while(0)
        #define AN0_GetValue()              AN0_PORT
        #define AN0_SetDigitalInput()       do { AN0_TRIS = 1; } while(0)
        #define AN0_SetDigitalOutput()      do { AN0_TRIS = 0; } while(0)
        
        #define AN1_TRIS                    EXT5_TRIS
        #define AN1_LAT                     EXT5_IO
        #define AN1_PORT                    EXT5_IO
        #define AN1_SetHigh()               do { AN1_LAT = 1; } while(0)
        #define AN1_SetLow()                do { AN1_LAT = 0; } while(0)
        #define AN1_Toggle()                do { AN1_LAT = ~AN1_LAT; } while(0)
        #define AN1_GetValue()              AN1_PORT
        #define AN1_SetDigitalInput()       do { AN1_TRIS = 1; } while(0)
        #define AN1_SetDigitalOutput()      do { AN1_TRIS = 0; } while(0)

        //--------------------------CS-THERNET------------------------------------------

        // get/set SCK aliases
        //#define SCK_TRIS                 TRISCbits.TRISC3
        //#define SCK_LAT                  LATCbits.LATC3
        //#define SCK_PORT                 PORTCbits.RC3
        //#define SCK_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
        //#define SCK_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
        //#define SCK_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
        //#define SCK_GetValue()           PORTCbits.RC3
        //#define SCK_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
        //#define SCK_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)

        //------------MODBUS------------------------------------------------------------
        // get/set RC0 procedures
        #define nRE_TRIS                    UART_nRE_TRIS
        #define nRE_LAT                     UART_nRE
        #define nRE_PORT                    UART_nRE
        #define nRE_SetHigh()               do { nRE_LAT = 1; } while(0)
        #define nRE_SetLow()                do { nRE_LAT = 0; } while(0)
        #define nRE_Toggle()                do { nRE_LAT = ~nRE_LAT; } while(0)
        #define nRE_GetValue()              nRE_PORT
        #define nRE_SetDigitalInput()       do { nRE_TRIS = 1; } while(0)
        #define nRE_SetDigitalOutput()      do { nRE_TRIS = 0; } while(0)

        // get/set RC1 procedures
        #define DE_TRIS                    UART_DE_TRIS
        #define DE_LAT                     UART_DE
        #define DE_PORT                    UART_nRE
        #define DE_SetHigh()               do { DE_LAT = 1; } while(0)
        #define DE_SetLow()                do { DE_LAT = 0; } while(0)
        #define DE_Toggle()                do { DE_LAT = ~DE_LAT; } while(0)
        #define DE_GetValue()              DE_PORT
        #define DE_SetDigitalInput()       do { nRE_TRIS = 1; } while(0)
        #define DE_SetDigitalOutput()      do { nRE_TRIS = 0; } while(0)

#endif
