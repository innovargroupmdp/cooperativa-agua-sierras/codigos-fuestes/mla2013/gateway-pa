/* 
 * File:   Func_of_Main.h
 * Author: cba
 *
 * Created on October 24, 2020, 3:23 PM
 */

#ifndef FUNC_OF_MAIN_H
#define	FUNC_OF_MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "TCPIP Stack/TCPIP.h"
    
void Serv_APP(void);
/* An actual function defined in MainDemo.c for displaying the current IP 
 address on the UART and/or LCD.
*/
void DisplayIPValue(IP_ADDR IPVal);

/*
 // Processes A/D data from the potentiometer
 */
CHAR *ProcessIO(void);
