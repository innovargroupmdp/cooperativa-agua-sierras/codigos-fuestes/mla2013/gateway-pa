//////////////////////////////////////////////////////////////////////////////////////////
////                                      modbus.c                                    ////
////                                                                                  ////
////                 MODBUS protocol driver for serial communications.                ////
////                                                                                  ////
////  Refer to documentation at http://www.modbus.org for more information on MODBUS. ////
////                                                                                  ////
//////////////////////////////////////////////////////////////////////////////////////////
////                                                                                  ////
//// DEFINES:                                                                         ////
////                                                                                  ////
////  MODBUS_TYPE                   MODBUS_TYPE_MASTER or MODBUS_TYPE_SLAVE           ////
////  MODBUS_SERIAL_INT_SOURCE      Source of interrupts                              ////
////                                (MODBUS_INT_EXT,MODBUS_INT_RDA,MODBUS_INT_RDA2)   ////
////  MODBUS_SERIAL_BAUD            Valid baud rate for serial                        ////
////  MODBUS_SERIAL_RX_PIN          Valid pin for serial receive                      ////
////  MODBUS_SERIAL_TX_PIN          Valid pin for serial transmit                     ////
////  MODBUS_SERIAL_ENABLE_PIN      Valid pin for serial enable, rs485 only           ////
////  MODBUS_SERIAL_RX_ENABLE       Valid pin for serial rcv enable, rs485 only       ////
////  MODBUS_SERAIL_RX_BUFFER_SIZE  Size of the receive buffer                        ////
////                                                                                  ////
////                                                                                  ////
//// SHARED API:                                                                      ////
////                                                                                  ////
////  modbus_init()                                                                   ////
////    - Initialize modbus serial communication system                               ////
////                                                                                  ////
////  modbus_serial_send_start(address,func)                                          ////
////    - Setup serial line to begin sending.  Once this is called, you can send data ////
////      using modbus_serial_putc().  Should only be used for custom commands.       ////
////                                                                                  ////
////  modbus_serial_send_stop()                                                       ////
////    - Must be called to finalize the send when modbus_serial_send_start is used.  ////
////                                                                                  ////
////  modbus_kbhit()                                                                  ////
////    - Used to check if a packet has been received.                                ////
////                                                                                  ////
//// MASTER API:                                                                      ////
////  All master API functions return 0 on success.                                   ////
////                                                                                  ////
////  exception modbus_read_coils(address,start_address,quantity)                     ////
////    - Wrapper for function 0x01(read coils) in the MODBUS specification.          ////
////                                                                                  ////
////  exception modbus_read_discrete_input(address,start_address,quantity)            ////
////    - Wrapper for function 0x02(read discret input) in the MODBUS specification.  ////
////                                                                                  ////
////  exception modbus_read_holding_registers(address,start_address,quantity)         ////
////    - Wrapper for function 0x03(read holding regs) in the MODBUS specification.   ////
////                                                                                  ////
////  exception modbus_read_input_registers(address,start_address,quantity)           ////
////    - Wrapper for function 0x04(read input regs) in the MODBUS specification.     ////
////                                                                                  ////
////  exception modbus_write_single_coil(address,output_address,on)                   ////
////    - Wrapper for function 0x05(write single coil) in the MODBUS specification.   ////
////                                                                                  ////
////  exception modbus_write_single_register(address,reg_address,reg_value)           ////
////    - Wrapper for function 0x06(write single reg) in the MODBUS specification.    ////
////                                                                                  ////
////  exception modbus_read_exception_status(address)                                 ////
////    - Wrapper for function 0x07(read void status) in the MODBUS specification.    ////
////                                                                                  ////
////  exception modbus_diagnostics(address,sub_func,data)                             ////
////    - Wrapper for function 0x08(diagnostics) in the MODBUS specification.         ////
////                                                                                  ////
////  exception modbus_get_comm_event_counter(address)                                ////
////    - Wrapper for function 0x0B(get comm event count) in the MODBUS specification.////
////                                                                                  ////
////  exception modbus_get_comm_event_log(address)                                    ////
////    - Wrapper for function 0x0C(get comm event log) in the MODBUS specification.  ////
////                                                                                  ////
////  exception modbus_write_multiple_coils(address,start_address,quantity,*values)   ////
////    - Wrapper for function 0x0F(write multiple coils) in the MODBUS specification.////
////    - Special Note: values is a pointer to an int8 array, each byte represents 8  ////
////                    coils.                                                        ////
////                                                                                  ////
////  exception modbus_write_multiple_registers(address,start_address,quantity,*values)///
////    - Wrapper for function 0x10(write multiple regs) in the MODBUS specification. ////
////    - Special Note: values is a pointer to an int8 array                          ////
////                                                                                  ////
////  exception modbus_report_slave_id(address)                                       ////
////    - Wrapper for function 0x11(report slave id) in the MODBUS specification.     ////
////                                                                                  ////
////  exception modbus_read_file_record(address,byte_count,*request)                  ////
////    - Wrapper for function 0x14(read file record) in the MODBUS specification.    ////
////                                                                                  ////
////  exception modbus_write_file_record(address,byte_count,*request)                 ////
////    - Wrapper for function 0x15(write file record) in the MODBUS specification.   ////
////                                                                                  ////
////  exception modbus_mask_write_register(address,reference_address,AND_mask,OR_mask)////
////    - Wrapper for function 0x16(read coils) in the MODBUS specification.          ////
////                                                                                  ////
////  exception modbus_read_write_multiple_registers(address,read_start,read_quantity,////
////                            write_start,write_quantity, *write_registers_value)   ////
////    - Wrapper for function 0x17(read write mult regs) in the MODBUS specification.////
////                                                                                  ////
////  exception modbus_read_FIFO_queue(address,FIFO_address)                          ////
////    - Wrapper for function 0x18(read FIFO queue) in the MODBUS specification.     ////
////                                                                                  ////
////                                                                                  ////
//// Slave API:                                                                       ////
////                                                                                  ////
////  void modbus_read_coils_rsp(address,byte_count,*coil_data)                       ////
////    - Wrapper to respond to 0x01(read coils) in the MODBUS specification.         ////
////                                                                                  ////
////  void modbus_read_discrete_input_rsp(address,byte_count,*input_data)             ////
////    - Wrapper to respond to 0x02(read discret input) in the MODBUS specification. ////
////                                                                                  ////
////  void modbus_read_holding_registers_rsp(address,byte_count,*reg_data)            ////
////    - Wrapper to respond to 0x03(read holding regs) in the MODBUS specification.  ////
////                                                                                  ////
////  void modbus_read_input_registers_rsp(address,byte_count,*input_data)            ////
////    - Wrapper to respond to 0x04(read input regs) in the MODBUS specification.    ////
////                                                                                  ////
////  void modbus_write_single_coil_rsp(address,output_address,output_value)          ////
////    - Wrapper to respond to 0x05(write single coil) in the MODBUS specification.  ////
////                                                                                  ////
////  void modbus_write_single_register_rsp(address,reg_address,reg_value)            ////
////    - Wrapper to respond to 0x06(write single reg) in the MODBUS specification.   ////
////                                                                                  ////
////  void modbus_read_exception_status_rsp(address, data)                            ////
////    - Wrapper to respond to 0x07(read void status) in the MODBUS specification.   ////
////                                                                                  ////
////  void modbus_diagnostics_rsp(address,sub_func,data)                              ////
////    - Wrapper to respond to 0x08(diagnostics) in the MODBUS specification.        ////
////                                                                                  ////
////  void modbus_get_comm_event_counter_rsp(address,status,event_count)              ////
////    - Wrapper to respond to 0x0B(get comm event count) in the MODBUS specification////
////                                                                                  ////
////  void modbus_get_comm_event_log_rsp(address,status,event_count,message_count,    ////
////                                   *events, events_len)                           ////
////    - Wrapper to respond to 0x0C(get comm event log) in the MODBUS specification. ////
////                                                                                  ////
////  void modbus_write_multiple_coils_rsp(address,start_address,quantity)            ////
////    - Wrapper to respond to 0x0F(write multiple coils) in the MODBUS specification////
////                                                                                  ////
////  void modbus_write_multiple_registers_rsp(address,start_address,quantity)        ////
////    - Wrapper to respond to 0x10(write multiple regs) in the MODBUS specification.////
////                                                                                  ////
////  void modbus_report_slave_id_rsp(address,slave_id,run_status,*data,data_len)     ////
////    - Wrapper to respond to 0x11(report slave id) in the MODBUS specification.    ////
////                                                                                  ////
////  void modbus_read_file_record_rsp(address,byte_count,*request)                   ////
////    - Wrapper to respond to 0x14(read file record) in the MODBUS specification.   ////
////                                                                                  ////
////  void modbus_write_file_record_rsp(address,byte_count,*request)                  ////
////    - Wrapper to respond to 0x15(write file record) in the MODBUS specification.  ////
////                                                                                  ////
////  void modbus_mask_write_register_rsp(address,reference_address,AND_mask,OR_mask) ////
////    - Wrapper to respond to 0x16(read coils) in the MODBUS specification.         ////
////                                                                                  ////
////  void modbus_read_write_multiple_registers_rsp(address,*data,data_len)           ////
////    - Wrapper to respond to 0x17(read write mult regs) in the MODBUS specification////
////                                                                                  ////
////  void modbus_read_FIFO_queue_rsp(address,FIFO_len,*data)                         ////
////    - Wrapper to respond to 0x18(read FIFO queue) in the MODBUS specification.    ////
////                                                                                  ////
////  void modbus_exception_rsp(int8 address, int16 func, exception error)            ////
////    - Wrapper to send an exception response.  See exception list below.           ////
////                                                                                  ////
//// Exception List:                                                                  ////
////  ILLEGAL_FUNCTION, ILLEGAL_DATA_ADDRESS, ILLEGAL_DATA_VALUE,                     ////
////  SLAVE_DEVICE_FAILURE, ACKNOWLEDGE, SLAVE_DEVICE_BUSY, MEMORY_PARITY_ERROR,      ////
////  GATEWAY_PATH_UNAVAILABLE, GATEWAY_TARGET_NO_RESPONSE                            ////
////                                                                                  ////
//////////////////////////////////////////////////////////////////////////////////////////
////                (C) Copyright 1996, 2006 Custom Computer Services                 ////
////        This source code may only be used by licensed users of the CCS            ////
////        C compiler.  This source code may only be distributed to other            ////
////        licensed users of the CCS C compiler.  No other use,                      ////
////        reproduction or distribution is permitted without written                 ////
////        permission.  Derivative programs created using this software              ////
////        in object code form are not restricted in any way.                        ////
//////////////////////////////////////////////////////////////////////////////////////////
#include <xc.h>
//#include <stdbool.h>
//#include <stdint.h>
#include "modbus.h"

#include "mcc_generated_files/tmr2.h"
#include "mcc_generated_files/eusart1.h"
#include "mcc_generated_files/pin_manager.h"


// Defines  /////////////////////////////////////
#define lowByte(x)     ((uint8_t)((x)&0xFF))
#define highByte(x)    ((uint8_t)(((x)>>8)&0xFF))

/*****************************************************************
 * Stages of MODBUS reception.  Used to keep our ISR fast enough.
 *
 ***************************************************************/

enum {
    MODBUS_GETADDY=0, MODBUS_GETFUNC=1, MODBUS_GETDATA=2
} modbus_serial_state = 0;

/* Table of CRC values for high�order byte */
const unsigned char modbus_auchCRCHi[] = {
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,
0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40
};

/* Table of CRC values for low�order byte */
const char modbus_auchCRCLo[]= {
0x00,0xC0,0xC1,0x01,0xC3,0x03,0x02,0xC2,0xC6,0x06,0x07,0xC7,0x05,0xC5,0xC4,
0x04,0xCC,0x0C,0x0D,0xCD,0x0F,0xCF,0xCE,0x0E,0x0A,0xCA,0xCB,0x0B,0xC9,0x09,
0x08,0xC8,0xD8,0x18,0x19,0xD9,0x1B,0xDB,0xDA,0x1A,0x1E,0xDE,0xDF,0x1F,0xDD,
0x1D,0x1C,0xDC,0x14,0xD4,0xD5,0x15,0xD7,0x17,0x16,0xD6,0xD2,0x12,0x13,0xD3,
0x11,0xD1,0xD0,0x10,0xF0,0x30,0x31,0xF1,0x33,0xF3,0xF2,0x32,0x36,0xF6,0xF7,
0x37,0xF5,0x35,0x34,0xF4,0x3C,0xFC,0xFD,0x3D,0xFF,0x3F,0x3E,0xFE,0xFA,0x3A,
0x3B,0xFB,0x39,0xF9,0xF8,0x38,0x28,0xE8,0xE9,0x29,0xEB,0x2B,0x2A,0xEA,0xEE,
0x2E,0x2F,0xEF,0x2D,0xED,0xEC,0x2C,0xE4,0x24,0x25,0xE5,0x27,0xE7,0xE6,0x26,
0x22,0xE2,0xE3,0x23,0xE1,0x21,0x20,0xE0,0xA0,0x60,0x61,0xA1,0x63,0xA3,0xA2,
0x62,0x66,0xA6,0xA7,0x67,0xA5,0x65,0x64,0xA4,0x6C,0xAC,0xAD,0x6D,0xAF,0x6F,
0x6E,0xAE,0xAA,0x6A,0x6B,0xAB,0x69,0xA9,0xA8,0x68,0x78,0xB8,0xB9,0x79,0xBB,
0x7B,0x7A,0xBA,0xBE,0x7E,0x7F,0xBF,0x7D,0xBD,0xBC,0x7C,0xB4,0x74,0x75,0xB5,
0x77,0xB7,0xB6,0x76,0x72,0xB2,0xB3,0x73,0xB1,0x71,0x70,0xB0,0x50,0x90,0x91,
0x51,0x93,0x53,0x52,0x92,0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9C,0x5C,
0x5D,0x9D,0x5F,0x9F,0x9E,0x5E,0x5A,0x9A,0x9B,0x5B,0x99,0x59,0x58,0x98,0x88,
0x48,0x49,0x89,0x4B,0x8B,0x8A,0x4A,0x4E,0x8E,0x8F,0x4F,0x8D,0x4D,0x4C,0x8C,
0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,0x43,0x83,0x41,0x81,0x80,
0x40
};

bool modbus_serial_new=0;
modbus_rx_t modbus_rx;

uint32_t modbus_serial_wait=MODBUS_SERIAL_TIMEOUT;

/*
#if (MODBUS_SERIAL_INT_SOURCE != MODBUS_INT_EXT)
#byte TXSTA=getenv("sfr:TXSTA")
#bit TRMT=TXSTA.1
*/

void WAIT_FOR_HW_BUFFER(void) {
    while(!TRMT);
    return;
}   
//#endif

// Purpose:    Enable data reception
// Inputs:     None
// Outputs:    None
void RCV_ON(void){
#if (MODBUS_SERIAL_INT_SOURCE!=MODBUS_INT_EXT) // si no esta definido interrupcion por pin serial sincronico por defecto
    //while(kbhit(MODBUS_SERIAL)) {getc();}  //Clear RX buffer. Clear RDA interrupt flag. Clear overrun error flag.
    while(EUSART1_is_rx_ready()) EUSART1_Read(); //Clear RX buffer. Clear RDA interrupt flag. Clear overrun error flag.    
    #if (MODBUS_SERIAL_INT_SOURCE==MODBUS_INT_RDA)
    //  clear_interrupt(INT_RDA);
        PIR1bits.RC1IF = 0;
    #else
    //  clear_interrupt(INT_RDA2); //si hay segunda eusart
    #endif

    #if (MODBUS_SERIAL_RX_ENABLE!=0) 
       //output_low(MODBUS_SERIAL_RX_ENABLE);
       nRE_SetLow();
    #endif
        

#else
      //clear_interrupt(INT_EXT); //cuidado si uso otro periferico tengo 3 fuentes para gama alta de pic18
      INTCONbits_t.INT0F=0;  
      //ext_int_edge(H_TO_L);
      INTCON2bits_t.INTEDG0=0;

      #if (MODBUS_SERIAL_RX_ENABLE!=0) 
         //output_low(MODBUS_SERIAL_RX_ENABLE);
       nRE_SetLow();
      #endif      
      
      //enable_interrupts(INT_EXT);
      INTCONbits_t.INT0IE=1;         

#endif         
    return;
}

#if( MODBUS_SERIAL_INT_SOURCE == MODBUS_asynchronous )

void RCV_OFF(void) {
    
    INTCONbits.INT0E=0;
    nRE_SetHigh(); 
    return;
}

#endif

// Purpose:    Initialize RS485 communication. Call this before
//             using any other RS485 functions.
// Inputs:     None
// Outputs:    None
void modbus_init(void)
{
   //output_low(MODBUS_SERIAL_ENABLE_PIN); // DE es activo alto. inhibe salida por DE
   DE_SetLow();// DE es activo alto. inhibe salida por DE
   RCV_ON();// HABILIRA PARA LA RECEPCION
   
   
   //setup_timer_2(T2_DIV_BY_16,249,5);   //~4ms interrupts
   TMR2_Initialize(); // POR MCC ~4ms interrupts
   //enable_interrupts(GLOBAL); 
   //INTERRUPT_GlobalInterruptEnable();//POR MCC
   INTCONbits.GIE = 1;
   return;
}

//------------------------------------------------------------------------------
// Purpose:    MODBUS_SERIAL_RESPONSE
// Inputs:     Enable, used to asynchronous control state comunication modbus
// Outputs:    exception
//------------------------------------------------------------------------------
exception MODBUS_SERIAL_RESPONSE(uint8_t address,function func ){
        
    if (modbus_serial_new && (address == modbus_rx.address) && (modbus_rx.func == func)){
        modbus_serial_wait = MODBUS_SERIAL_TIMEOUT;
        return (ACKNOWLEDGE);    
    }
    
    //kbhit: si modbus_serial_new es true entrega el tipo de error, si existe, segun la fun
    //modbus_serial_new es false su salida en false y permite ir decrementando el TIMEOUT
    if(!modbus_kbhit() && --modbus_serial_wait)
    { 
        /*debo colococar aqui una salidad por puerto para saber cada cuento hay ingreso y ajustar el 
         time out para que sea baudtime por trama enviada + tara recepcion+ procesamiento*/   
        if(modbus_serial_wait<MODBUS_SERIAL_TIMEOUT_inf) //evito que sea negativo
            modbus_rx.error=TIMEOUT;
        return(TIMEOUT);
    }
    
    return(SLAVE_DEVICE_BUSY);
}
//------------------------------------------------------------------------------
// Purpose:    Start our timeout timer
// Inputs:     Enable, used to turn timer on/off
// Outputs:    None
//------------------------------------------------------------------------------
void modbus_enable_timeout(bool enable)
{
   //disable_interrupts(INT_TIMER2);
   PIE1bits.TMR2IE = 0;
   if (enable) {
      //set_timer2(0);
      TMR2_WriteTimer(0); 
      //clear_interrupt(INT_TIMER2);
      PIR1bits.TMR2IF=0;
      //enable_interrupts(INT_TIMER2);
      PIE1bits.TMR2IE=1;
   }
}
//------------------------------------------------------------------------------
// SOBRE LA INT_TMR2 SE CORRE ESTA RUTINA
// Purpose:    Check if we have timed out waiting for a response
// Inputs:     None
// Outputs:    None
//#int_timer2
//-------se aplica cada time out int de TMR2
void modbus_timeout_now(void)
{
   if((modbus_serial_state == MODBUS_GETDATA) && (modbus_serial_crc.d == 0x0000) && (!modbus_serial_new))
   {
      modbus_rx.len-=2; // elimino los dos ultimos elementos por ser el CRC quedan solo datos
      modbus_serial_new=true;
   }
   else
      modbus_serial_new=false;

   modbus_serial_crc.d=0xFFFF;
   modbus_serial_state=MODBUS_GETADDY;
   modbus_enable_timeout(false); // NO INT timer2 
}
//------------------------------------------------------------------------------
// Purpose:    Calculate crc of data and updates global crc
// Inputs:     Character
// Outputs:    None
void modbus_calc_crc(char data)
{
  uint8_t uIndex ; // will index into CRC lookup table

  uIndex = (uint8_t)(modbus_serial_crc.b[1] ^ data); // calculate the CRC
  modbus_serial_crc.b[1] = (uint8_t)(modbus_serial_crc.b[0] ^ modbus_auchCRCHi[uIndex]);
  modbus_serial_crc.b[0] = modbus_auchCRCLo[uIndex];
}

// Purpose:    Puts a character onto the serial line
// Inputs:     Character
// Outputs:    None
void modbus_serial_putc(uint8_t c)
{
   EUSART1_Write(c);
   modbus_calc_crc(c);
   __delay_us(MODBUS_stop_bit); // 4 char stop bit.  not exact
}

// Purpose:    Send a message over the RS485 bus
// Inputs:     1) The destination address
//             2) The number of bytes of data to send
//             3) A pointer to the data to send
//             4) The length of the data
// Outputs:    TRUE if successful
//             FALSE if failed
// Note:       Format:  source | destination | data-length | data | checksum
void modbus_serial_send_start(uint8_t to, uint8_t func)
{
   modbus_serial_crc.d=0xFFFF;
   modbus_serial_new=false;

   RCV_OFF();
   
#if (MODBUS_SERIAL_ENABLE_PIN!=0) 
   //output_high(MODBUS_SERIAL_ENABLE_PIN);
   DE_SetHigh();
#endif

   __delay_us(MODBUS_character_delay); //3.5 character delay

   modbus_serial_putc(to);
   modbus_serial_putc(func);
}
//------------------------------------------------------------------------------
// Purpose:   Interrupt service routine for handling incoming serial data
// Inputs:    None
// Outputs:   None
#if (MODBUS_SERIAL_INT_SOURCE==MODBUS_INT_RDA)
//#int_rda
PIE1bits_t.RC1IE= 1;
#elif (MODBUS_SERIAL_INT_SOURCE==MODBUS_INT_RDA2)
//#int_rda2
//PIE1bits_t.RC2IE= 1; //SI EXISTE
#elif (MODBUS_SERIAL_INT_SOURCE==MODBUS_INT_EXT)
//#int_ext
INTCONbits_t.INT0E=1;
#elif( MODBUS_SERIAL_INT_SOURCE == MODBUS_asynchronous )
//voy analizando sobre un estado independiente de la llegada de los datos
// PIE1bits.RC1IE=0;//.RC1IE=(bit)0;
//PIE1bits_t.RC2IE= 1; //SI EXISTE
#else
//#error Please define a correct interrupt source
#endif
void incomming_modbus_serial(void) {
   char c;

   c= EUSART1_Read();

   if (!modbus_serial_new)
   {
      if(modbus_serial_state == MODBUS_GETADDY)                                 //ARmado de trama
      {
         modbus_serial_crc.d = 0xFFFF;
         modbus_rx.address = c;
         modbus_serial_state++;
         modbus_rx.len = 0;
         modbus_rx.error=0;
      }
      else if(modbus_serial_state == MODBUS_GETFUNC)
      {
         modbus_rx.func = c;
         modbus_serial_state++;
      }
      else if(modbus_serial_state == MODBUS_GETDATA)
      {  //por si existe desbordamiento
         if (modbus_rx.len>=MODBUS_SERIAL_RX_BUFFER_SIZE) {modbus_rx.len=MODBUS_SERIAL_RX_BUFFER_SIZE-1;} //verificar tal vez decremento y la segunda linea no se corrige!!!!!!
         modbus_rx.data[modbus_rx.len]=c;
         modbus_rx.len++; //cuenta sobre los datos no sobre la trama
      }
                                                                                //Requiero Bist de STop????
      modbus_calc_crc(c);
      modbus_enable_timeout(true);
   }
}
//------------------------------------------------------------------------------


void modbus_serial_send_stop(void)
{
   uint8_t crc_low, crc_high;

   crc_high=modbus_serial_crc.b[1];
   crc_low=modbus_serial_crc.b[0];

   modbus_serial_putc(crc_high);
   modbus_serial_putc(crc_low);

#if (MODBUS_SERIAL_INT_SOURCE!=MODBUS_INT_EXT)
   WAIT_FOR_HW_BUFFER();
#endif
    
   __delay_us(MODBUS_character_delay); //3.5 character delay

   RCV_ON();

#if (MODBUS_SERIAL_ENABLE_PIN!=0) 
   //output_low(MODBUS_SERIAL_ENABLE_PIN);
   DE_SetLow();
#endif

   modbus_serial_crc.d=0xFFFF;
}

// Purpose:    Get a message from the RS485 bus and store it in a buffer
// Inputs:     None
// Outputs:    TRUE if a message was received
//             FALSE if no message is available
// Note:       Data will be filled in at the modbus_rx struct:
bool modbus_kbhit(void)
{
   if(!modbus_serial_new)
      return false;
   else if(modbus_rx.func & 0x80)           //did we receive an error?
   {
      modbus_rx.error = modbus_rx.data[0];  //if so grab the error and return true
      modbus_rx.len = 1;
   }
   modbus_serial_new=false;
   return true;
}

#if (MODBUS_TYPE==MODBUS_TYPE_MASTER)


/********************************************************************
The following functions are defined in the MODBUS protocol.  Please
refer to http://www.modbus.org for the purpose of each of these.
All functions take the slaves address as their first parameter.
Each function returns the exception code received from the response.
The function will return 0 if there were no errors in transmission.
********************************************************************/




void MODBUS_SERIAL_WAIT_FOR_RESPONSE(uint8_t address)
{
    if(address)
    {
        while(!modbus_kbhit() && --modbus_serial_wait)
            __delay_us(1);
        if(!modbus_serial_wait)
            modbus_rx.error=TIMEOUT;
    }
    modbus_serial_wait = MODBUS_SERIAL_TIMEOUT;
    return;
}


/*******************************************************************************
*read_coils
*Input:     int8       address            Slave Address
*           int16      start_address      Address to start reading from
*           int16      quantity           Amount of addresses to read
*Output:    exception                     0 if no error, else the exception
********************************************************************************/
exception modbus_read_coils(uint8_t address, uint16_t start_address, uint16_t quantity)
{
   modbus_serial_send_start(address, FUNC_READ_COILS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_send_stop();
   #if MODBUS_SERIAL_INT_SOURCE!=MODBUS_asynchronous
   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);//salgo positivo si modbus_serial_new=true de lo contrario ERROR
   #endif 
   

   return modbus_rx.error;
}

/*
read_discrete_input
Input:     int8       address            Slave Address
           int16      start_address      Address to start reading from
           int16      quantity           Amount of addresses to read
Output:    exception                     0 if no error, else the exception
*/
exception modbus_read_discrete_input(uint8_t address, uint16_t start_address, uint16_t quantity)
{
   modbus_serial_send_start(address, FUNC_READ_DISCRETE_INPUT);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_send_stop();
      
   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
read_holding_registers
Input:     int8       address            Slave Address
           int16      start_address      Address to start reading from
           int16      quantity           Amount of addresses to read
Output:    exception                     0 if no error, else the exception
*/
exception modbus_read_holding_registers(uint8_t address, uint16_t start_address, uint16_t quantity)
{
   modbus_serial_send_start(address, FUNC_READ_HOLDING_REGISTERS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_send_stop();
      
   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
read_input_registers
Input:     int8       address            Slave Address
           int16      start_address      Address to start reading from
           int16      quantity           Amount of addresses to read
Output:    exception                     0 if no error, else the exception
*/
exception modbus_read_input_registers(uint8_t address, uint16_t start_address, uint16_t quantity)
{
   modbus_serial_send_start(address, FUNC_READ_INPUT_REGISTERS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_send_stop();
   
   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
write_single_coil
Input:     int8       address            Slave Address
           int16      output_address     Address to write into
           int1       on                 true for on, false for off
Output:    exception                     0 if no error, else the exception
*/
exception modbus_write_single_coil(uint8_t address, uint16_t output_address, bool on)
{
   modbus_serial_send_start(address, FUNC_WRITE_SINGLE_COIL);

   modbus_serial_putc(highByte(output_address));
   modbus_serial_putc(lowByte(output_address));

   if(on)
       modbus_serial_putc(0xFF);
   else
       modbus_serial_putc(0x00);
   
   modbus_serial_putc(0x00);

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
write_single_register
Input:     int8       address            Slave Address
           int16      reg_address        Address to write into
           int16      reg_value          Value to write
Output:    exception                     0 if no error, else the exception
*/
exception modbus_write_single_register(uint8_t address, uint16_t reg_address, uint16_t reg_value)
{
   modbus_serial_send_start(address, FUNC_WRITE_SINGLE_REGISTER);

   modbus_serial_putc(highByte(reg_address));
   modbus_serial_putc(lowByte(reg_address));

   modbus_serial_putc(highByte(reg_value));
   modbus_serial_putc(lowByte(reg_value));

   modbus_serial_send_stop();
   
   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
read_exception_status
Input:     int8       address            Slave Address
Output:    exception                     0 if no error, else the exception
*/
exception modbus_read_exception_status(uint8_t address)
{
   modbus_serial_send_start(address, FUNC_READ_EXCEPTION_STATUS);
   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
diagnostics
Input:     int8       address            Slave Address
           int16      sub_func           Subfunction to send
           int16      data               Data to send, changes based on subfunction
Output:    exception                     0 if no error, else the exception
*/
exception modbus_diagnostics(uint8_t address, uint16_t sub_func, uint16_t data)
{
   modbus_serial_send_start(address, FUNC_DIAGNOSTICS);

   modbus_serial_putc(highByte(sub_func));
   modbus_serial_putc(lowByte(sub_func));

   modbus_serial_putc(highByte(data));
   modbus_serial_putc(lowByte(data));

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
get_comm_event_couter
Input:     int8       address            Slave Address
Output:    exception                     0 if no error, else the exception
*/
exception modbus_get_comm_event_counter(uint8_t address)
{
   modbus_serial_send_start(address, FUNC_GET_COMM_EVENT_COUNTER);
   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
get_comm_event_log
Input:     int8       address            Slave Address
Output:    exception                     0 if no error, else the exception
*/
exception modbus_get_comm_event_log(uint8_t address)
{
   modbus_serial_send_start(address, FUNC_GET_COMM_EVENT_LOG);
   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
write_multiple_coils
Input:     int8       address            Slave Address
           int16      start_address      Address to start at
           int16      quantity           Amount of coils to write to
           int1*      values             A pointer to an array holding the values to write
Output:    exception                     0 if no error, else the exception
*/


exception modbus_write_multiple_coils(uint8_t address, uint16_t start_address, uint16_t quantity,uint8_t *values)
{
   uint8_t i,count;
   
   count = (uint8_t)((quantity/8));
   
   if(quantity/8)
      count++;      

   modbus_serial_send_start(address, FUNC_WRITE_MULTIPLE_COILS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_putc(count);

   for(i=0; i < count; ++i) 
      modbus_serial_putc(values[i]);

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
write_multiple_registers
Input:     int8       address            Slave Address
           int16      start_address      Address to start at
           int16      quantity           Amount of coils to write to
           int16*     values             A pointer to an array holding the data to write
Output:    exception                     0 if no error, else the exception
*/

exception modbus_write_multiple_registers(uint8_t address, uint16_t start_address, uint16_t quantity,uint16_t *values)
{
   uint8_t i,count;
   
   count = quantity*2;

   modbus_serial_send_start(address, FUNC_WRITE_MULTIPLE_REGISTERS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));
   
   modbus_serial_putc(count);

   for(i=0; i < quantity; ++i)
   {
      modbus_serial_putc(highByte(values[i]));
      modbus_serial_putc(lowByte(values[i]));
   }

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}


/*
report_slave_id
Input:     int8       address            Slave Address
Output:    exception                     0 if no error, else the exception
*/
exception modbus_report_slave_id(uint8_t address)
{
   modbus_serial_send_start(address, FUNC_REPORT_SLAVE_ID);
   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
read_file_record
Input:     int8                address            Slave Address
           int8                byte_count         Number of bytes to read
           read_sub_request*   request            Structure holding record information
Output:    exception                              0 if no error, else the exception
*/
exception modbus_read_file_record(uint8_t address, uint8_t byte_count,modbus_read_sub_request *request)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_FILE_RECORD);

   modbus_serial_putc(byte_count);

   for(i=0; i < (byte_count/7); i+=7)
   {
      modbus_serial_putc(request->reference_type);
      modbus_serial_putc(highByte(request->file_number));
      modbus_serial_putc(lowByte(request->file_number));
      modbus_serial_putc(highByte(request->record_number));
      modbus_serial_putc(lowByte(request->record_number));
      modbus_serial_putc(highByte(request->record_length));
      modbus_serial_putc(lowByte(request->record_length));
      request++;
   }

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
write_file_record
Input:     int8                address            Slave Address
           int8                byte_count         Number of bytes to read
           read_sub_request*   request            Structure holding record/data information
Output:    exception                              0 if no error, else the exception
*/
exception modbus_write_file_record(uint8_t address, uint8_t byte_count,modbus_write_sub_request *request)
{
   uint8_t i, j=0;

   modbus_serial_send_start(address, FUNC_WRITE_FILE_RECORD);

   modbus_serial_putc(byte_count);

   for(i=0; i < byte_count; i+=(7+(j*2)))
   {
      modbus_serial_putc(request->reference_type);
      modbus_serial_putc(highByte(request->file_number));
      modbus_serial_putc(lowByte(request->file_number));
      modbus_serial_putc(highByte(request->record_number));
      modbus_serial_putc(lowByte(request->record_number));
      modbus_serial_putc(highByte(request->record_length));
      modbus_serial_putc(lowByte(request->record_length));

      for(j=0; (j < request->record_length) && 
            (j < MODBUS_SERIAL_RX_BUFFER_SIZE-8); j+=2)
      {
         modbus_serial_putc(highByte(request->data[j]));
         modbus_serial_putc(lowByte(request->data[j]));
      }
      request++;
   }

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
mask_write_register
Input:     int8       address            Slave Address
           int16      reference_address  Address to mask
           int16      AND_mask           A mask to AND with the data at reference_address
           int16      OR_mask            A mask to OR with the data at reference_address
Output:    exception                              0 if no error, else the exception
*/
exception modbus_mask_write_register(uint8_t address, uint16_t reference_address,uint16_t AND_mask, uint16_t OR_mask)
{
   modbus_serial_send_start(address, FUNC_MASK_WRITE_REGISTER);

   modbus_serial_putc(highByte(reference_address));
   modbus_serial_putc(lowByte(reference_address));

   modbus_serial_putc(highByte(AND_mask));
   modbus_serial_putc(lowByte(AND_mask));

   modbus_serial_putc(highByte(OR_mask));
   modbus_serial_putc(lowByte(OR_mask));

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
read_write_multiple_registers
Input:     int8       address                Slave Address
           int16      read_start             Address to start reading
           int16      read_quantity          Amount of registers to read
           int16      write_start            Address to start writing
           int16      write_quantity         Amount of registers to write
           int16*     write_registers_value  Pointer to an aray us to write
Output:    exception                         0 if no error, else the exception
*/
exception modbus_read_write_multiple_registers(uint8_t address, uint16_t read_start,
                                    uint16_t read_quantity, uint16_t write_start,
                                    uint16_t write_quantity,
                                    uint16_t *write_registers_value)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_WRITE_MULTIPLE_REGISTERS);

   modbus_serial_putc(highByte(read_start));
   modbus_serial_putc(lowByte(read_start));

   modbus_serial_putc(highByte(read_quantity));
   modbus_serial_putc(lowByte(read_quantity));

   modbus_serial_putc(highByte(write_start));
   modbus_serial_putc(lowByte(write_start));

   modbus_serial_putc(highByte(write_quantity));
   modbus_serial_putc(lowByte(write_quantity));

   modbus_serial_putc((uint8_t)(2*write_quantity));

   for(i=0; i < write_quantity ; i+=2)
   {
      modbus_serial_putc(highByte(write_registers_value[i]));
      modbus_serial_putc(lowByte(write_registers_value[i+1]));
   }

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

/*
read_FIFO_queue
Input:     int8       address           Slave Address
           int16      FIFO_address      FIFO address
Output:    exception                    0 if no error, else the exception
*/
exception modbus_read_FIFO_queue(uint8_t address, uint16_t FIFO_address)
{
   modbus_serial_send_start(address, FUNC_READ_FIFO_QUEUE);

   modbus_serial_putc(highByte(FIFO_address));
   modbus_serial_putc(lowByte(FIFO_address));

   modbus_serial_send_stop();

   MODBUS_SERIAL_WAIT_FOR_RESPONSE(address);

   return modbus_rx.error;
}

#else  /*MODBUS Slave Functions*/

/********************************************************************
The following slave functions are defined in the MODBUS protocol.
Please refer to http://www.modbus.org for the purpose of each of
these.  All functions take the slaves address as their first
parameter.
********************************************************************/

/*
read_coils_rsp
Input:     int8       address            Slave Address
           int8       byte_count         Number of bytes being sent
           int8*      coil_data          Pointer to an array of data to send
Output:    void
*/
void modbus_read_coils_rsp(uint8_t address, uint8_t byte_count, uint8_t* coil_data)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_COILS);

   modbus_serial_putc(byte_count);

   for(i=0; i < byte_count; ++i)
   {
      modbus_serial_putc(*coil_data);
      coil_data++;
   }

   modbus_serial_send_stop();
}

/*
read_discrete_input_rsp
Input:     int8       address            Slave Address
           int8       byte_count         Number of bytes being sent
           int8*      input_data         Pointer to an array of data to send
Output:    void
*/
void modbus_read_discrete_input_rsp(uint8_t address, uint8_t byte_count, 
                                    uint8_t *input_data)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_DISCRETE_INPUT);

   modbus_serial_putc(byte_count);

   for(i=0; i < byte_count; ++i)
   {
      modbus_serial_putc(*input_data);
      input_data++;
   }

   modbus_serial_send_stop();
}

/*
read_holding_registers_rsp
Input:     int8       address            Slave Address
           int8       byte_count         Number of bytes being sent
           int8*      reg_data           Pointer to an array of data to send
Output:    void
*/
void modbus_read_holding_registers_rsp(uint8_t address, uint8_t byte_count, 
                                        uint8_t *reg_data)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_HOLDING_REGISTERS);

   modbus_serial_putc(byte_count);

   for(i=0; i < byte_count; ++i)
   {
      modbus_serial_putc(*reg_data);
      reg_data++;
   }

   modbus_serial_send_stop();
}

/*
read_input_registers_rsp
Input:     int8       address            Slave Address
           int8       byte_count         Number of bytes being sent
           int8*      input_data         Pointer to an array of data to send
Output:    void
*/
void modbus_read_input_registers_rsp(uint8_t address, uint8_t byte_count, 
                                        uint8_t *input_data)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_INPUT_REGISTERS);

   modbus_serial_putc(byte_count);

   for(i=0; i < byte_count; ++i)
   {
      modbus_serial_putc(*input_data);
      input_data++;
   }

   modbus_serial_send_stop();
}

/*
write_single_coil_rsp
Input:     int8       address            Slave Address
           uint16_t      output_address     Echo of output address received
           int16      output_value       Echo of output value received
Output:    void
*/
void modbus_write_single_coil_rsp(uint8_t address, uint16_t output_address, 
                                    uint16_t output_value)
{
   modbus_serial_send_start(address, FUNC_WRITE_SINGLE_COIL);

   modbus_serial_putc(highByte(output_address));
   modbus_serial_putc(lowByte(output_address));

   modbus_serial_putc(highByte(output_value));
   modbus_serial_putc(lowByte(output_value));

   modbus_serial_send_stop();
}

/*
write_single_register_rsp
Input:     int8       address            Slave Address
           int16      reg_address        Echo of register address received
           int16      reg_value          Echo of register value received
Output:    void
*/
void modbus_write_single_register_rsp(uint8_t address, uint16_t reg_address, 
                                        uint16_t reg_value)
{
   modbus_serial_send_start(address, FUNC_WRITE_SINGLE_REGISTER);

   modbus_serial_putc(highByte(reg_address));
   modbus_serial_putc(lowByte(reg_address));

   modbus_serial_putc(highByte(reg_value));
   modbus_serial_putc(lowByte(reg_value));

   modbus_serial_send_stop();
}

/*
read_exception_status_rsp
Input:     int8       address            Slave Address
Output:    void
*/
void modbus_read_exception_status_rsp(uint8_t address, uint8_t data)
{
   modbus_serial_send_start(address, FUNC_READ_EXCEPTION_STATUS);
   modbus_serial_send_stop();
}

/*
diagnostics_rsp
Input:     int8       address            Slave Address
           int16      sub_func           Echo of sub function received
           int16      data               Echo of data received
Output:    void
*/
void modbus_diagnostics_rsp(uint8_t address, uint16_t sub_func, uint16_t data)
{
   modbus_serial_send_start(address, FUNC_DIAGNOSTICS);

   modbus_serial_putc(highByte(sub_func));
   modbus_serial_putc(lowByte(sub_func));

   modbus_serial_putc(highByte(data));
   modbus_serial_putc(lowByte(data));

   modbus_serial_send_stop();
}

/*
get_comm_event_counter_rsp
Input:     int8       address            Slave Address
           int16      status             Status, refer to MODBUS documentation
           int16      event_count        Count of events
Output:    void
*/
void modbus_get_comm_event_counter_rsp(uint8_t address, uint16_t status, 
                                        uint16_t event_count)
{
   modbus_serial_send_start(address, FUNC_GET_COMM_EVENT_COUNTER);

   modbus_serial_putc(highByte(status));
   modbus_serial_putc(lowByte(status));

   modbus_serial_putc(highByte(event_count));
   modbus_serial_putc(lowByte(event_count));

   modbus_serial_send_stop();
}

/*
get_comm_event_counter_rsp
Input:     int8       address            Slave Address
           int16      status             Status, refer to MODBUS documentation
           int16      event_count        Count of events
           int16      message_count      Count of messages
           int8*      events             Pointer to event data
           int8       events_len         Length of event data in bytes
Output:    void
*/
void modbus_get_comm_event_log_rsp(uint8_t address, uint16_t status,
                                    uint16_t event_count, uint16_t message_count, 
                                    uint8_t *events, uint8_t events_len)
{
   uint8_t i;
    
   modbus_serial_send_start(address, FUNC_GET_COMM_EVENT_LOG);

   modbus_serial_putc(events_len+6);

   modbus_serial_putc(highByte(status));
   modbus_serial_putc(lowByte(status));

   modbus_serial_putc(highByte(event_count));
   modbus_serial_putc(lowByte(event_count));

   modbus_serial_putc(highByte(message_count));
   modbus_serial_putc(lowByte(message_count));

   for(i=0; i < events_len; ++i)
   {
      modbus_serial_putc(*events);
      events++;
   }

   modbus_serial_send_stop();
}

/*
write_multiple_coils_rsp
Input:     int8       address            Slave Address
           int16      start_address      Echo of address to start at
           int16      quantity           Echo of amount of coils written to
Output:    void
*/
void modbus_write_multiple_coils_rsp(uint8_t address, uint16_t start_address, 
                                        uint16_t quantity)
{
   modbus_serial_send_start(address, FUNC_WRITE_MULTIPLE_COILS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_send_stop();
}

/*
write_multiple_registers_rsp
Input:     int8       address            Slave Address
           int16      start_address      Echo of address to start at
           int16      quantity           Echo of amount of registers written to
Output:    void
*/
void modbus_write_multiple_registers_rsp(uint8_t address, uint16_t start_address, 
                                            uint16_t quantity)
{
   modbus_serial_send_start(address, FUNC_WRITE_MULTIPLE_REGISTERS);

   modbus_serial_putc(highByte(start_address));
   modbus_serial_putc(lowByte(start_address));

   modbus_serial_putc(highByte(quantity));
   modbus_serial_putc(lowByte(quantity));

   modbus_serial_send_stop();
}

/*
report_slave_id_rsp
Input:     int8       address            Slave Address
           int8       slave_id           Slave Address
           int8       run_status         Are we running?
           int8*      data               Pointer to an array holding the data
           int8       data_len           Length of data in bytes
Output:    void
*/
void modbus_report_slave_id_rsp(uint8_t address, uint8_t slave_id, bool run_status,
                              uint8_t *data, uint8_t data_len)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_REPORT_SLAVE_ID);

   modbus_serial_putc(data_len+2);
   modbus_serial_putc(slave_id);

   if(run_status)
    modbus_serial_putc(0xFF);
   else
    modbus_serial_putc(0x00);

   for(i=0; i < data_len; ++i)
   {
      modbus_serial_putc(*data);
      data++;
   }

   modbus_serial_send_stop();
}

/*
read_file_record_rsp
Input:     int8                     address            Slave Address
           int8                     byte_count         Number of bytes to send
           read_sub_request_rsp*    request            Structure holding record/data information
Output:    void
*/
void modbus_read_file_record_rsp(uint8_t address, uint8_t byte_count, 
                                    modbus_read_sub_request_rsp *request)
{
   uint8_t i=0,j;

   modbus_serial_send_start(address, FUNC_READ_FILE_RECORD);

   modbus_serial_putc(byte_count);

   while(i < byte_count);
   {
      modbus_serial_putc(request->record_length);
      modbus_serial_putc(request->reference_type);

      for(j=0; (j < request->record_length); j+=2)
      {
         modbus_serial_putc(highByte(request->data[j]));
         modbus_serial_putc(lowByte(request->data[j]));
      }

      i += (request->record_length)+1;
      request++;
   }

   modbus_serial_send_stop();
}

/*
write_file_record_rsp
Input:     int8                     address            Slave Address
           int8                     byte_count         Echo of number of bytes sent
           write_sub_request_rsp*   request            Echo of Structure holding record information
Output:    void
*/
void modbus_write_file_record_rsp(uint8_t address, uint8_t byte_count, 
                                    modbus_write_sub_request_rsp *request)
{
   uint8_t i, j=0;

   modbus_serial_send_start(address, FUNC_WRITE_FILE_RECORD);

   modbus_serial_putc(byte_count);

   for(i=0; i < byte_count; i+=(7+(j*2)))
   {
      modbus_serial_putc(request->reference_type);
      modbus_serial_putc(highByte(request->file_number));
      modbus_serial_putc(lowByte(request->file_number));
      modbus_serial_putc(highByte(request->record_number));
      modbus_serial_putc(lowByte(request->record_number));
      modbus_serial_putc(highByte(request->record_length));
      modbus_serial_putc(lowByte(request->record_length));

      for(j=0; (j < request->record_length); j+=2)
      {
         modbus_serial_putc(highByte(request->data[j]));
         modbus_serial_putc(lowByte(request->data[j]));
      }
   }

   modbus_serial_send_stop();
}

/*
mask_write_register_rsp
Input:     int8        address            Slave Address
           int16       reference_address  Echo of reference address
           int16       AND_mask           Echo of AND mask
           int16       OR_mask            Echo or OR mask
Output:    void
*/
void modbus_mask_write_register_rsp(uint8_t address, uint16_t reference_address,
                           uint16_t AND_mask, uint16_t OR_mask)
{
   modbus_serial_send_start(address, FUNC_MASK_WRITE_REGISTER);

   modbus_serial_putc(highByte(reference_address));
   modbus_serial_putc(lowByte(reference_address));

   modbus_serial_putc(highByte(AND_mask));
   modbus_serial_putc(lowByte(AND_mask));

   modbus_serial_putc(highByte(OR_mask));
   modbus_serial_putc(lowByte(OR_mask));

   modbus_serial_send_stop();
}

/*
read_write_multiple_registers_rsp
Input:     int8        address            Slave Address
           int16*      data               Pointer to an array of data
           int8        data_len           Length of data in bytes
Output:    void
*/
void modbus_read_write_multiple_registers_rsp(uint8_t address, uint8_t data_len, 
                                                uint16_t *data)
{
   uint8_t i;

   modbus_serial_send_start(address, FUNC_READ_WRITE_MULTIPLE_REGISTERS);

   modbus_serial_putc(data_len*2);

   for(i=0; i < data_len*2; i+=2)
   {
      modbus_serial_putc(highByte(data[i]));
      modbus_serial_putc(lowByte(data[i]));
   }

   modbus_serial_send_stop();
}

/*
read_FIFO_queue_rsp
Input:     int8        address            Slave Address
           int16       FIFO_len           Length of FIFO in bytes
           int16*      data               Pointer to an array of data
Output:    void
*/
void modbus_read_FIFO_queue_rsp(uint8_t address, uint16_t FIFO_len, uint16_t *data)
{
   uint8_t i;
   uint16_t byte_count;

   byte_count = ((FIFO_len*2)+2);

   modbus_serial_send_start(address, FUNC_READ_FIFO_QUEUE);

   modbus_serial_putc(highByte(byte_count));
   modbus_serial_putc(lowByte(byte_count));

   modbus_serial_putc(highByte(FIFO_len));
   modbus_serial_putc(lowByte(FIFO_len));

   for(i=0; i < FIFO_len; i+=2)
   {
      modbus_serial_putc(highByte(data[i]));
      modbus_serial_putc(lowByte(data[i]));
   }

   modbus_serial_send_stop();
}

void modbus_exception_rsp(uint8_t address, uint16_t func, exception error)
{
   modbus_serial_send_start(address, func|0x80);
   modbus_serial_putc(error);
   modbus_serial_send_stop();
}

#endif
