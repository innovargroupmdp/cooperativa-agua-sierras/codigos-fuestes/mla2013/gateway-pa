//////////////////////////////////////////////////////////////////////////////////////////
////                                      modbus.h                                    ////
////                                                                                  ////
////                 MODBUS protocol driver for serial communications.                ////
////                                                                                  ////
////  Refer to documentation at http://www.modbus.org for more information on MODBUS. ////
////                                                                                  ////
//////////////////////////////////////////////////////////////////////////////////////////
////                                                                                  ////
//// DEFINES:                                                                         ////
////                                                                                  ////
////  MODBUS_TYPE                   MODBUS_TYPE_MASTER or MODBUS_TYPE_SLAVE           ////
////  MODBUS_SERIAL_INT_SOURCE      Source of interrupts                              ////
////                                (MODBUS_INT_EXT,MODBUS_INT_RDA,MODBUS_INT_RDA2)   ////
////  MODBUS_SERIAL_BAUD            Valid baud rate for serial                        ////
////  MODBUS_SERIAL_RX_PIN          Valid pin for serial receive                      ////
////  MODBUS_SERIAL_TX_PIN          Valid pin for serial transmit                     ////
////  MODBUS_SERIAL_ENABLE_PIN      Valid pin for serial enable, rs485 only           ////
////  MODBUS_SERIAL_RX_ENABLE       Valid pin for serial rcv enable, rs485 only       ////
////  MODBUS_SERAIL_RX_BUFFER_SIZE  Size of the receive buffer                        ////
////                                                                                  ////
////                                                                                  ////
//// SHARED API:                                                                      ////
////                                                                                  ////
////  modbus_init()                                                                   ////
////    - Initialize modbus serial communication system                               ////
////                                                                                  ////
////  modbus_serial_send_start(address,func)                                          ////
////    - Setup serial line to begin sending.  Once this is called, you can send data ////
////      using modbus_serial_putc().  Should only be used for custom commands.       ////
////                                                                                  ////
////  modbus_serial_send_stop()                                                       ////
////    - Must be called to finalize the send when modbus_serial_send_start is used.  ////
////                                                                                  ////
////  modbus_kbhit()                                                                  ////
////    - Used to check if a packet has been received.                                ////
////                                                                                  ////
//// MASTER API:                                                                      ////
////  All master API functions return 0 on success.                                   ////
////                                                                                  ////
////  exception modbus_read_coils(address,start_address,quantity)                     ////
////    - Wrapper for function 0x01(read coils) in the MODBUS specification.          ////
////                                                                                  ////
////  exception modbus_read_discrete_input(address,start_address,quantity)            ////
////    - Wrapper for function 0x02(read discret input) in the MODBUS specification.  ////
////                                                                                  ////
////  exception modbus_read_holding_registers(address,start_address,quantity)         ////
////    - Wrapper for function 0x03(read holding regs) in the MODBUS specification.   ////
////                                                                                  ////
////  exception modbus_read_input_registers(address,start_address,quantity)           ////
////    - Wrapper for function 0x04(read input regs) in the MODBUS specification.     ////
////                                                                                  ////
////  exception modbus_write_single_coil(address,output_address,on)                   ////
////    - Wrapper for function 0x05(write single coil) in the MODBUS specification.   ////
////                                                                                  ////
////  exception modbus_write_single_register(address,reg_address,reg_value)           ////
////    - Wrapper for function 0x06(write single reg) in the MODBUS specification.    ////
////                                                                                  ////
////  exception modbus_read_exception_status(address)                                 ////
////    - Wrapper for function 0x07(read void status) in the MODBUS specification.    ////
////                                                                                  ////
////  exception modbus_diagnostics(address,sub_func,data)                             ////
////    - Wrapper for function 0x08(diagnostics) in the MODBUS specification.         ////
////                                                                                  ////
////  exception modbus_get_comm_event_counter(address)                                ////
////    - Wrapper for function 0x0B(get comm event count) in the MODBUS specification.////
////                                                                                  ////
////  exception modbus_get_comm_event_log(address)                                    ////
////    - Wrapper for function 0x0C(get comm event log) in the MODBUS specification.  ////
////                                                                                  ////
////  exception modbus_write_multiple_coils(address,start_address,quantity,*values)   ////
////    - Wrapper for function 0x0F(write multiple coils) in the MODBUS specification.////
////    - Special Note: values is a pointer to an int8 array, each byte represents 8  ////
////                    coils.                                                        ////
////                                                                                  ////
////  exception modbus_write_multiple_registers(address,start_address,quantity,*values)///
////    - Wrapper for function 0x10(write multiple regs) in the MODBUS specification. ////
////    - Special Note: values is a pointer to an int8 array                          ////
////                                                                                  ////
////  exception modbus_report_slave_id(address)                                       ////
////    - Wrapper for function 0x11(report slave id) in the MODBUS specification.     ////
////                                                                                  ////
////  exception modbus_read_file_record(address,byte_count,*request)                  ////
////    - Wrapper for function 0x14(read file record) in the MODBUS specification.    ////
////                                                                                  ////
////  exception modbus_write_file_record(address,byte_count,*request)                 ////
////    - Wrapper for function 0x15(write file record) in the MODBUS specification.   ////
////                                                                                  ////
////  exception modbus_mask_write_register(address,reference_address,AND_mask,OR_mask)////
////    - Wrapper for function 0x16(read coils) in the MODBUS specification.          ////
////                                                                                  ////
////  exception modbus_read_write_multiple_registers(address,read_start,read_quantity,////
////                            write_start,write_quantity, *write_registers_value)   ////
////    - Wrapper for function 0x17(read write mult regs) in the MODBUS specification.////
////                                                                                  ////
////  exception modbus_read_FIFO_queue(address,FIFO_address)                          ////
////    - Wrapper for function 0x18(read FIFO queue) in the MODBUS specification.     ////
////                                                                                  ////
////                                                                                  ////
//// Slave API:                                                                       ////
////                                                                                  ////
////  void modbus_read_coils_rsp(address,byte_count,*coil_data)                       ////
////    - Wrapper to respond to 0x01(read coils) in the MODBUS specification.         ////
////                                                                                  ////
////  void modbus_read_discrete_input_rsp(address,byte_count,*input_data)             ////
////    - Wrapper to respond to 0x02(read discret input) in the MODBUS specification. ////
////                                                                                  ////
////  void modbus_read_holding_registers_rsp(address,byte_count,*reg_data)            ////
////    - Wrapper to respond to 0x03(read holding regs) in the MODBUS specification.  ////
////                                                                                  ////
////  void modbus_read_input_registers_rsp(address,byte_count,*input_data)            ////
////    - Wrapper to respond to 0x04(read input regs) in the MODBUS specification.    ////
////                                                                                  ////
////  void modbus_write_single_coil_rsp(address,output_address,output_value)          ////
////    - Wrapper to respond to 0x05(write single coil) in the MODBUS specification.  ////
////                                                                                  ////
////  void modbus_write_single_register_rsp(address,reg_address,reg_value)            ////
////    - Wrapper to respond to 0x06(write single reg) in the MODBUS specification.   ////
////                                                                                  ////
////  void modbus_read_exception_status_rsp(address, data)                            ////
////    - Wrapper to respond to 0x07(read void status) in the MODBUS specification.   ////
////                                                                                  ////
////  void modbus_diagnostics_rsp(address,sub_func,data)                              ////
////    - Wrapper to respond to 0x08(diagnostics) in the MODBUS specification.        ////
////                                                                                  ////
////  void modbus_get_comm_event_counter_rsp(address,status,event_count)              ////
////    - Wrapper to respond to 0x0B(get comm event count) in the MODBUS specification////
////                                                                                  ////
////  void modbus_get_comm_event_log_rsp(address,status,event_count,message_count,    ////
////                                   *events, events_len)                           ////
////    - Wrapper to respond to 0x0C(get comm event log) in the MODBUS specification. ////
////                                                                                  ////
////  void modbus_write_multiple_coils_rsp(address,start_address,quantity)            ////
////    - Wrapper to respond to 0x0F(write multiple coils) in the MODBUS specification////
////                                                                                  ////
////  void modbus_write_multiple_registers_rsp(address,start_address,quantity)        ////
////    - Wrapper to respond to 0x10(write multiple regs) in the MODBUS specification.////
////                                                                                  ////
////  void modbus_report_slave_id_rsp(address,slave_id,run_status,*data,data_len)     ////
////    - Wrapper to respond to 0x11(report slave id) in the MODBUS specification.    ////
////                                                                                  ////
////  void modbus_read_file_record_rsp(address,byte_count,*request)                   ////
////    - Wrapper to respond to 0x14(read file record) in the MODBUS specification.   ////
////                                                                                  ////
////  void modbus_write_file_record_rsp(address,byte_count,*request)                  ////
////    - Wrapper to respond to 0x15(write file record) in the MODBUS specification.  ////
////                                                                                  ////
////  void modbus_mask_write_register_rsp(address,reference_address,AND_mask,OR_mask) ////
////    - Wrapper to respond to 0x16(read coils) in the MODBUS specification.         ////
////                                                                                  ////
////  void modbus_read_write_multiple_registers_rsp(address,*data,data_len)           ////
////    - Wrapper to respond to 0x17(read write mult regs) in the MODBUS specification////
////                                                                                  ////
////  void modbus_read_FIFO_queue_rsp(address,FIFO_len,*data)                         ////
////    - Wrapper to respond to 0x18(read FIFO queue) in the MODBUS specification.    ////
////                                                                                  ////
////  void modbus_exception_rsp(int8 address, int16 func, exception error)            ////
////    - Wrapper to send an exception response.  See exception list below.           ////
////                                                                                  ////
//// Exception List:                                                                  ////
////  ILLEGAL_FUNCTION, ILLEGAL_DATA_ADDRESS, ILLEGAL_DATA_VALUE,                     ////
////  SLAVE_DEVICE_FAILURE, ACKNOWLEDGE, SLAVE_DEVICE_BUSY, MEMORY_PARITY_ERROR,      ////
////  GATEWAY_PATH_UNAVAILABLE, GATEWAY_TARGET_NO_RESPONSE                            ////
////                                                                                  ////
//////////////////////////////////////////////////////////////////////////////////////////
////                (C) Copyright 1996, 2006 Custom Computer Services                 ////
////        This source code may only be used by licensed users of the CCS            ////
////        C compiler.  This source code may only be distributed to other            ////
////        licensed users of the CCS C compiler.  No other use,                      ////
////        reproduction or distribution is permitted without written                 ////
////        permission.  Derivative programs created using this software              ////
////        in object code form are not restricted in any way.                        ////
//////////////////////////////////////////////////////////////////////////////////////////
//#include <stdbool.h>
//#include <stdint.h>


/********************************************************************
These exceptions are defined in the MODBUS protocol.  These can be
used by the slave to communicate problems with the transmission back
to the master who can also use these to easily check the exceptions.  
The first exception is the only one that is not part of the protocol 
specification.  The TIMEOUT exception is returned when no slave 
responds to the master's request within the timeout period.
********************************************************************/
#ifndef ExcetionDef
#define ExcetionDef

#include "GenericTypeDefs.h"
enum exception_t_Def{
    
    ILLEGAL_FUNCTION=1,
    ILLEGAL_DATA_ADDRESS=2, 
    ILLEGAL_DATA_VALUE=3,
    SLAVE_DEVICE_FAILURE=4,
    ACKNOWLEDGE=5,
    SLAVE_DEVICE_BUSY=6, 
    MEMORY_PARITY_ERROR=8,
    GATEWAY_PATH_UNAVAILABLE=10,
    GATEWAY_TARGET_NO_RESPONSE=11,
    TIMEOUT=12
};
#endif

#ifndef EXCEP_t_Def
#define EXCEP_t_Def
typedef enum exception_t_Def exception;
#endif

/********************************************************************
These functions are defined in the MODBUS protocol.  These can be
used by the slave to check the incomming function.  See 
ex_modbus_slave.c for example usage.
********************************************************************/
#ifndef function_Def
#define function_Def
enum function_t
{
    FUNC_READ_COILS=0x01,FUNC_READ_DISCRETE_INPUT=0x02,
    FUNC_READ_HOLDING_REGISTERS=0x03,FUNC_READ_INPUT_REGISTERS=0x04,
    FUNC_WRITE_SINGLE_COIL=0x05,FUNC_WRITE_SINGLE_REGISTER=0x06,
    FUNC_READ_EXCEPTION_STATUS=0x07,FUNC_DIAGNOSTICS=0x08,
    FUNC_GET_COMM_EVENT_COUNTER=0x0B,FUNC_GET_COMM_EVENT_LOG=0x0C,
    FUNC_WRITE_MULTIPLE_COILS=0x0F,FUNC_WRITE_MULTIPLE_REGISTERS=0x10,
    FUNC_REPORT_SLAVE_ID=0x11,FUNC_READ_FILE_RECORD=0x14,
    FUNC_WRITE_FILE_RECORD=0x15,FUNC_MASK_WRITE_REGISTER=0x16,
    FUNC_READ_WRITE_MULTIPLE_REGISTERS=0x17,FUNC_READ_FIFO_QUEUE=0x18
} ;



typedef enum function_t function;
#endif
/*Global value holding our current CRC value.*/

#ifndef modbus_serial_crc_def
#define modbus_serial_crc_def

union modbus_serial_crc_t_Def
{
   BYTE b[2];
   WORD d;
} ;

//@TODO ver donde especializo
typedef union modbus_serial_crc_t_Def modbus_serial_crc_t;

#endif




/*Global value current ASK state value.*/


/*Some defines so we can use identifiers to set things up*/
#define MODBUS_TYPE_MASTER 99999
#define MODBUS_TYPE_SLAVE 88888
#define MODBUS_INT_RDA 77777
#define MODBUS_INT_RDA2 66666
#define MODBUS_INT_EXT 55555
#define MODBUS_asynchronous 44444

#ifndef MODBUS_TYPE
#define MODBUS_TYPE MODBUS_TYPE_MASTER
//#define MODBUS_TYPE MODBUS_TYPE_SLAVE
#endif

#ifndef MODBUS_SERIAL_INT_SOURCE
#define MODBUS_SERIAL_INT_SOURCE MODBUS_asynchronous  // Select between external interrupt
#endif                                           // or asynchronous serial interrupt

#ifndef MODBUS_SERIAL_BAUD
#define MODBUS_SERIAL_BAUD 9600
#endif

#ifndef MODBUS_stop_bit
#define MODBUS_stop_bit 1000000/MODBUS_SERIAL_BAUD
#endif


#ifndef MODBUS_character_delay
#define MODBUS_character_delay 3500000/MODBUS_SERIAL_BAUD
#endif


#ifndef MODBUS_SERIAL_RX_PIN
#define MODBUS_SERIAL_RX_PIN       PIN_C7   // Data receive pin
#endif

#ifndef MODBUS_SERIAL_TX_PIN
#define MODBUS_SERIAL_TX_PIN       PIN_C6   // Data transmit pin
#endif

#ifndef MODBUS_SERIAL_ENABLE_PIN
#define MODBUS_SERIAL_ENABLE_PIN   1   // Controls DE pin.  RX low, TX high.
#endif

#ifndef MODBUS_SERIAL_RX_ENABLE
#define MODBUS_SERIAL_RX_ENABLE    1  // Controls RE pin.  Should keep low.
#endif

// MODBUS_SERIAL_RESPONSE
/* Determinan un limite duro y difuso para el cual no es definido el timeout de forma dura
 para el caso de una recepcion no exitosa mediante MODBUS_SERIAL_TIMEOUT_inf*/
#ifndef MODBUS_SERIAL_TIMEOUT
#define MODBUS_SERIAL_TIMEOUT      10000     //eventos max ingresados a MODBUS_SERIAL_RESPONSE()) sin recepcion
#endif

#ifndef MODBUS_SERIAL_TIMEOUT_inf
#define MODBUS_SERIAL_TIMEOUT_inf   MODBUS_SERIAL_TIMEOUT/85     // ava parte, limite ,eventos restantes que pueden ser in recepcion
#endif

#ifndef MODBUS_SERIAL_RX_BUFFER_SIZE
#define MODBUS_SERIAL_RX_BUFFER_SIZE  64      //size of send/rcv buffer
#endif

/********************************************************************
Our receive struct.  This is used when receiving data as a master or
slave.  Once a message is sent to you with your address, you should
begin processing that message.  Refer to ex_modbus_slave.c to see 
how to properly use this structure.
********************************************************************/
#ifndef modbus_rx_t_Def_def
#define modbus_rx_t_Def_def
struct modbus_rx_t_Def
{
   BYTE address;
   BYTE len;                                //number of bytes in the message received
   function func;                           //the function of the message received
   exception error;                         //error recieved, if any
   BYTE data[MODBUS_SERIAL_RX_BUFFER_SIZE]; //data of the message received
};


typedef struct modbus_rx_t_Def modbus_rx_t;
#endif
/*---------------GLOBALES-----------------------------------------------------*/
/*---------------GLOBALES-----------------------------------------------------*/

extern modbus_rx_t modbus_rx;

/*----------------------------------------------------------------------------*/



#if( MODBUS_SERIAL_INT_SOURCE == MODBUS_INT_RDA )
  // #use rs232(baud=MODBUS_SERIAL_BAUD, UART1, parity=N, stream=MODBUS_SERIAL, error)
  // #define RCV_OFF() {disable_interrupts(INT_RDA);}
#elif( MODBUS_SERIAL_INT_SOURCE == MODBUS_INT_RDA2 )
   //#use rs232(baud=MODBUS_SERIAL_BAUD, UART2, parity=N, stream=MODBUS_SERIAL, error)
   //#define RCV_OFF() {disable_interrupts(INT_RDA2);}
#elif( MODBUS_SERIAL_INT_SOURCE == MODBUS_INT_EXT )
   //#use rs232(baud=MODBUS_SERIAL_BAUD, xmit=MODBUS_SERIAL_TX_PIN, rcv=MODBUS_SERIAL_RX_PIN, parity=N, stream=MODBUS_SERIAL, disable_ints)
     void RCV_OFF(void) {disable_interrupts(INT_EXT);}     
#endif



//// DEFINES:                                                                         ////

  ////MODBUS_TYPE                   MODBUS_TYPE_MASTER or MODBUS_TYPE_SLAVE           ////
  ////MODBUS_SERIAL_INT_SOURCE      Source of interrupts                              ////
      ////                          (MODBUS_INT_EXT,MODBUS_INT_RDA,MODBUS_INT_RDA2)   ////
  ////MODBUS_SERIAL_BAUD            Valid baud rate for serial                        ////
  ////MODBUS_SERIAL_RX_PIN          Valid pin for serial receive                      ////
  ////MODBUS_SERIAL_TX_PIN          Valid pin for serial transmit                     ////
  ////MODBUS_SERIAL_ENABLE_PIN      Valid pin for serial enable, rs485 only           ////
  ////MODBUS_SERIAL_RX_ENABLE       Valid pin for serial rcv enable, rs485 only       ////
  ////MODBUS_SERAIL_RX_BUFFER_SIZE  Size of the receive buffer                        ////
                                                                                ////
//// SHARED API:                                                                      ////

     


#if (MODBUS_SERIAL_INT_SOURCE != MODBUS_INT_EXT)
void WAIT_FOR_HW_BUFFER(void);    
#endif

void RCV_ON(void);

#if( MODBUS_SERIAL_INT_SOURCE == MODBUS_asynchronous )
   //#use rs232(baud=MODBUS_SERIAL_BAUD, xmit=MODBUS_SERIAL_TX_PIN, rcv=MODBUS_SERIAL_RX_PIN, parity=N, stream=MODBUS_SERIAL, disable_ints)
void RCV_OFF(void);
#endif

void modbus_init(void);                                                                   ////
////    - Initialize modbus serial communication system   
exception MODBUS_SERIAL_RESPONSE(BYTE,function);
//// Define el estado de los procesos de paquetes enviados a recibidos para poder
//// usar un esqueme asincronico mediante evolucion de estados sin interrupciones.
void modbus_enable_timeout(BOOL);
////   - Start our timeout timer
void modbus_timeout_now(void);
////   - Check if we have timed out waiting for a response
void modbus_calc_crc(char);
////   - Calculate crc of data and updates global crc
void modbus_serial_putc(BYTE);
////   - Puts a character onto the serial line
void modbus_serial_send_start(BYTE,BYTE);
////    - Setup serial line to begin sending.  Once this is called, you can send data ////
////      using modbus_serial_putc().  Should only be used for custom commands.       ////
void incomming_modbus_serial(void);
////    - Interrupt service routine for handling incoming serial data
void modbus_serial_send_stop(void);
////    - Must be called to finalize the send when modbus_serial_send_start is used.  ////
////                                                                                  ////
BOOL modbus_kbhit(void);                                                                  ////
////    - Used to check if a packet has been received.                                ////
////                                                                                  ////
//// MASTER API:                                                                      ////

#if (MODBUS_TYPE==MODBUS_TYPE_MASTER)

/*MODBUS Master Functions*/

/********************************************************************
The following structs are used for read/write_sub_request.  These
functions take in one of these structs.
Please refer to the MODBUS protocol specification if you do not
understand the members of the structure.
********************************************************************/
#ifndef _modbus_read_sub_request_Def
#define _modbus_read_sub_request_Def
struct _modbus_read_sub_request
{
   BYTE reference_type;
   WORD file_number;
   WORD record_number;
   WORD record_length;
} ;


typedef struct _modbus_read_sub_request modbus_read_sub_request;
#endif

#ifndef _modbus_write_sub_request_Def
#define _modbus_write_sub_request_Def
 struct _modbus_write_sub_request
{
   BYTE reference_type;
   WORD file_number;
   WORD record_number;
   WORD record_length;
   WORD data[1];//[MODBUS_SERIAL_RX_BUFFER_SIZE-8];
} ;



typedef struct _modbus_write_sub_request modbus_write_sub_request;
#endif

void MODBUS_SERIAL_WAIT_FOR_RESPONSE(BYTE);

////  All master API functions return 0 on success.                                   ////
////                                                                                  ////
exception modbus_read_coils(BYTE,WORD,WORD);                          ////
////    - Wrapper for function 0x01(read coils) in the MODBUS specification.          ////
////                                                                                  ////
exception modbus_read_discrete_input(BYTE,WORD,WORD);            ////
////    - Wrapper for function 0x02(read discret input) in the MODBUS specification.  ////
////                                                                                  ////
exception modbus_read_holding_registers(BYTE,WORD,WORD);          ////
////    - Wrapper for function 0x03(read holding regs) in the MODBUS specification.   ////
////                                                                                  ////
exception modbus_read_input_registers(BYTE,WORD,WORD);            ////
////    - Wrapper for function 0x04(read input regs) in the MODBUS specification.     ////
////                                                                                  ////
exception modbus_write_single_coil(BYTE,WORD,BOOL);                   ////
////    - Wrapper for function 0x05(write single coil) in the MODBUS specification.   ////
////                                                                                  ////
exception modbus_write_single_register(BYTE,WORD,WORD);            ////
////    - Wrapper for function 0x06(write single reg) in the MODBUS specification.    ////
////                                                                                  ////
exception modbus_read_exception_status(BYTE);                                 ////
////    - Wrapper for function 0x07(read void status) in the MODBUS specification.    ////
////                                                                                  ////
exception modbus_diagnostics(BYTE,WORD,WORD);                              ////
////    - Wrapper for function 0x08(diagnostics) in the MODBUS specification.         ////
////                                                                                  ////
exception modbus_get_comm_event_counter(BYTE);                                ////
////    - Wrapper for function 0x0B(get comm event count) in the MODBUS specification.////
////                                                                                  ////
exception modbus_get_comm_event_log(BYTE);                                    ////
////    - Wrapper for function 0x0C(get comm event log) in the MODBUS specification.  ////
////                                                                                  ////
exception modbus_write_multiple_coils(BYTE,WORD,WORD,BYTE*);   ////
////    - Wrapper for function 0x0F(write multiple coils) in the MODBUS specification.////
////    - Special Note: values is a pointer to an int8 array, each byte represents 8  ////
////                    coils.                                                        ////
////                                                                                  ////
exception modbus_write_multiple_registers(BYTE,WORD,WORD,WORD*);///
////    - Wrapper for function 0x10(write multiple regs) in the MODBUS specification. ////
////    - Special Note: values is a pointer to an int8 array                          ////
////                                                                                  ////
exception modbus_report_slave_id(BYTE);                                       ////
////    - Wrapper for function 0x11(report slave id) in the MODBUS specification.     ////
////                                                                                  ////
exception modbus_read_file_record(BYTE,BYTE,modbus_read_sub_request*);          ////
////    - Wrapper for function 0x14(read file record) in the MODBUS specification.    ////
////                                                                                  ////
exception modbus_write_file_record(BYTE,BYTE,modbus_write_sub_request*);                 ////
////    - Wrapper for function 0x15(write file record) in the MODBUS specification.   ////
////                                                                                  ////
exception modbus_mask_write_register(BYTE,WORD,WORD,WORD);////
////    - Wrapper for function 0x16(read coils) in the MODBUS specification.          ////
////                                                                                  ////
exception modbus_read_write_multiple_registers(BYTE,WORD,WORD,WORD,WORD, WORD*);   ////
////    - Wrapper for function 0x17(read write mult regs) in the MODBUS specification.////
////                                                                                  ////
exception modbus_read_FIFO_queue(BYTE,WORD);                          ////
////    - Wrapper for function 0x18(read FIFO queue) in the MODBUS specification.     ////
////                                                                                  ////
////   

#endif

//// Slave API:
#if (MODBUS_TYPE==MODBUS_TYPE_SLAVE)

/*MODBUS Slave Functions*/

/********************************************************************
The following structs are used for read/write_sub_request_rsp.  These
functions take in one of these structs.  Please refer to the MODBUS
protocol specification if you do not understand the members of the
structure.
********************************************************************/
typedef struct _modbus_read_sub_request_rsp
{
   uint8_t record_length;
   uint8_t reference_type;
   uint16_t data[((MODBUS_SERIAL_RX_BUFFER_SIZE)/2)-3];
} modbus_read_sub_request_rsp;

typedef struct _modbus_write_sub_request_rsp
{
   uint8_t reference_type;
   uint16_t file_number;
   uint16_t record_number;
   uint16_t record_length;
   uint16_t data[((MODBUS_SERIAL_RX_BUFFER_SIZE)/2)-8];
} modbus_write_sub_request_rsp;
////
////                                                                                  ////
void modbus_read_coils_rsp(uint8_t,uint8_t,*uint8_t);                       ////
////    - Wrapper to respond to 0x01(read coils) in the MODBUS specification.         ////
////                                                                                  ////
void modbus_read_discrete_input_rsp(uint8_t,uint8_t,uint8_t*);             ////
////    - Wrapper to respond to 0x02(read discret input) in the MODBUS specification. ////
////                                                                                  ////
void modbus_read_holding_registers_rsp(uint8_t,uint8_t,uint8_t*);            ////
////    - Wrapper to respond to 0x03(read holding regs) in the MODBUS specification.  ////
////                                                                                  ////
void modbus_read_input_registers_rsp(uint8_t,uint8_t,uint8_t*);            ////
////    - Wrapper to respond to 0x04(read input regs) in the MODBUS specification.    ////
////                                                                                  ////
void modbus_write_single_coil_rsp(uint8_t,uint16_t,uint16_t);          ////
////    - Wrapper to respond to 0x05(write single coil) in the MODBUS specification.  ////
////                                                                                  ////
void modbus_write_single_register_rsp(uint8_t,uint16_t,uint16_t);            ////
////    - Wrapper to respond to 0x06(write single reg) in the MODBUS specification.   ////
////                                                                                  ////
void modbus_read_exception_status_rsp(uint8_t, uint8_t);                            ////
////    - Wrapper to respond to 0x07(read void status) in the MODBUS specification.   ////
////                                                                                  ////
void modbus_diagnostics_rsp(uint8_t,uint16_t,uint16_t);                              ////
////    - Wrapper to respond to 0x08(diagnostics) in the MODBUS specification.        ////
////                                                                                  ////
void modbus_get_comm_event_counter_rsp(uint8_t,uint16_t,uint16_t);              ////
////    - Wrapper to respond to 0x0B(get comm event count) in the MODBUS specification////
////                                                                                  ////
void modbus_get_comm_event_log_rsp(uint8_t,uint16_t,uint16_t,uint16_t,uint8_t*,uint8_t);                           ////
////    - Wrapper to respond to 0x0C(get comm event log) in the MODBUS specification. ////
////                                                                                  ////
void modbus_write_multiple_coils_rsp(uint8_t,uint16_t,uint16_t);            ////
////    - Wrapper to respond to 0x0F(write multiple coils) in the MODBUS specification////
////                                                                                  ////
void modbus_write_multiple_registers_rsp(uint8_t,uint16_t,uint16_t);        ////
////    - Wrapper to respond to 0x10(write multiple regs) in the MODBUS specification.////
////                                                                                  ////
void modbus_report_slave_id_rsp(uint8_t,uint8_t,bool,uint8_t*,uint8_t);     ////
////    - Wrapper to respond to 0x11(report slave id) in the MODBUS specification.    ////
////                                                                                  ////
void modbus_read_file_record_rsp(uint8_t,uint8_t,modbus_read_sub_request_rsp*);        ////
////    - Wrapper to respond to 0x14(read file record) in the MODBUS specification.   ////
////                                                                                  ////
void modbus_write_file_record_rsp(uint8_t,uint8_t,modbus_write_sub_request_rsp*);     ////
////    - Wrapper to respond to 0x15(write file record) in the MODBUS specification.  ////
////                                                                                  ////
void modbus_mask_write_register_rsp(uint8_t,uint16_t,uint16_t,uint16_t); ////
////    - Wrapper to respond to 0x16(read coils) in the MODBUS specification.         ////
////                                                                                  ////
void modbus_read_write_multiple_registers_rsp(uint8_t,uint8_t,uint16_t*);           ////
////    - Wrapper to respond to 0x17(read write mult regs) in the MODBUS specification////
////                                                                                  ////
void modbus_read_FIFO_queue_rsp(uint8_t,uint16_t,uint16_t*);                         ////
////    - Wrapper to respond to 0x18(read FIFO queue) in the MODBUS specification.    ////
////                                                                                  ////
void modbus_exception_rsp( uint8_t, uint16_t, exception);            ////
////    - Wrapper to send an exception response.  See exception list below.           ////
////                                                                                  ////
#endif

////Exception List:                                                                   ////

                                                                                      ////
//////////////////////////////////////////////////////////////////////////////////////////
////                (C) Copyright 1996, 2006 Custom Computer Services                 ////
////        This source code may only be used by licensed users of the CCS            ////
////        C compiler.  This source code may only be distributed to other            ////
////        licensed users of the CCS C compiler.  No other use,                      ////
////        reproduction or distribution is permitted without written                 ////
////        permission.  Derivative programs created using this software              ////
////        in object code form are not restricted in any way.                        ////
//////////////////////////////////////////////////////////////////////////////////////////
