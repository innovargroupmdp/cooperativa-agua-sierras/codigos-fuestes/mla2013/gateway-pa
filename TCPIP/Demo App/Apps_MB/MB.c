#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "system.h"
//#include "gtypedefs.h"
//#include "symbol.h"
//#include "delay.h"

//variables globales
#include "App.h"
//App que intercambia tramas modbus para presenar a Miwi 
#include "APP_MIWI_ModBus.h"
//servidor maestro modbus que se encarga de establecer la 
//comunicacion entre los diferentes dispositivos de la red modbus
#include "Modbus_M.h"
//Se encarga de establecer la conexion administras las secuencias entre PAM y el gateWay
#include "MIWI.h"

#define VER_MAJOR  2
#define VER_MINOR  0


void Ini_MB(void)
{
    
    
}    
void Put_RXBufferMB(void){
    uint8_t i= 0;
    
    if (Rx_Buffer_MODMIWI.point < Length_buffer_RX)
    {
        do{ 
            Rx_Buffer_MODMIWI.Message[Rx_Buffer_MODMIWI.point].Payload[i] =modbus_rx.data[i];
            i++;
        }while (i<modbus_rx.len);
        Rx_Buffer_MODMIWI.point++;
    }
}

COM_State_t FunRx_MB(void)
{    
    static bool st= true;
    static bool st1= true;
    //uint8_t *p;    
    //DIRECCION ID O MAC en la asociacion
    //llama al main de ModbusM la secuencia es:
    /*primero: consulta
      segundo: llegada y espera de datos
      Tercero: recepcion
      cuarto:  presentacion de buffer modbus a capa superior miwi almaceno N tramas entrantes
      quinto:  Trasmitir datos de capa superio
      Sexto:   escuchar recepcion de ack
    */
    //@TODO modificar para que los estados concuerden con la salida de la funcion
    switch( Serv_Master_Modbus_MIWI())
    {  
        case State_Req :
                /*blankea las variables*/
                st=  true;
                st1= true;
                return(false);
        break;            
        
        case State_Get:
            
            if(st)//---------------almaceno en un buffer los datos recibidos------------//
            {   
                st--;
                Put_RXBufferMB();
                //---------vacio el registro modbus_rx--------------------------------//
                Modbus_DiscardMessage();
                return(true);                     
            }    
            
        break;
        
        case State_Put:
            
            /* Caso que haya trama valida LARGA recibida modbus*/
            /* Proceso las tramas recibidas segun su tipo: Normal, Control*/
            //servicio que define una trama valida recibida para almacenar y no un ACK
            if(st1)
            {   
                st1--;
                //---------------almaceno en un buffer los datos recibidos------------//
                Put_RXBufferMB();
                //---------vacio el registro modbus_rx----------------------------------
                Modbus_DiscardMessage();
                //----------------------------------------------------------------------
                return(true);
            }
        break;
                        
        default :
            
            return(false);
        break;    
    }
    App1.Tipo= 0;
    App1.ID=   0;
    return(false);        
}
//------------------------------------------------------------------------------
/*!
  Function:
	BOOL FunTx(void)

 @brief  Description:
	Writes a single byte to a TCP socket.

  Precondition:
	TCP is initialized.

  Parameters:
 @param c1 the first argument.
 @param c2 the second argument.
	hTCP - The socket to which data is to be written.
	byte - The byte to write.
 @return
 Return Values:
	TRUE - The byte was written to the transmit buffer.
	FALSE - The transmit buffer was full, or the socket is not connected.
*/
//------------------------------------------------------------------------------
COM_State_t FunTx_MB()
{
    static bool  s= false;
    static bool s0= false;
    static bool s1= false;
    //uint16_t A;
    uint8_t i;
    uint8_t k=0;    
    //ROM addr_t coord={0};   
    //memset(coord.bytes,0x00,sizeof(coord));        
    //Serv_Master_Modbus();
  
    if (!s)
    {       
        k= Tx_Buffer_MODMIWI.point;
        s++; 
        switch (App.Tipo) 
        {    //indica longitud total de payload MB      
            case nOP:
                /*------------TRAMA CORTA UNICO  ENVIO----------------------------*/ 
                if (!s)
                {
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_nOP;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_nOP;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;           //para que en cuanto responda, machee
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;          //MB
                    /*-DATA-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;             //<-(3)   
                    s++; 
                }else 
                {
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_nOP +2;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_nOP +2;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;           //para que en cuanto responda, machee
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;          
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = 'O'; //Valido    
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = 'K'; //Valido
                    s--;
                }
            break;    
            case OP_NORMAL:

                if (!s) 
                {   
                    s++;
                    /*------------TRAMA CORTA PRIMER ENVIO------------------------*/ 
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Short;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Short;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;        //<-(3) 

                }else
                {   
                    s--;
                    /*------------TRAMA LARGA SEGUNDO ENVIO-----------------------*/  
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Norm + App.MedidaDif.Index;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Norm + App.MedidaDif.Index;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = highByte(App.ID);
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = lowByte(App.ID);
                    Tx_Buffer_MODMIWI.Message[k].Payload[6] = highByte(App.TOKEN);
                    Tx_Buffer_MODMIWI.Message[k].Payload[7] = lowByte(App.TOKEN);

                    Tx_Buffer_MODMIWI.Message[k].Payload[8] = App.Secuencia;
                    Tx_Buffer_MODMIWI.Message[k].Payload[9] = App.Nro_intentos;
                    Tx_Buffer_MODMIWI.Message[k].Payload[10] = App.Noise;
                    Tx_Buffer_MODMIWI.Message[k].Payload[11] = App.Signal;
                    Tx_Buffer_MODMIWI.Message[k].Payload[12] = App.Bat;

                    Tx_Buffer_MODMIWI.Message[k].Payload[13] = highByte(App.Slot);
                    Tx_Buffer_MODMIWI.Message[k].Payload[14] = lowByte(App.Slot);

                    Tx_Buffer_MODMIWI.Message[k].Payload[15] = App.DateTx.Seg;
                    Tx_Buffer_MODMIWI.Message[k].Payload[16] = App.DateTx.Min;
                    Tx_Buffer_MODMIWI.Message[k].Payload[17] = App.DateTx.Hou;          
                    /*-DATA Of APPLICATION-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[18] = App.MedidaRE.DateMesu.Hou;
                    Tx_Buffer_MODMIWI.Message[k].Payload[19] = App.MedidaRE.DateMesu.Day;
                    Tx_Buffer_MODMIWI.Message[k].Payload[20] = App.MedidaRE.DateMesu.Mon;

                    Tx_Buffer_MODMIWI.Message[k].Payload[21] = highByte(App.MedidaRE.Mesure);
                    Tx_Buffer_MODMIWI.Message[k].Payload[22] = lowByte (App.MedidaRE.Mesure);

                    Tx_Buffer_MODMIWI.Message[k].Payload[23] = App.MedidaDif.Index;

                    for (i=0;i<=App.MedidaDif.Index;i++)
                        Tx_Buffer_MODMIWI.Message[k].Payload[24+i] = App.MedidaDif.MedidaD[i];// <-(24)                
                }

            break;  

            case OP_ASOCIACION:  //tanto FFD , monitores   

                if (!s) 
                {            
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Aso;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Aso;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;          //para que en cuanto responda, machee
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = highByte(App.ID);     //MAC
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = lowByte(App.ID);      //MAC
                }else
                {
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Aso;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Aso;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;           //para que en cuanto responda, machee
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;          
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = 'O'; //Valido    
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = 'K'; //Valido
                }

            break;

            case OP_TIME:

                Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Time;
                /*-HEADER-*/
                Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Time;
                Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;              //para que en cuanto responda, machee
                /*-DATA OF LAYER-*/ 
                Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                Tx_Buffer_MODMIWI.Message[k].Payload[4] = highByte(App.ID);     
                Tx_Buffer_MODMIWI.Message[k].Payload[5] = lowByte(App.ID);      
            break;

            case OP_ASOCIACION_PAN: //SOLO COOR-PAN
                if (!s)
                {    
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Pan;                
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Pan;//<-(8)
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = highByte(App.ID);     //MAC
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = lowByte(App.ID);      //MAC                                            
                }else
                {
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Pan;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Pan;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;          
                    /*-DATA OF LAYER-*/ 
                               //para que en cuanto responda, machee
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = 'O'; //Valido    
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = 'K'; //Valido
                }
            break; 

            case OP_CONTROL:

                if (!s) 
                {   
                    s++;
                    /*------------TRAMA CORTA PRIMER ENVIO------------------*/ 
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Short;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Short;
                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;      //para que en cuanto responda, machee
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;
                    /*-DATA OF LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;         //<-(3) 

                }else
                {   
                    s--;
                    /*------------TRAMA LARGA SEGUNDO ENVIO----------------*/ 
                    Tx_Buffer_MODMIWI.Message[k].Length = Length_Trama_Ctrl + App.DatCtrl.Index;
                    /*-HEADER-*/
                    Tx_Buffer_MODMIWI.Message[k].Payload[0] = Length_Trama_Ctrl + App.DatCtrl.Index;

                    Tx_Buffer_MODMIWI.Message[k].Payload[1] = App.Sec_GW;
                    Tx_Buffer_MODMIWI.Message[k].Payload[2] = App.Sec_PAN;
                    /*-DATA Of LAYER-*/ 
                    Tx_Buffer_MODMIWI.Message[k].Payload[3] = App.Tipo;                
                    Tx_Buffer_MODMIWI.Message[k].Payload[4] = highByte(App.ID);
                    Tx_Buffer_MODMIWI.Message[k].Payload[5] = lowByte(App.ID);
                    Tx_Buffer_MODMIWI.Message[k].Payload[6] = highByte(App.TOKEN);
                    Tx_Buffer_MODMIWI.Message[k].Payload[7] = lowByte(App.TOKEN);
                    Tx_Buffer_MODMIWI.Message[k].Payload[8] = highByte(App.TKMM);
                    Tx_Buffer_MODMIWI.Message[k].Payload[9] = lowByte(App.TKMM);
                    /*
                    Tx_Buffer_MODMIWI.Message[k].Payload[7] = App.Secuencia;
                    Tx_Buffer_MODMIWI.Message[k].Payload[8] = App.Nro_intentos;
                    Tx_Buffer_MODMIWI.Message[k].Payload[9] = App.Noise;
                    Tx_Buffer_MODMIWI.Message[k].Payload[10] = App.Signal;
                    Tx_Buffer_MODMIWI.Message[k].Payload[11] = App.Bat;

                    Tx_Buffer_MODMIWI.Message[k].Payload[12] = highByte(App.Slot);
                    Tx_Buffer_MODMIWI.Message[k].Payload[13] = lowByte(App.Slot);

                    Tx_Buffer_MODMIWI.Message[k].Payload[14] = App.DateTx.Seg;
                    Tx_Buffer_MODMIWI.Message[k].Payload[15] = App.DateTx.Min;
                    Tx_Buffer_MODMIWI.Message[k].Payload[16] = App.DateTx.Hou;          
                    */
                    /*-DATA Of APPLICATION-*/ 

                    Tx_Buffer_MODMIWI.Message[k].Payload[10] = App.SubTipo;
                    Tx_Buffer_MODMIWI.Message[k].Payload[11] = App.DatCtrl.Index;

                    for (i=0;i<=App.DatCtrl.Index;i++)
                        Tx_Buffer_MODMIWI.Message[k].Payload[12+i] = App.DatCtrl.Data[i];//<-(20)                
                }    
            break; 

            default:
                return (FAIL);
        }          
    }
          
    //p=&App1.DateTx.Seg;
    //for(i=0;i<6;i++)
    //{    
    //    Tx_Buffer_MODMIWI.Message[k].Payload[4+i]=halfnippleToByte(lowNipple(*p),highNipple(*(p+1)));
    //    p++;
    //    txMessage[0].Payload[i++]=*(p++); //proceso primer paquete 90 bytes en total el buffer
    // }
    //A=App1.Slot;
    //A=A>>8; 
    //Tx_Buffer_MODMIWI.Message[k].Payload[i]=(uint8_t)A;        
    
    /*indica que ya efectuo el envio y recibio ACK paso de State_AcK a State_Req*/
    // Paso de hacer la consulta con State_Req>State_get> hora estoy para transmitir en State_Put >State_ACK luego State_Req
    
    switch (Serv_Master_Modbus_MIWI())
    {        
        case(State_Put):
                s0= 1;   // Establede envio de primer escritura
            break;
        
        case(State_Ack):                 
                s1= 1;   // Reconocimiento de trama enviada al esclavo writerespons
                s0= 0;   // Blanqueo de anterior
            break;
        
        case(State_Req):                
                if (s0)
                    return(FAIL);   //difiero hay algun problema paso de put->Req sin ack
                                    //debo volver a intentar sin eliminar el TXBUFFER                                                                                //dejo el RX-buffer-modmiwi sin cambio lo atiendo en otro momento       
                if (s1)
                {                                                       
                    s--;            //reinicio la escritura de buffer
                    s1--;
                    if (Tx_Buffer_MODMIWI.point>0) 
                        Tx_Buffer_MODMIWI.point--;
                    // Actualizo el puntero
                    // TXpoint pora esta aplicacion es siempre el primero
                    return(ACK);
                }                
                s--;                //reinicio la escritura de buffer
                return(FAIL);
            break;  
            
        default:
            return(FAIL);
    }
    //return(ModBus_UnicastAddress(txMessage[0].Adress));  si resulta en una transmision correcta   
    return(WAIT);
}

/*****************************************************************************
*  Function:
*	BOOL Serv_MIWI(void)
*
*  Description:
*	Writes a single byte to a TCP socket.
*
*  Precondition:
*	TCP is initialized.
*
*  Parameters:
* @param c1 the first argument.
* @param c2 the second argument.
*	hTCP - The socket to which data is to be written.
*	byte - The byte to write.
* @return
* Return Values:
*	TRUE - The byte was written to the transmit buffer.
*	FALSE - The transmit buffer was full, or the socket is not connected.
*****************************************************************************/
COM_State_t Serv_MB(State_t fun){ // DA EVOLUVION AL STACK
//bool Serv_MIWI(void){ // DA EVOLUVION AL STACK
    //DEBO DECIDIR SI TRANSMITO O RECIBO
    //SI USO App1 no puedo discriminar tengo los siguientes casos:
    /* recibomiwi-analizo-respondomiwi. Solucion maquina de estados.
     * reciboTCPIP-???
     */
  
    switch(fun){ 
        case RECB_MB:  //Si el PANCO recibio Paquetes
            /*------Consulto al PAN por TRAMAS A RECIBIR verifico el estado el buffer miwi y almaceno las tramas segun corresponda----*/
            
            //if(ModBus_MessageAvailable())// Verifico si exite una trama segun mi protocolo para modbus
            // {
                /*--------------Almaceno el ADDRES--------------------------------------------*/
                //Deberia saber a quien consulto en primer instancia solo un esclavo de haber mas es un broadcast y luego escuchar respuestas aleatorias diferidas
                //uso una ADD modbus valida de la PAN 0000 sin uso
                // 01:master; 02:PAN; 
                // A0:LORA0; 
                // B0:plc0; B1:plc1 ,B2:plc2
                //memcpy(AddMIWI,rxMessage[0].Adress,Length_Add); 
                // Cx:libre 
                /*--------------extraigo campos de la trama----------------------------------*/
                // FUNRX CONSULTA POR MODBUS Y BAJO LAS TRAMAS DE LA PAN 
            switch(FunRx_MB()){ //debe haber un estado interno primero solicito por modbus y luego escucho dentro de funrx
                case FAIL :
                    return(FAIL);
                break;
                case ACK:
                    //Caso de transmision y recepcion completas
                    //Las tramas nOP del PAN puedo usar para TX lo que tenga disponible
                    //Sem_MB_DONE=true; //tomo el recurso para operarlo

                    //Modbus_DiscardMessage(); // Libero buffer si exite una trama segun mi protocolo para modbus Todos loa registos estan en App1 salvados
                    //proximo estado ??
                    //State=TRANS;
                    return(ACK);
                break;
                case WAIT:
                    return(WAIT);
                break;
            }
                /*
                 False :
                       -trama no valida- 
                        App1.Tipo= 0xff;
                        App1.ID=   0;
                       -trama sin recibir
                        App1.Tipo= 0;
                        App1.ID=   0;
                 */
                
                //Modbus_DiscardMessage();//ya lo hago antes
                         
           // }
                           
        break;
        case TRANS_MB:
            //TRANSMITE LOS DATOS DIRECTO DEL PAN AL COOR ENTONCES DEBO EN FUNTX COLOCAR LA TRAMA MIWI EN MODBUS
            //transmito tramas de consulta de datos en el PAN, pueden ser varias o en secuencias segun la aplicacion.
            switch(FunTx_MB()){ //debe haber un estado interno primero solicito por modbus y luego escucho dentro de funrx
                case FAIL :
                    return(FAIL);
                break;
                case ACK:
                    //Caso de transmision y recepcion completas
                    //Las tramas nOP del PAN puedo usar para TX lo que tenga disponible
                    //Sem_MB_DONE=true; //tomo el recurso para operarlo

                    //Modbus_DiscardMessage(); // Libero buffer si exite una trama segun mi protocolo para modbus Todos loa registos estan en App1 salvados
                    //proximo estado ??
                    //State=TRANS;
                    return(ACK);
                break;
                case WAIT:
                    return(WAIT);
                break;            
            }
        break;    
    }
    return(false);
}