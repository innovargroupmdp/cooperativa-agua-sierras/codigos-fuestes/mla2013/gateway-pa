/*
    Modbus.c - a Modbus RTU MASTER-Slave Library for Naitsabes (PIC24-PIC18)
    Embedsa - Sebastian Allende
    https://github.com/emmertex/Modbus-Library/
    https://gitlab.com/Allende/Modbus-Library
 
    Ported from Siamects variation of Mudbus.cpp by Dee Wykoff
    Arduino Library - http://gitorious.org/mudbus

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//#include <stdbool.h>
//#include <stdint.h>
#include "modbus.h"

#ifndef Modbus_h
#define Modbus_h

#define MB_C 64         //Holding Coils (commonly 0x)
#define MB_I 16         //Input Coils (commonly 1x)
#define MB_IR 32        //Input Registers (commonly 3x)
#define MB_HR 64        //Holding Registers (commonly 4x)

#define MB_ADD_M 01     //Modbus Master (default is 1
#define MB_Mx 1//256    //Modbus ADU : ADD|PDU|CRC
#define MB_PDU 253      //Modbus DPU : FC|RG|NR

// Global Variables  ///////////////////////////////////
/*
#if !defined(THIS_IS_MODBUS_C)
    extern bool MBC[MB_C];
    extern bool MBI[MB_I];
    extern uint16_t MBIR[MB_IR];
    extern uint16_t MBR[MB_HR];
#endif
*/    
/*****************************
 * Estados del protocolo conexion MB
 *****************************/
typedef enum Modbus_M_state_t_def
    {
        State_Req=0,State_Get,State_Put,State_Ack
    } Modbus_M_state_t;
      
struct ADD_t_Def{
    union 
        {
            uint16_t Val;
            uint8_t v[2];
            struct 
            {
                uint8_t HB;
                uint8_t LB;
            }byte;   
        }Add; 
    };
typedef struct ADD_t_Def ADD_t;

// Global Variables  ///////////////////////////////////
extern ADD_t Start_Add;
extern ADD_t Quantity;

/*    
typedef struct BUFFER_MB_t_Def      
{
    uint8_t byteArray[MB_Mx];
    uint8_t point;
}BUFFER_MB_t;

typedef union Buffer_MB_t_Def{  //ahorro memoria RAM son buffer que no se usan combinados
    BUFFER_MB_t byteReceiveArray;
    BUFFER_MB_t byteSendArray;
}Buffer_MB_t;

extern Buffer_MB_t Buffer_MB; //buffers del tipo union
*/

// Function Prototypes mias ///////////////////////////////////
uint8_t     swap_bits(uint8_t);
void        read_all_coils(void);
exception   read_coils(uint8_t);
void        read_all_inputs(void);
void        read_inputs(void);
void        read_all_holding(void);
void        read_all_input_reg(void);
void        read_input_reg(void);
exception   write_coils(void);
void        write_regs(void);
void        write_coil(void);
void        write_reg(void);
void        read_all_coils(void);
void        read_all_holding(void);
void        unknown_func(void);

exception        read_tip_Frame_Ava(void);
Modbus_M_state_t Serv_Master_Modbus_MIWI(void);

// Function Prototypes de modbustcp  ///////////////////////////////////
/*
void Put_byteSendArray(Buffer_MB_t *);

void MBRun(void);

void MBPopulateSendBuffer(uint8_t *, uint16_t);
void MFSetFC(uint16_t);
void MBbuffer_restore(void);
void MBbuffer_save(void);
int word(uint8_t, uint8_t);
void MB_wFloat(float, int);
float MB_rFloat(int);
*/



/* Speculations on Modbus message structure:
**********************************************
**********Master(PC) request frames***********
00 ID high              0
01 ID low               1
02 Protocol high        0
03 Protocol low         0
04 Message length high  0
05 Message length low   6 (6 bytes after this)
06 Slave number         1
07 Function code
08 Start address high   maybe 0
09 Start address low    maybe 0
10 Length high          maybe 125 or Data high if write
11 Length low           maybe 125 or Data low if write
**********************************************
**********Slave response frames******
00 ID high              echo  /           0
01 ID low               echo  /  slave ID 1
02 Protocol high        echo
03 Protocol low         echo
04 Message length high  echo
05 Message length low   num bytes after this
06 Slave number         echo
07 Function code        echo for OK, 80h+FC for Exception
08 Start address high   num bytes of data for OK, else Exception Code
09 Data high
10 Data low
**********************************************
*/

/* Exception Codes
**********************************************
01 Illegal Function         Implemented
02 Illegal Data Address     Implemented
03 Illegal Data Value       Not Needed? Application Specific?
04 Failure in Device        Not Needed. Application Specific.
All others are program or special function related
**********************************************
*/

#endif	/* MODBUS_H */

