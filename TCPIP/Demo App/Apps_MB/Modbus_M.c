/*
    Modbus_M.c - a Modbus TCP Slave Library for Netcruzer (PIC24)
    EMMERTEX - Andrew Frahn
    https://github.com/emmertex/Modbus-Library/

    Ported from Siamects variation of Mudbus.cpp by Dee Wykoff
    Arduino Library - http://gitorious.org/mudbus

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define THIS_IS_MODBUS_C

#include <string.h>
//#include <stdbool.h>

#include "APP_MIWI_ModBus.h"
#include "Modbus_M.h"
#include "mcc_generated_files/eusart1.h"
//#include "App.h" incluida en APL_FUN.h
#include "mcc_generated_files/TCPIPLibrary/tcpip_types.h"
#include "APL_Fun.h"

// MACROS  /////////////////////////////////////
#define lowByte(x)     ((uint8_t)((x)&0xFF))
#define highByte(x)    ((uint8_t)(((x)>>8)&0xFF))
#define lowWord(x)  ((uint16_t)((x)&0xFFFF))
#define highWord(x) (((uint16_t)(((x)>>16)&0xFFFF)))
#define wordstoDWord(hw,lw) ((((uint16_t)(hw&0xFFFF))<<16) | ((uint16_t)lw))
#define bitRead(value,bit) (((value) >> (bit)) & 0x01)
#define bitSet(value,bit) ((value) |= (1ul << (bit)))
#define bitClear(value,bit) ((value) &= ~(1ul <<(bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value,bit) : bitClear(value,bit))
#define bytesToWord(hb,lb) ( (((uint16_t)(hb&0xFF))<<8) | ((uint16_t)lb) )
#define Float_CD_AB    //else it is Float_CD_AB

/*-----------ADDRESS de GATEWAY-----------------------*/
#define MODBUS_MASTER_ADDRESS       0x01
#define MODBUS_SLAVE_ADDRESS        0x02
#define MODBUS_SLAVE_ADDRESS_LORA_0 0xA0
#define MODBUS_SLAVE_ADDRESS_PLC_0  0xB0
/*-----------MEM ADDRESS por tipo de trama------------*/
//@TODO revisar estas constantes
#define START_ADD           0x00
#define LGTH_Read_Tp        0x08  //bobinas
#define QUANTITY_Read_Norm  0x08  //bobinas
#define QUANTITY_Read_Aso   0x08  //bobinas
#define QUANTITY_Read_Time  0x08  //bobinas
#define QUANTITY_Read_Aso_PN 0x08  //bobinas
#define QUANTITY_Read_Ctrl 0x08  //bobinas

#define LGTH_Write_Tp       0x08  //bobinas
#define QUANTITY_Write_Norm 0x08  //bobinas
#define QUANTITY_Write_Aso  0x08  //bobinas
#define QUANTITY_Write_Time 0x08  //bobinas
#define QUANTITY_Write_Aso_PN 0x08  //bobinas
#define QUANTITY_Write_Ctrl 0x08  //bobinas

/*Fixed Length of frames becose type*/
/*
#define Length_Trama_Short  3
#define Length_Trama_nOP    3
#define Length_Trama_Norm   24
#define Length_Trama_Aso    13
#define Length_Trama_Time   0
#define Length_Trama_Pan    8
#define Length_Trama_Con    20
*/
// Global Variables  ///////////////////////////////////
ADD_t Start_Add;     //define el incio de lectura escritura colis reg para operacion MB
ADD_t Quantity;      //cantidad de lectura escritura colis reg para operacion MB
/*******************************************************
 * Variable define estado de ACK
 ******************************************************/
bool state_Ack= true;

/*This function may come in handy for you since MODBUS uses MSB first.*/
uint8_t swap_bits(uint8_t c)
{
   return ( (uint8_t) (((c&1)?128:0)|((c&2)?64:0)|((c&4)?32:0)|((c&8)?16:0)|((c&16)?8:0)|((c&32)?4:0)|((c&64)?2:0)|((c&128)?1:0)) );
}

void read_all_coils(void)
{
   modbus_read_coils(MODBUS_SLAVE_ADDRESS,0,8);
}
/*
 read_coils
 Leo rama disponibles en el PAN segun su tipo
 La info recibiida se almacena en el buffer del stack
 */
exception read_coils(uint8_t t)
{ 
    if(state_Ack){
        
        switch (t){
            //@TODO completar argumentos sergun lo que sea necesario, implementar defines
            case OP_NORMAL: //TRAMA DE NORMAL trama larga
                    //trama larga normal
                    modbus_read_coils(MODBUS_SLAVE_ADDRESS,START_ADD,QUANTITY_Read_Norm);
                    --state_Ack;      //paso a leer la respuesta
                    return(SLAVE_DEVICE_BUSY);
                break; 
                case OP_ASOCIACION: //TRAMA DE ASOCIACION Trama corta
                    //trama corta asociacion
                    modbus_read_coils(MODBUS_SLAVE_ADDRESS,START_ADD,QUANTITY_Read_Aso);//id:16|tipo:4|sec:8|
                    --state_Ack;     //paso a leer la respuesta
                    return(SLAVE_DEVICE_BUSY);
                break;
                case OP_TIME: 
                        //trama corta TIME
                    modbus_read_coils(MODBUS_SLAVE_ADDRESS,START_ADD,QUANTITY_Read_Time);//id:16|tipo:4|sec:8|
                    --state_Ack;     //paso a leer la respuesta
                    return(SLAVE_DEVICE_BUSY);
                break;
                case OP_ASOCIACION_PAN: 
                     //trama corta asociacion PAN
                    modbus_read_coils(MODBUS_SLAVE_ADDRESS,START_ADD,QUANTITY_Read_Aso_PN);//id:16|tipo:4|sec:8|
                    --state_Ack;     //paso a leer la respuesta
                    return(SLAVE_DEVICE_BUSY);
                break;
                case OP_CONTROL: 
                    //trama CONTROL
                    modbus_read_coils(MODBUS_SLAVE_ADDRESS,START_ADD,QUANTITY_Read_Ctrl);//id:16|tipo:4|sec:8|
                    --state_Ack;     //paso a leer la respuesta
                    return(SLAVE_DEVICE_BUSY);
                break;
                
                default:  //TRAMA fuera de protocolo
                     return(ILLEGAL_FUNCTION);          
        }            
        return(ILLEGAL_FUNCTION);
        
    }else{
        switch(MODBUS_SERIAL_RESPONSE(MODBUS_SLAVE_ADDRESS,FUNC_READ_COILS)){
            
            case SLAVE_DEVICE_BUSY:
                return(SLAVE_DEVICE_BUSY);
            break;
            case ACKNOWLEDGE:    
                ++state_Ack;
                return(ACKNOWLEDGE);
            break;
            case TIMEOUT:    
                ++state_Ack;
                return(TIMEOUT);
            default:
                ++state_Ack;                
                return(ILLEGAL_FUNCTION);
        }    
    }        
}

void read_all_inputs(void)
{
    modbus_read_discrete_input(MODBUS_SLAVE_ADDRESS,0,8);
}

void read_inputs(void)
{
    modbus_read_discrete_input(MODBUS_SLAVE_ADDRESS,0,8);
}

void read_all_holding(void)
{
   modbus_read_holding_registers(MODBUS_SLAVE_ADDRESS,0,8);
}

void read_all_input_reg(void)
{
 
   modbus_read_input_registers(MODBUS_SLAVE_ADDRESS,0,8);
}

void read_input_reg(void)
{
 
   modbus_read_input_registers(MODBUS_SLAVE_ADDRESS,0,8);
}

void write_coil(void)
{
   modbus_write_single_coil(MODBUS_SLAVE_ADDRESS,6,true);
}

void write_reg(void)
{
   modbus_write_single_register(MODBUS_SLAVE_ADDRESS,3,0x4444);
}
/*
 wirte_coils
 Escribo trama en el PAN segun su tipo
 
 */
exception write_coils(void)
{
   //puntero al buffer de transmision preparado previamente
   //uint8_t coils[1] = { 0x50 };
   uint8_t t,Lengt;
   
   if(state_Ack){
        //@TODO modificar esto de las longitud de tramas
        if(App1.Tipo == OP_NORMAL)
            Lengt= 88;
        
        else if(App1.Tipo== OP_ASOCIACION)
            Lengt= 104;
            //trama larga normal y asocia
            //modbus_read_coils(MODBUS_SLAVE_ADDRESS,0,300);
            
        modbus_write_multiple_coils(MODBUS_SLAVE_ADDRESS,0,Lengt,Tx_Buffer_MODMIWI.Message[Tx_Buffer_MODMIWI.point].Payload);

        --state_Ack;
        
    }else{
        switch(MODBUS_SERIAL_RESPONSE(MODBUS_SLAVE_ADDRESS,FUNC_WRITE_MULTIPLE_COILS)){
            
            case SLAVE_DEVICE_BUSY:    
                return(SLAVE_DEVICE_BUSY);
            break;
            
            case ACKNOWLEDGE:    
                ++state_Ack;
                return(ACKNOWLEDGE);
            break;
            
            default:// TIMEOUT:    
                ++state_Ack;
                return(TIMEOUT);
        }    
    }
   
   return(SLAVE_DEVICE_BUSY);
}

void write_regs(void)
{
   uint16_t reg_array[2] = {0x1111, 0x2222};
   
   modbus_write_multiple_registers(MODBUS_SLAVE_ADDRESS,0,2,reg_array);
      
}

void unknown_func(void)
{
   
   modbus_diagnostics(MODBUS_SLAVE_ADDRESS,0,0);
   
}
/**************************************************************************
 *read_tip_Frame_Ava
 *Leo el tipo de trama disponibles en el PAN lista para recibir
 *La info recibiida se almacena en el buffer del stack
 * Read coil desde 15, 8 bits o bobinas entonces hasta la posicion 15->23
 **************************************************************************/
 exception read_tip_Frame_Ava(void){
    
    if(state_Ack){
        //@TODO comprobar argumentos sobre la posicion de los datos al leer
        modbus_read_coils(MODBUS_SLAVE_ADDRESS,START_ADD,LGTH_Read_Tp);// Deseo leer APP.TIPO
        --state_Ack;
    }else{
        switch(MODBUS_SERIAL_RESPONSE(MODBUS_SLAVE_ADDRESS,FUNC_READ_COILS)){ //espero la lectura  
            
            case SLAVE_DEVICE_BUSY:    
                return(SLAVE_DEVICE_BUSY);
            break;
            case ACKNOWLEDGE:    
                ++state_Ack;
                return(ACKNOWLEDGE);
            break;
            default:// TIMEOUT:    
                ++state_Ack;
                return(TIMEOUT);
        }    
    }
    return(SLAVE_DEVICE_BUSY);  
}

/****************************************************************************
 * Define una comunicacion de maquina de estandos, asincronica sin INT
 * **************************************************************************/
Modbus_M_state_t Serv_Master_Modbus_MIWI(void)
{      
   static Modbus_M_state_t state= State_Req; 
   static bool st= false;
   //static TIPO_t tipo;
   
   switch(state)
   {  
       /************************************************************************
        * * 1ro Serv_MIWI(RECB)
        * **********************************************************************
        * Solicita si hay tramas nuevas para leer
        * Envia un read coil de tipo en el PAN 
        * Estructura de maquina de estados que evoluciona por ASK OK
        ***********************************************************************/
        case State_Req:
            /***************************************************
            * Consulta si llegaron las tramas nuevas para leer            
            ****************************************************/  
            if (ACKNOWLEDGE == read_tip_Frame_Ava()) 
            { //solicito tipo de trama disponible en PANCO
                state= State_Get;               
                /* estando aqui los datos enviados por el esclavo estan 
                   La decision permite ajustar segun eltipo de trama ya que la secuencia
                   de intercambios es diferente en tramas de asociacion y time*/
                //@TODO ajustar a la posicion exacta del tipo en la trama
                switch(modbus_rx.data[POST_TIPE])  //primer elemento del buffer obedece al TIPO (*modbus_rx.data)
                {   
                    case OP_NORMAL:
                        /* Caso que haya trama LARGA valida  recibida modbus*/
                        state= State_Get;
                    break; 
                    case OP_ASOCIACION:
                        /* Caso que haya trama CORTA valida recibida modbus*/
                        state= State_Put;
                    break;
                    case OP_TIME:
                        /* Caso que haya trama CORTA valida recibida modbus*/
                        state= State_Put;
                    break;  
                    case OP_ASOCIACION_PAN:
                        /* Caso que haya trama CORTA valida recibida modbus*/ 
                        state= State_Put;
                    break;  
                    case OP_CONTROL:
                        /* Caso que haya trama LARGA valida  recibida modbus*/
                        state= State_Get;
                    break;      
                    case nOP:
                        /* Caso que haya trama CORTA valida recibida modbus*/ 
                        state= State_Put;
                    break;

                    default: //TRAMA fuera de protocolo
                        state= State_Req;
                }
            } 
        break;            
       
        case State_Get:
             /******************************************
              * ESTANDO AQUI YA SE QUE DEBO SOLICITAR AL PAN POR ESTAR DISPONIBLE
              *  
              *****************************************/  
             /*modbus_serial_new: indica si hubo recepcion completa de trama modbus de ack*/
                                            
            switch(App1.Tipo)  
            {   //primer elemento del buffer obedece al TIPO             
                case OP_NORMAL: //TRAMA DE NORMAL trama larga
                    switch(read_coils(OP_NORMAL))
                    {
                        case ACKNOWLEDGE:
                            state=State_Put;
                            
                            if (App1.Tipo != *(modbus_rx.data+POST_TIPE)){ 
                                /*No see respeto la secuencia*/                                                                                                   
                                state= State_Req;
                            }
                        break;    
                        case TIMEOUT:
                            state= State_Req;
                            
                        break; 
                        default:
                            state= State_Get;
                    }
                break; 
                case OP_ASOCIACION: //TRAMA DE ASOCIACION Trama corta
                    /*Los datos ya estan disponibles en la primer consulta*/
                    /*
                        switch(read_coils(OP_ASOCIACION))
                        {
                            case ACKNOWLEDGE:
                                state= State_Put;
                            break;    
                            case TIMEOUT:
                                state= State_Req;
                            break;
                            default:
                               state= State_Get;
                        }
                    */
                    state= State_Put;
                break;
                case OP_TIME:
                    /*Los datos ya estan disponibles en la primer consulta*/
                    /*
                        switch(read_coils(OP_TIME))
                        {
                            case ACKNOWLEDGE:
                                state= State_Put;
                            break;    
                            case TIMEOUT:
                                state= State_Req;
                            break;
                            default:
                               state= State_Get;
                        }
                    */ 
                    state= State_Put;
                break;
                case OP_ASOCIACION_PAN: //TRAMA DE ASOCIACION Trama corta
                    /*Los datos ya estan disponibles en la primer consulta*/
                    /*
                        switch(read_coils(OP_ASOCIACION_PAN))
                        {
                            case ACKNOWLEDGE:
                                state= State_Put;
                            break;    
                            case TIMEOUT:
                                state= State_Req;
                            break;
                            default:
                               state= State_Get;
                        }
                    */
                    state= State_Put;
                break;
                case OP_CONTROL: 
                    switch(read_coils(OP_CONTROL))
                    {
                        case ACKNOWLEDGE:
                            state= State_Put;
                            if (App1.Tipo != *(modbus_rx.data+POST_TIPE)){ 
                                /*No see respeto la secuencia*/                                                                                                   
                                state= State_Req;
                            }
                        break;    
                        case TIMEOUT:
                            state= State_Req;
                        break;
                        default:
                           state= State_Get;
                    }
                break;
                case nOP:
                    state= State_Put;
                break;

                default:  //TRAMA fuera de protocolo
                    state= State_Req;
            }

              /******************************************
              * ESTANDO AQUI siendo state=State_Put;
              * EL stack tiene una trama con paylod listo para ser leido por miwi
              * Pocesar y pasar a la siguiente estado 
              *****************************************/  

              /******************************************
              * Hasta aca llego con la consultas luego en la presedencia
              * 1ro Serv_MIWI(RECB)
              * 2do Serv_MIWI(TRANS)
              *  
              *****************************************/      

        break;
         
       /***********************************************
        *  2do Serv_MIWI(TRANS)          
        ***********************************************
        * Escribe los datos que procesados para el PAN
        * Luego este los descarga en la red modbus
        **********************************************/  
        case State_Put:

            //if (write_coils() == ACKNOWLEDGE) state=State_Ask;
            //@TODO definir decision segun dea escribir o leer    
          switch(write_coils())
          {
              case ACKNOWLEDGE:
                    state= State_Ack;
              break;    
              case TIMEOUT:
                  /*----------------------------------------------
                   * state=State_Req; o  anular comunicacion ya que 
                   * de el sistema retorna a escucha y puede tome 
                   * otros datos luego si no salgo de Serv_MIWI(TRANS)
                   * en la rama superior de la maquina de estados 
                   * evoluciona REQ->PUT cuando solo requiero transmitir  
                  --------------------------------------------- */  
                    state= State_Req;
              break;  
              default:
                    state= State_Put;
          }
        break; 
       /***********************************************
        * Escucho si llega la confirmacion
        * Envia un read reg de tipo en el PAN 
        **********************************************/  
        case State_Ack:

            state= State_Req; //vuelvo al inicio de estados

        break;          
   }
   
   return(state);
 
}
