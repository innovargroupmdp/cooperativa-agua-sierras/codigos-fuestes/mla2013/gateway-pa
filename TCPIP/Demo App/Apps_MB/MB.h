/* 
 * File:   MIWI.h
 * Author: cba
 *
 * Created on April 9, 2018, 9:52 AM
 */

#ifndef MIWI_H
#define	MIWI_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* MIWI_H */

typedef enum  State_t_Def
{
    RECB_MB=0,
    TRANS_MB
}State_t; // estado inicial al inicio

typedef enum  COM_State_t_Def
{
    FAIL=0,
    ACK,
    WAIT        
}COM_State_t;

//Can only be used on structures, due to address misalignment issues on 16bit and 32bit platforms
#define u16(a)  *(WORD*)(&a)
// Defines  /////////////////////////////////////
#define lowNipple(x)        ((BYTE)((x)&0x0F))
#define highNipple(x)       ((BYTE)(((x)>>4)&0x0F))
#define halfnippleToByte(hn,ln) ( (((hn)&0x0F)<<4) | (highNipple(ln))) 

#define lowByte(x)          ((BYTE)((x)&0xFF))
#define highByte(x)         ((BYTE)(((x)>>8)&0xFF))
#define lowWord(x)          ((WORD)((x)&0xFFFF))
#define highWord(x)         (((WORD)(((x)>>16)&0xFFFF)))
#define wordstoDWord(hw,lw) ((((WORD)(hw&0xFFFF))<<16) | ((WORD)lw))
#define bitRead(value,bit)  (((value) >> (bit)) & 0x01)
#define bitSet(value,bit)   ((value) |= (1ul << (bit)))
#define bitClear(value,bit) ((value) &= ~(1ul <<(bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value,bit) : bitClear(value,bit))
#define bytesToWord(hb,lb)             ( (((WORD)(hb&0xFF))<<8) | ((WORD)lb) )
#define Float_CD_AB    //else it is Float_CD_AB


/*****************************DEFINES*******************************************/

#define TIPO_MB 2
#define Length_Trama_Short  4
#define Length_Trama_nOP    4
#define Length_Trama_Norm   24
#define Length_Trama_Aso    6
#define Length_Trama_Time   6
#define Length_Trama_Pan    6
#define Length_Trama_Ctrl   12

#define nOP                 0 
#define OP_NORMAL           1 
#define OP_ASOCIACION       2
#define OP_TIME             3
#define OP_ASOCIACION_PAN   4
#define OP_CONTROL          5
#define OP_ASOCIACION_GW    6
#define OP_FAILURE          0xff


/*Declaracion de prototipos*/
void Ini_MB(void);
void Put_RXBufferMB(void);
COM_State_t FunRx_MB(void);
COM_State_t FunTx_MB(void);
//bool Serv_MIWI(void);
//COM_State_t Serv_MB(State_t);
