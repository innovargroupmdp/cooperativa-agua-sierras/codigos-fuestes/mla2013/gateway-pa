/**
  Generated Pin Manager File

  Company:
    Microchip Technology Inc.

  File Name:
    pin_manager.c

  Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC18F26K20
        Driver Version    :  2.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.45
        MPLAB             :  MPLAB X 4.15

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "pin_manager.h"
#include "MiWi.h"




/*********************************************************************
 * Function:        void PIN_MANAGER_Initialize( void )
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    Board is initialized for P2P usage
 *
 * Overview:        This function configures the board 
 *
 * Note:            This routine needs to be called before the function 
 *                  to initialize stack or any other function that
 *                  operates on the stack
 ********************************************************************/
void PIN_MANAGER_Initialize(void)
{
    /**
    LATx registers
    */
    LATA = 0x00;
    LATB = 0x00;
    LATC = 0x00;
    LATG = 0x00;
    LATF = 0x00;
    LATE = 0x00;
    LATG = 0x00;
    nRE_LAT= HIGH;
    DE_LAT=  LOW;
    /**
    TRISx registers
    */
    TRISA = 0xFF;
    TRISB = 0xFF;
    TRISC = 0xFF;
    TRISG = 0xFF;
    TRISF = 0xFF;
    TRISE = 0xFF;
    TRISG = 0xFF;
    nRE_TRIS= OutPut;
    DE_TRIS=  OutPut;
    
    /*******************************************************************/
    // AN0 & AN1 Analog Pins others Digital Pins
    /*******************************************************************/
    //ANSELH= 0;
    //ANSEL=  0;
    
    ADCON0 = 0x09;        // ADON, Channel 2
    ADCON1 = 0x0B;        // Vdd/Vss is +/-REF, AN0, AN1, AN2, AN3 are analog        
    ADCON2 = 0xBE;        // Right justify, 20TAD ACQ time, Fosc/64 (~21.0kHz)
    // Do a calibration A/D conversion    
    ADCON0bits.ADCAL = 1;
    ADCON0bits.GO = 1;
    while(ADCON0bits.GO);
    ADCON0bits.ADCAL = 0;    
    
    /*******************************************************************/
    // Configure Switch and LED I/O Ports
    /*******************************************************************/
    LED=       OFF;       
    LED0_TRIS= OutPut;
             
    SW1_TRIS=  InPut;
    //SW2_TRIS = InPut;      
								
    INTCON2bits.RBPU= PULL_UP_DISABLED;   // Enable PORTB Pull-ups for Switches

    /*******************************************************************/
    // Configure the Temp Sensor and VBat port
    /*******************************************************************/	
    //TRISAbits.TRISA1 = INPUT;
    //TRISAbits.TRISA0 = INPUT;
    
    /*******************************************************************/
	// Config RF Radio
	/*******************************************************************/
#if defined(MRF24J40)
    /*******************************************************************/
	// Config MRF24J40 Pins
	/*******************************************************************/
	PHY_CS=     ON;
	PHY_RESETn= ON;    
    PHY_WAKE=   ON;
    
    PHY_CS_TRIS=     OutPut;
    PHY_RESETn_TRIS= OutPut;        
    PHY_WAKE_TRIS=   OutPut;
    RF_INT_TRIS=     InPut;                            //TRISBbits.TRISB1
             
    // Config INT0 Edge = Falling
    INTCON2bits.INTEDG1= 0;
    
    RFIF= 0;
    RFIE= 1;
#endif
    
    /*******************************************************************/
    // Confiure SPI1
    /*******************************************************************/     
    #if defined(EEPROM_CS_TRIS)
    EEPROM_CS_IO = 1;
    EEPROM_CS_TRIS = 0;
    #endif
    #if defined(SPIRAM_CS_TRIS)
    SPIRAM_CS_IO = 1;
    SPIRAM_CS_TRIS = 0;
    #endif
    #if defined(SPIFLASH_CS_TRIS)
    SPIFLASH_CS_IO = ON;
    SPIFLASH_CS_TRIS = OutPut;
    #endif

    SDI_TRIS = InPut;
    SDO_TRIS = OutPut;
    SCK_TRIS = OutPut;
    
    SSP1STAT = 0xC0;
    SSP1CON1 = 0x20;  
    
    /*******************************************************************/
    // Configure USART 2
    /*******************************************************************/
    //  Ro_TRIS=1;          //RX2
    //  Di_TRIS=0;          //TX2
    //RCSTAbits.SPEN;    //Enable
  
   
    #if defined(MRF89XA)
    /*******************************************************************/
	// Config MRF89XA Pins
	/*******************************************************************/ 
    Data_nCS = 1;
    Config_nCS = 1;
    
    Data_nCS_TRIS = 0; 
    Config_nCS_TRIS = 0;   
    
    IRQ0_INT_TRIS = 1;
    IRQ1_INT_TRIS = 1;
    
    // Config IRQ1 Edge = Rising
    INTCON2bits.INTEDG1 = 1;
    
    // Config IRQ0 Edge = Falling
    INTCON2bits.INTEDG0 = 1;
    
    PHY_IRQ0 = 0;           // MRF89XA
    PHY_IRQ0_En = 1;        // MRF89XA  
    PHY_IRQ1 = 0;           // MRF89XA
    PHY_IRQ1_En = 1;        // MRF89XA    
    /*******************************************************************/
    // Confiure SPI1
    /*******************************************************************/   
    SDI_TRIS = 1;
    SDO_TRIS = 0;
    SCK_TRIS = 0;
    
    SSP1STAT = 0xC0;
    SSP1CON1 = 0x21;

    PHY_RESETn = 0;
    PHY_RESETn_TRIS = 0;

    #endif
    
    /*******************************************************************/    
    // Configure EEProm Pins
    /*******************************************************************/
    /*
    RF_EEnCS = 1;
    RF_EEnCS_TRIS = 0;
    EE_nCS = 1;
    EE_nCS_TRIS = 0;
    */ 
    
    /*******************************************************************/
    // Configure LCD Pins
    /*******************************************************************/
    /*
    LCD_BKLT = 0;
    LCD_CS = 1;
    LCD_RS = 0;
    LCD_RESET = 0;
    LCD_BKLT_TRIS = 0;
    LCD_CS_TRIS = 0;
    LCD_RS_TRIS = 0;
    LCD_RESET_TRIS = 0;
    */
    /*******************************************************************/
    // Configure SPI2
    /*******************************************************************/   
    /*
    SDI2_TRIS = 1;
    SDO2_TRIS = 0;
    SCK2_TRIS = 0;
    
    SSP2STAT = 0x00;
    SSP2CON1 = 0x31;
    
    PIR3bits.SSP2IF = 0; 
    */
    /*******************************************************************/
    // Enable System Interupts
    /*******************************************************************/
    
    //INTCONbits.GIEH = 1;
    
    /**
    WPUx registers
    */
    
    //INTCON2bits.NOT_RBPU = 1;

    /**
    ODx registers
    */
    
}
  
void PIN_MANAGER_IOC(void)
{   
	// Clear global Interrupt-On-Change flag
    INTCONbits.RBIF = 0;
}

/**
 End of File
*/