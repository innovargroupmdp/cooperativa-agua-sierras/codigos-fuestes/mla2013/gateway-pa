/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "spi_driver.h"

#pragma warning disable 520        

void spi_close(void)
{
    SSPCON1bits.SSPEN = 0;
}

//con1 == SSPxCON1, stat == SSPxSTAT, add == SSPxADD
typedef struct { 
    BYTE con1; 
    BYTE stat; 
    BYTE add; 
} spi_configuration_t;
    
static const spi_configuration_t spi_configuration[] = {   
    {0x0, 0xc0, 0x1 }
};

//Setup SPI for Master Mode
BOOL spi_master_open(spi_modes spiUniqueConfiguration)
{
    if(!SSPCON1bits.SSPEN)
    {
        SSPSTAT = spi_configuration[spiUniqueConfiguration].stat;
        SSPCON1 = spi_configuration[spiUniqueConfiguration].con1 | 0x2A;
        SSPCON2 = 0x00;
        SSPADD = spi_configuration[spiUniqueConfiguration].add | 0;

        TRISCbits.TRISC3 = 0;
        return TRUE;
    }
    return FALSE;
}

//Setup SPI for Slave Mode
bool spi_slave_open(spi_modes spiUniqueConfiguration)
{
    if(!SSPCON1bits.SSPEN)
    {
        SSPSTAT = spi_configuration[spiUniqueConfiguration].stat;
        SSPCON1 = spi_configuration[spiUniqueConfiguration].con1 | 0x25;
        SSPCON2 = 0x00;
        SSPADD = spi_configuration[spiUniqueConfiguration].add | 0;
        
        TRISCbits.TRISC3 = 1;
        return TRUE;
    }
    return FALSE;
}

// Full Duplex SPI Functions
BYTE spi_exchangeByte(BYTE b)
{
    SSPBUF = b;
    while(!PIR1bits.SSPIF);
    PIR1bits.SSPIF = 0;
    return SSPBUF;
}
//aseguro que fue escrito en ssp buffer
void spi_exchangeBlock(void *block, size_t blockSize)
{
    BYTE *data = block;
    while(blockSize--)
    {
        *data = spi_exchangeByte(*data );
        data++;
    }
}

// Half Duplex SPI Functions
void spi_writeBlock(void *block, size_t blockSize)
{
    BYTE *data = block;
    while(blockSize--)
    {
        spi_exchangeByte(*data++);
    }
}

void spi_readBlock(void *block, size_t blockSize)
{
    BYTE *data = block;
    while(blockSize--)
    {
        *(data++) = spi_exchangeByte(0);
    }
}
