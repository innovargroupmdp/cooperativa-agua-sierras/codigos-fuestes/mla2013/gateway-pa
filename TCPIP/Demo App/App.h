/* 
 * File:   App.h
 * Author: cba
 *
 * Created on February 13, 2018, 4:04 PM
 */
#include "GenericTypeDefs.h"

/************************ MACROS **********************************/
//Can only be used on structures, due to address misalignment issues on 16bit and 32bit platforms
#define u16(a)  *(WORD*)(&a)
// Defines  /////////////////////////////////////
#define lowNipple(x)        ((BYTE)((x)&0x0F))
#define highNipple(x)       ((BYTE)(((x)>>4)&0x0F))
#define halfnippleToByte(hn,ln) ( (((hn)&0x0F)<<4) | (highNipple(ln))) 

#define lowByte(x)          ((BYTE)((x)&0xFF))
#define highByte(x)         ((BYTE)(((x)>>8)&0xFF))
#define lowWord(x)          ((WORD)((x)&0xFFFF))
#define highWord(x)         (((WORD)(((x)>>16)&0xFFFF)))
#define wordstoDWord(hw,lw) ((((WORD)(hw&0xFFFF))<<16) | ((WORD)lw))
#define bitRead(value,bit)  (((value) >> (bit)) & 0x01)
#define bitSet(value,bit)   ((value) |= (1ul << (bit)))
#define bitClear(value,bit) ((value) &= ~(1ul <<(bit)))
#define bytesToWord(hb,lb)  ( (((WORD)(hb&0xFF))<<8) | ((WORD)lb) )
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value,bit) : bitClear(value,bit))
#define Float_CD_AB    //else it is Float_CD_AB

/********************************************************************************/


#define MW_CHANNEL      24

#define TOMAS           2
#define CTRL_LNGTH      1     //payload of frame 
#define BS_CTRL_LNGTH   1     //tramas de ctrl de cada buffer para cada PAN
#define ALRM_BFFR       1
#define BFFR_RX_LNGTH   1
#define BUFFER_LNGTH    1      //longitud buffer osociacion

#define BASE_SLT_LNGTH  5     //
#define TBL_ID_MB_LNGTH 1
#define BS_TK_LNGTH     1      // 1 minuto     
#define N_PAN           2      // cantidad de Pan cord por GW  

#define ADDRESS_LEN     8
#define LENGTH_PASS     10
#define POST_TIPE       3
// indice tabla Cntrl
#define LCL_MiWi        0     // indice de aplicacion en Local MIWI
#define LCL_LoRa        1     // indice de aplicacion en Local LoRa
#define LCL_PLC         2     // indice de aplicacion en Local PLC
#define LCL_FRee        4     // indice que apunta a direccion libre para ubicacion de PAN , LORa , PLC , Rs-232/484 

/*****************************DEFINES*******************************************/

#define nOP                 0 
#define OP_NORMAL           1 
#define OP_ASOCIACION       2
#define OP_TIME             3
#define OP_ASOCIACION_PAN   4
#define OP_CONTROL          5
#define OP_ASOCIACION_GW    6
#define OP_FAILURE          0xff

/* Sub codigos App.Tipo>=5;
            * 00-09: De RED:Cambio de Slot, RFD, FFD.
            * 10-19: De Monitoreo Interno: temperatura, humedad, scaneo, consumo, generacion solar, velociadad viento.
            * 20-29: De Control Interno: reset POW, reset blank, cambio PAN, cambio de canal, cambio de banda, control potencia, T de sleep.
            * 30-39: De Control Externo: Control IOs, USB, CAN, USART,Display.
            * 40-49: De Control Datos: Escritura de Variable, Lectura de Variable.
            * 50-59: De Control Programa: Control de flujo de programa(vector de punteros a funciones).
            * 60-69:
            * 70-79:
            * 80-89:
            * 90-99:
*/
#define Ctrl_NET            0
#define Ctrl_NET_SLOT       0
#define Ctrl_NET_CHG_PAN    1
#define Ctrl_NET_CHG_CHN    2
#define Ctrl_NET_CHG_BND    3
#define Ctrl_NET_CHG_PTX    4
        
#define Ctrl_VIEW       1
#define Ctrl_VIEW_TEMP  0
#define Ctrl_VIEW_SCN   1
#define Ctrl_VIEW_PWR   3
#define Ctrl_VIEW_GNS   4
#define Ctrl_VIEW_WND   5

#define Ctrl_IN         2
#define Ctrl_IN_POR     0
#define Ctrl_IN_RSTM    1
#define Ctrl_IN_T_SLP   2


#define Ctrl_OUT       3
#define Ctrl_OUT_IO    0
#define Ctrl_OUT_USB   1
#define Ctrl_OUT_CAN   2
#define Ctrl_OUT_SPI   3
#define Ctrl_OUT_I2C   4
#define Ctrl_OUT_USART 5
#define Ctrl_OUT_DSPLY 6

#define Ctrl_DAT       4
#define Ctrl_DAT_EEP   0
#define Ctrl_DAT_SST   1
#define Ctrl_DAT_FLH   2

#define Ctrl_PRG       5
#define Ctrl_PRG_PTR   0

/*Definiciones de direcciones de memoria en NVM*/
//         25AA02E48 2k 48 bits MAC
#define    NVM_ADD_MyPANID              0 // 2 BYTES
#define    NVM_ADD_CurrentChannel       2 // 1 BYTES
#define    NVM_ADD_ConnMode             
#define    NVM_ADD_ConnectionTable      
#define    NVM_ADD_OutFrameCounter

#define    NVM_ADD_MyShortAddress       3 // 2 BYTES
#define    NVM_ADD_MyParent             5 // 2 BYTES

           // Definidas por mi
#define    NVM_ADD_TOKEN_INI            7  // 2 BYTES
#define    NVM_ADD_ID                   9  // 2 BYTES     
#define    NVM_ADD_TIME                 11 // 1 BYTES
#define    NVM_ADD_PASS_INI             12 // 10 BYTES + \0
           // Bits de estado
#define    NVM_ADD_OPE                  100   // 1 Bit
#define    NVM_ADD_INI                  NVM_ADD_OPE // 1 Bit

#define    NVM_ADD_RoutingTable 
#define    NVM_ADD_FamilyTree   
#define    NVM_ADD_NeighborRoutingTable
#define    NVM_ADD_KnownCoordinators
#define    NVM_ADD_Role  
/***********************************************/

// Define a header structure for validating the AppConfig data structure in EEPROM/Flash
typedef struct
{
	unsigned short wConfigurationLength;	// Number of bytes saved in EEPROM/Flash (sizeof(APP_CONFIG))
	unsigned short wOriginalChecksum;		// Checksum of the original AppConfig defaults as loaded from ROM (to detect when to wipe the EEPROM/Flash record of AppConfig due to a stack change, such as when switching from Ethernet to Wi-Fi)
	unsigned short wCurrentChecksum;		// Checksum of the current EEPROM/Flash data.  This protects against using corrupt values if power failure occurs while writing them and helps detect coding errors in which some other task writes to the EEPROM in the AppConfig area.
} NVM_VALIDATION_STRUCT;

enum TIPO_t_Def{
        noP=0,
        normal,
        aso,
        tm,
        aso_pan,
        ctrl,          
        aso_gw 
    };

struct PASS_t_Def{
        union 
        {
            WORD Val;
            BYTE v[2];
            struct 
            {
                BYTE HB;
                BYTE LB;
            }byte;   
        }Id_GW;             // ID de GATEWAy
        
        union 
        {
            WORD Val;
            BYTE v[2];
            struct 
            {
                BYTE HB;
                BYTE LB;
            }byte;   
        }TK_SRV;            // token dinamico frente a APP servidor        
        
        union 
        {
            WORD Val;
            BYTE v[2];
            struct 
            {
                BYTE HB;
                BYTE LB;
            }byte;   
        }TK_GW;             // token dinamico frente a APP Cntrl GW
        
        char   Pass_SRV[LENGTH_PASS +1];
        char   Pass_GW [LENGTH_PASS +1];
        BYTE Sec;        // secuencia del GW
    };

struct DATE_t_Def      
    {
        BYTE Seg;
        BYTE Min;
        BYTE Hou;
        BYTE Day;
        BYTE Mon;
        BYTE Yea;
    };//DATE_t;
    
struct DATERE_t_Def      
    {    
        BYTE Hou;
        BYTE Day;
        BYTE Mon;    
    };//DATERE_t;
    
struct DATECons_t_Def      
    {
    
        BYTE Seg;
        BYTE Min;
        BYTE Hou;
    
    };//DATERECons_t;
    
struct MED_t_Def      
    {
        struct DATERE_t_Def  DateMesu;
        WORD             Mesure;          
    };

struct MED_DIF_t_Def
    {    
        BYTE   Index;                    // apunta ultima medida disponible [0-(Tomas-1)]
        BYTE   MedidaD[TOMAS];
    };    
    
struct DATCTRL_t_Def
    {    
        BYTE   Index;                    // apunta ultimo elemento [0-(CTRL_LNGTH-1)]
        BYTE   Data[CTRL_LNGTH];
    };        
    
struct APP_t_Def                    // Tipos para variables de intercambio ModBus
    {           
        /*-HEADER MB-*/        
        BYTE   Sec_GW;
        BYTE   Sec_PAN;                  //Para control de paquetes MB
        /*-DATA OF LAYER-*/
        /*-HEADER MIWI-*/
        BYTE   Tipo;                     // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: ASOCI ||T=5: control
        WORD   ID;
        WORD   TOKEN;                    //Autentica Transaccion del Mimias Frente a la aplicacion del Servidor                 
        /*-DATA Of APPLICATION-*/        
        BYTE   Secuencia;
        WORD   Slot;
        BYTE   Nro_intentos;
        BYTE   Pot;
        BYTE   Noise;
        BYTE   Signal;
        BYTE   Bat;
        
        struct DATE_t_Def     DateTx;     // no reducir se usa para actualizar rtcc
        struct MED_t_Def      MedidaRE;   //medida ultima
        struct MED_DIF_t_Def  MedidaDif;
        WORD   ID_AUT;                    //Recibida para mimia
        
        /*-DATA Of APPLICATION CTRL-*/ 
        WORD   TKMM;                     //Token de aplicacion (APP) Control Mimia   
        BYTE   SubTipo;                  //Valido para tramas de control para capa aplicacion           
        struct DATCTRL_t_Def  DatCtrl;    //Datos provecientes de Tramas de control   
        
        /*-META DATA MB*********/
        BOOL   RW_op;                    // Read=true/input Write=fasle/output
        BOOL   PermanentAdd;
        BYTE   Add[ADDRESS_LEN];
    };

struct APP_tcp_t_Def                    // Tipos para variables de intercambio para tcp-ip
    {   
        /*-HEADER TP-IP--*/
        char      Pass_SRV[LENGTH_PASS+1];      
        char      Pass_GW [LENGTH_PASS+1];
        WORD  ID_GW_AUT;                // Recibida para GW autencicacion
        WORD  ID_GW;                    // permanete de GW
        WORD  TK_SrvGw;                 // Llave token de APP en servidor
        WORD  TKGW;                     // Llave token de APP en GW 
        
        /*-HEADER MB-*/        
        BYTE   Sec_GW;
        BYTE   Sec_PAN;                  //Para control de paquetes MB
        /*-DATA OF LAYER-*/
        /*-HEADER MIWI-*/
        BYTE   Tipo;                     // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: ASOCI ||T=5: control
        WORD   ID;
        WORD   TOKEN;                    //Autentica Transaccion del Mimias Frente a la aplicacion del Servidor                 
        /*-DATA Of APPLICATION-*/        
        BYTE   Secuencia;
        WORD   Slot;
        BYTE   Nro_intentos;
        BYTE   Pot;
        BYTE   Noise;
        BYTE   Signal;
        BYTE   Bat;
        
        struct    DATE_t_Def     DateTx;     // no reducir se usa para actualizar rtcc
        struct    MED_t_Def      MedidaRE;   //medida ultima
        struct    MED_DIF_t_Def  MedidaDif;
        WORD  ID_AUT;                   //Recibida para mimia autenticacion
        
        /*-DATA Of APPLICATION CTRL-*/ 
        WORD  TKMM;                     //Token de aplicacion (APP) Control Mimia   
        BYTE   SubTipo;                  //Valido para tramas de control para capa aplicacion           
        struct    DATCTRL_t_Def  DatCtrl;   //Datos provecientes de Tramas de control   
        /*-META DATA MB*********/
        BOOL   RW_op;                    // Read=true/input Write=fasle/output
        BYTE   Tipo_st;                  //Variable de estado anterior
    };    
    
struct APP_t_Cons_Def
    {   
        /*-HEADER-*/                
        WORD   ID;
        WORD   TOKEN;                    //Autentica Transaccion del Mimias Frente a la aplicacion del Servidor                 
        /*-DATA OF LAYER-*/
        WORD   Slot;
        BYTE   Nro_intentos;
        BYTE   Pot;
        BYTE   Noise;
        BYTE   Signal;
        BYTE   Bat;        
        
        struct DATECons_t_Def DateTx;       //tratar de reducir para ajustar el tama�o de buffer solo importa seg min
        struct MED_t_Def      MedidaRE;     //medida ultima
        struct MED_DIF_t_Def  MedidaDif;
        
    };//APP_t_Cons;
    
struct APP_t_Rdcd_Complet_Def
    {
        struct APP_t_Cons_Def Buffer[BFFR_RX_LNGTH];
        BYTE               point;
        BYTE               send_point;
     
    };//APP_t_Cons_Complet;
    
struct APP1_t_Def
    {
        WORD  ID_AUI; //MAC
        WORD  TK;         
        WORD  Slot;
        BYTE  Secuencia;
        
    };//APP1_t;
    
struct APP_t_Buffer_Def
    {         
        BOOL                Rece;           // indica recibido
        BOOL                Send;           // indica envio
        BYTE                Sec_GW;         // Secuencia con la que se envio al servidor
        //BYTE                Sec_PAN;        // Secuencia con la que se llego al GW desde PANCO
        WORD                ID_AUT;         // ID de autenticacion del servidor               
        struct APP1_t_Def   Element;        // Datos del MEDIDOR        
        BOOL                PermanentAdd;
        BYTE                Add[ADDRESS_LEN];   // Direccion del nodo para retransmitrir solo para PANCO
    };//Gen_Buffer_t;

struct APP_Ctrl_t_Buffer_Def
    {    
        WORD                 ID;
        WORD                 TKSRV;           // token para enviar al servidor 
        WORD                 TKMM;            // Token para ingresar a MIMIA  
        BYTE                 SubTipo;         // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
        BOOL                 Send;       
        struct DATCTRL_t_Def DatCtrl;    
    };
    
struct APP_Ctrl_GW_t_Def
    {    
        BYTE                 SubTipo;         // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
        BOOL                 Send;       
        struct DATCTRL_t_Def DatCtrl;    
    };
    
struct APP_Ctrl_t_Buffer
    {
        BYTE                          index_to_send;            //apunta al siguiente elemento a enviar
        BYTE                          index;                    //apunta al elemento libre para escribir
        struct APP_Ctrl_t_Buffer_Def  Ctrl_Base[BS_CTRL_LNGTH];  
    };
    
/***********************************************************
        Tablas de control de indice segun el PANCOOR
 **********************************************************/    

struct ID_AddMB_t_Def
    {
        BYTE Inx;
        BOOL Act;        
        BYTE ID; 
        BYTE Add_MB;
    };
    
struct Table_ID_AddMB_t_Def
    {    
        BYTE                   Pntr;                  //apunta al elemento libre para escribir
        struct ID_AddMB_t_Def  Table[TBL_ID_MB_LNGTH];    
    };
    
/**********************************************************/    
    
struct TK_Buffer_t_Def
    {
        BYTE             Inx;           
        BOOL             Send;               
        WORD             TKSRV;                   // token para enviar al servidor                                         
    };
    
struct APP_TK_t_Buffer_Def
    {
        BYTE                   index_to_send;         //apunta al siguiente elemento a enviar
        BYTE                   index;                 //apunta al elemento libre para escribir
        struct TK_Buffer_t_Def Tables[BS_TK_LNGTH];  
    };
    
struct APP_TK_t_Buffer
    {
        BYTE                       Inx;           
        BYTE                       ID;                //MASK 6 bit
        struct APP_TK_t_Buffer_Def Base;
    };
    
/*****DEFINO NOMBRES PARA LAS ESTRUCTURAS DE LOS TIPOS NUEVAS******************
  *****LUEGO PUEDO INSTANCIAR MAS FACILMENTE SIN ANTEPONER TYPE ***************/
    
typedef struct APP_t_Rdcd_Complet_Def APP_t_Rdcd_Complet;
typedef struct APP_t_Buffer_Def       Gen_Buffer_t;
typedef struct APP_Ctrl_t_Buffer      Ctrl_Buffer_t;
typedef struct APP_Ctrl_GW_t_Def      APP_Ctrl_GW_t;
typedef struct APP_TK_t_Buffer        APP_TK_t;
typedef struct Table_ID_AddMB_t_Def   Table_ID_AddMB_t;
typedef struct APP_t_Def              APP_t; 
typedef struct APP_tcp_t_Def          APP_tcp_t;
typedef struct DATE_t_Def             DATE_t;
typedef struct PASS_t_Def             PASS_t;    
typedef enum   TIPO_t_Def             TIPO_t;

//APP_t BufferTX[1];
    
extern APP_tcp_t     App;                    //variables de comando o de intercambio TCPIP
extern APP_t         App1;                   //variables de comando o de intercambio MIWI
extern APP_Ctrl_GW_t App_Ctrl;               //Variables de control GW
//extern uint8_t  AddMIWI[ADDRESS_LEN];     //intercambio     

//----Buffer y bases------------------------------

extern APP_t_Rdcd_Complet Buffer_Nrm;                     //buffer Recepcion MIWI, por lo gral tramas Norm tipo 1

extern WORD               Base_Slot[BASE_SLT_LNGTH][2]; //[ID][NEW-Slot] buffer Recepcion MIWI para cambioos de slot

extern Gen_Buffer_t       Buffer_Aso[BUFFER_LNGTH];     //buffer asociacion recepcion-transmision tcp-ip 

extern Gen_Buffer_t       Buffer_PN[N_PAN];             //buffer asociacion PAN 

extern Ctrl_Buffer_t      Ctrl_Buffer_Q[N_PAN];         //buffer Recepcion QUERY tramas TIPO Control gral

extern Ctrl_Buffer_t      Ctrl_Buffer_R[N_PAN];         //buffer Transmision RESPONStramas TIPO Control gral

extern APP_TK_t           TK_Buffer_Nrm[N_PAN];         //beffer token tramas normales

extern Table_ID_AddMB_t   Table_ID_AddMB;               //Tabla de ID vs ADDmodbus, dispositivos escalvos
//----------------------------------

extern  char   Pass   [LENGTH_PASS+1];                    //pass de GW
extern  char   Pass_AU[LENGTH_PASS+1];                 //Pass recibida
extern  PASS_t PASS;

//----semaforos------------------------------

extern BOOL Buff_NRM_MW;                     //bandera buffer rx en capacidad de transmision supera limite para envios modo Normal
extern BOOL Buff_RTCC_MW;
extern BOOL Buff_CTRL_MW;
extern BOOL Buff_CTRL_TCP;
extern BOOL Buff_ASO_PN_MB;
extern BOOL Buff_ASO_PN_TCP;

extern BOOL Sem_TCP_DONE;                   //bandera de APP rx en capacidad de transmision 1 indica libre- 0 ocupado
extern BOOL Sem_MB_DONE;                    //bandera de APP rx en capacidad de transmision 1 indica libre- 0 ocupado
extern BOOL Sem_MIWI_DONE;                  //bandera que indica el estado de tramiento del paquete recibido  1 indica libre(resultado)- 0 ocupado(tramiento))
extern BOOL Sem_ModBus_Free;                //bandera de Modbus libre
extern BOOL Sem_Free_App;                   //bandera de APP regis
extern BOOL Sem_Free_App1;                  //bandera de APP1 regis
extern BOOL Sem_Tx_CTRL_MB;   
extern BOOL Sem_Resp_CTRL_MB;

extern WORD   Id_GW;                      //Id de GW
extern WORD   Id_GW_AU;                   //Id Recibida
extern BYTE   GW_Sec_TCP;                 //Secuencia de transmicion actual 
extern BYTE   GW_Sec_MB;
extern BYTE   GW_Sec_MW;                  //Secuencia de transmicion MB 
extern DATE_t GW_RTCC;                    //variable de intercambio de RTCC 



