 /*********************************************************************
 *
 *	Hardware specific definitions for:
 *    - PICDEM.net 2
 *    - PIC18F97J60
 *    - Internal 10BaseT Ethernet
 *
 *********************************************************************
 * FileName:        HardwareProfile.h
 * Dependencies:    Compiler.h
 * Processor:       PIC18
 * Compiler:        Microchip C18 v3.36 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder		09/16/2010	Regenerated for specific boards
 ********************************************************************/
#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include "Compiler.h"

// Define a macro describing this hardware set up (used in other files)
#define PICDEMNET2

// Set configuration fuses (but only in MainDemo.c where THIS_IS_STACK_APPLICATION is defined)
#if defined(THIS_IS_STACK_APPLICATION)
	#pragma config WDT=OFF, FOSC2=ON, FOSC=HSPLL, ETHLED=ON

	// Automatically set Extended Instruction Set fuse based on compiler setting
	#if defined(__EXTENDED18__)
		#pragma config XINST=ON
	#else
		#pragma config XINST=OFF
	#endif
#endif

// Clock frequency values
// These directly influence timed events using the Tick module.  They also are used for UART and SPI baud rate generation.
#define GetSystemClock()		(41666667ul)			// Hz
#define GetInstructionClock()	(GetSystemClock()/4)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Might need changing if using Doze modes.
#define GetPeripheralClock()	(GetSystemClock()/4)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Divisor may be different if using a PIC32 since it's configurable.

// Hardware I/O pin mappings

// LEDs
#define LED0_TRIS			(TRISBbits.TRISB4)	// Ref 
#define LED0_IO				(LATBbits.LATB4)

// LEDS de connector RJ45
#define LED1_TRIS			(TRISAbits.TRISA0)	// Ref D7
#define LED1_IO				(LATAbits.LATA0)
#define LED2_TRIS			(TRISAbits.TRISA1)	// Ref D6
#define LED2_IO				(LATAbits.LATA1)

#define LEDA_TRIS			(TRISAbits.TRISA0)	// Ref D7
#define LEDA_IO				(LATAbits.LATA0)
#define LEDB_TRIS			(TRISAbits.TRISA1)	// Ref D6
#define LEDB_IO				(LATAbits.LATA1)

#define LED_GET()			(LED0_IO)
#define LED_PUT(a)			(LED0_IO = (a))

// Momentary push buttons

#define BUTTON0_TRIS		(TRISBbits.TRISB0)
#define	BUTTON0_IO			(PORTBbits.RB0)     // Ref T1157

// Ethernet TPIN+/- polarity swap circuitry (PICDEM.net 2 Rev 6)
//#define ETH_RX_POLARITY_SWAP_TRIS	(TRISGbits.TRISG0)
//#define ETH_RX_POLARITY_SWAP_IO		(LATGbits.LATG0)

// 25LC256 I/O pins  solo por UEXT o EXT pins
//#define EEPROM_CS_TRIS		(TRISFbits.TRISF7)
#define EEPROM_CS_IO		(LATFbits.LATF7)
#define EEPROM_SCK_TRIS		(TRISCbits.TRISC3)
#define EEPROM_SDI_TRIS		(TRISCbits.TRISC4)
#define EEPROM_SDO_TRIS		(TRISCbits.TRISC5)
#define EEPROM_SPI_IF		(PIR1bits.SSP1IF)
#define EEPROM_SSPBUF		(SSP1BUF)
#define EEPROM_SPICON1		(SSP1CON1)
#define EEPROM_SPICON1bits	(SSP1CON1bits)
#define EEPROM_SPICON2		(SSP1CON2)
#define EEPROM_SPISTAT		(SSP1STAT)
#define EEPROM_SPISTATbits	(SSP1STATbits)

// Serial Flash AT45DB011 /SRAM 
#define SPIFLASH_CS_TRIS		(TRISFbits.TRISF7)
#define SPIFLASH_CS_IO			(LATFbits.LATF7)

#define SPIFLASH_SCK_TRIS		(TRISCbits.TRISC3)
#define SPIFLASH_SDI_TRIS		(TRISCbits.TRISC4)
#define SPIFLASH_SDI_IO			(PORTCbits.RC4)
#define SPIFLASH_SDO_TRIS		(TRISCbits.TRISC5)
#define SPIFLASH_SPI_IF			(PIR1bits.SSPIF)

#define SPIFLASH_SSPBUF			(SSP1BUF)
#define SPIFLASH_SPICON1		(SSP1CON1)
#define SPIFLASH_SPICON1bits	(SSP1CON1bits)
#define SPIFLASH_SPICON2		(SSP1CON2)
#define SPIFLASH_SPISTAT		(SSP1STAT)
#define SPIFLASH_SPISTATbits	(SSP1STATbits)

// MRF PI and I/O pins 

// SPI-2 I2C-2

// UEXT and EXT I/O pins 
#define UEXT_CS_TRIS           (TRISEbits.TRISE2)
#define UEXT_CS_IO             (LATEbits.LATE2)
#define UEXT_CS_O              (PORTEbits.RE2)

#define EXT1_TRIS              (TRISAbits.TRISA2)
#define EXT1_IO                (LATAbits.LATA2)

#define EXT2_TRIS              (TRISAbits.TRISA3)
#define EXT2_IO                (LATAbits.LATA3)

#define EXT3_TRIS              (TRISAbits.TRISA4)
#define EXT3_IO                (LATAbits.LATA4)
#define EXT4_TRIS              (LATAbits.LATA5)
#define EXT4_IO                (LATAbits.LATA5)

#define EXT5_TRIS              (TRISEbits.TRISE0)
#define EXT5_IO                (LATEbits.LATE0)
#define EXT6_TRIS              (TRISEbits.TRISE1)
#define EXT6_IO                (LATEbits.LATE1)
#define EXT7_TRIS              (TRISEbits.TRISE2)
#define EXT7_IO                (LATEbits.LATE2)

#define EXT8_TRIS              (TRISCbits.TRISC2)
#define EXT8_IO                (LATCbits.LATC2)
#define EXT9_TRIS              (TRISDbits.TRISD0)
#define EXT9_IO                (LATDbits.LATD0)

#define EXT10_TRIS              (TRISDbits.TRISD1)
#define EXT10_IO                (LATDbits.LATD1)
#define EXT11_TRIS              (TRISDbits.TRISD2)
#define EXT11_IO                (LATDbits.LATD2)

#define EXT12_TRIS              (TRISBbits.TRISB1)
#define EXT12_IO                (LATBbits.LATB1)
#define EXT13_TRIS              (TRISBbits.TRISB2)
#define EXT13_IO                (LATBbits.LATB2)
#define EXT14_TRIS              (TRISBbits.TRISB3)
#define EXT14_IO                (LATBbits.LATB3)

#define EXT15_TRIS              (TRISBbits.TRISB5)
#define EXT15_IO                (LATBbits.LATB5)

#define EXT21_TRIS              (TRISEbits.TRISE3)
#define EXT21_IO                (LATEbits.LATE3)
#define EXT22_TRIS              (TRISEbits.TRISE4))
#define EXT22_IO                (LATEbits.LATE4)
#define EXT23_TRIS              (TRISEbits.TRISE5)
#define EXT23_IO                (LATEbits.LATE5)
#define EXT24_TRIS              (TRISFbits.TRISF1)
#define EXT24_IO                (LATFbits.LATF1)

#define EXT25_TRIS              (TRISFbits.TRISF2)
#define EXT25_IO                (LATFbits.LATF2)
#define EXT26_TRIS              (TRISFbits.TRISF5)
#define EXT26_IO                (LATFbits.LATF5)
#define EXT27_TRIS              (TRISFbits.TRISF6)
#define EXT27_IO                (LATFbits.LATF6)
#define EXT28_TRIS              (TRISGbits.TRISG4)
#define EXT28_IO                (LATGbits.LATG4)


// ANALOG I/O pins 
#define AN_TRIM_CS_TRIS            (TRISFbits.TRISF3)
#define AN_TRIM_CS_IO              (PORTFbits.RF3)
#define AN_TEMP_CS_TRIS            (TRISFbits.TRISF4)
#define AN_TEMP_CS_IO              (PORTFbits.RF4)

// LCD I/O pins
#define LCD_DATA_TRIS		(EXT25_TRIS)
#define LCD_DATA_IO			(EXT25_IO)
#define LCD_RD_WR_TRIS		(EXT26_TRIS)
#define LCD_RD_WR_IO		(EXT26_IO)
#define LCD_RS_TRIS			(EXT27_TRIS)
#define LCD_RS_IO			(EXT27_IO)
#define LCD_E_TRIS			(EXT28_TRIS)
#define LCD_E_IO			(EXT28_IO)

/*
// LCD Pin Definitions
#define LCD_CS_TRIS         TRISDbits.TRISD7
#define LCD_CS              LATDbits.LATD7
#define LCD_RS_TRIS         TRISDbits.TRISD3
#define LCD_RS              LATDbits.LATD3
#define LCD_RESET_TRIS      TRISEbits.TRISE0
#define LCD_RESET           LATEbits.LATE0
#define LCD_BKLT_TRIS       TRISEbits.TRISE1
#define LCD_BKLT            LATEbits.LATE1
*/

// UART I/O pins
#define UART_TX1_TRIS		(TRISCbits.TRISC6)
#define UART_TX1    		(LATCbits.LATC6)
#define UART_RX1_TRIS		(TRISCbits.TRISC7)
#define UART_RX1        	(PORTCbits.RC7)

#define UART_nRE_TRIS		EXT1_TRIS
#define UART_nRE        	EXT1_IO
#define UART_DE_TRIS		EXT2_TRIS
#define UART_DE            	EXT2_IO

// UART mapping functions for consistent API names across 8-bit and 16 or 
// 32 bit compilers.  For simplicity, everything will use "UART" instead 
// of USART/EUSART/etc.
#define BusyUART()			BusyUSART()
#define CloseUART()			CloseUSART()
#define ConfigIntUART(a)	ConfigIntUSART(a)
#define DataRdyUART()		DataRdyUSART()
#define OpenUART(a,b,c)		OpenUSART(a,b,c)
#define ReadUART()			ReadUSART()
#define WriteUART(a)		WriteUSART(a)
#define getsUART(a,b,c)		getsUSART(b,a)
#define putsUART(a)			putsUSART(a)
#define getcUART()			ReadUSART()
#define putcUART(a)			WriteUSART(a)
#define putrsUART(a)		putrsUSART((far rom char*)a)

#endif // #ifndef HARDWARE_PROFILE_H
