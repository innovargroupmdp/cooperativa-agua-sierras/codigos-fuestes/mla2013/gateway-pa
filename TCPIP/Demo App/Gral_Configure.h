/**
  @Generated PIC10 / PIC12 / PIC16 / PIC18 MCUs Source File

  @Company:
    Microchip Technology Inc.

  @File Name:
    Gral_Configure.h

  @Summary:
    This is main configuration function for all Apps, stacks and pheriferical

  @Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC18F26K20
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.45 or later
        MPLAB             :  MPLAB X 4.15
*/


#include "mcc_c8/pin_manager.h"
//#include   "HardwareProfile.h" se encuentra dentro pin_Manager
#include "mcc_c8/interrupt_manager.h"
//#include "mcc_c8/spi_driver.h"
//#include "mcc_c8/tmr1.h"
//#include "mcc_c8/tmr2.h"
//#include "mcc_c8/tmr0.h"
//#include "mcc_c8/eusart1.h"



//#include "TCPIPLibrary/network.h"

//#define _XTAL_FREQ  GetSystemClock()//16000000


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the device to the default states configured in the
 *                  MCC GUI
 * @Example
    SYSTEM_Initialize(void);
 */
void SYSTEM_Initialize(void);


void InitializeBoard(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the oscillator to the default states configured in the
 *                  
 * @Example
    SYSTEM_Initialize(void);
 */
//@TODO configurar correctamente
void OSCILLATOR_Initialize(void);
