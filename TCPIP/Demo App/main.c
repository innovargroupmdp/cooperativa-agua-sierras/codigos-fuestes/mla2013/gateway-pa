/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/


#include <xc.h>            /* XC8 General Include File */
#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "mcc_generated_files/mcc.h"
//#include "TCP-IP_APPs.h" /* User funct/params, such as InitApp */

#include "App.h"
#include "TCPClient.h"
#include "APLICACION.h"
#include "modbus.h"
#include "APP_MIWI_ModBus.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */
 APP_tcp_t     App;                     //variables de comando o de intercambio TCPIP
 APP_t         App1;                    //variables de comando o de intercambio ModBus
 APP_Ctrl_GW_t App_Ctrl;                //Variables de control GW
//uint8_t  AddMIWI[ADDRESS_LEN];    //intercambio     
 
//----Buffer y bases------------------------------
 
 APP_t_Cons_Complet BufferRX;             //buffer Recepcion MIWI

 uint16_t      Base_Slot[BASE_SLT_LNGTH][2];  //buffer Recepcion MIWI para cambioos de slot

 Gen_Buffer_t  Buffer_Aso[BUFFER_LNGTH];  //buffer recepcion-transmision tcp-ip
 
 Gen_Buffer_t  Buffer_PN[N_PAN];          //buffer ASO Pam
 
 Ctrl_Buffer_t Ctrl_Buffer_Q[N_PAN];      //buffer Recepcion QUERY tramas TIPO Control gral enviada por el SrV

 Ctrl_Buffer_t Ctrl_Buffer_R[N_PAN];      //buffer Transmision RESPONStramas TIPO Control gral enviada desde MIMIA
 
 APP_TK_t      TK_Buffer_Nrm[N_PAN];      //buffer TK de respuestas paquetes NORM desde SRV
 
 Table_ID_AddMB_t   Table_ID_AddMB;       //Tabla de ID vs ADDmodbus, dispositivos escalvos
//------------Claves, ID, Tokens------------------
 
 char   Pass[LENGTH_PASS+1];       //pass de GW    :token para acceder al servidor antigua
 char   Pass_AU[LENGTH_PASS+1];    //Pass recibida :token para acceder al servidor proxima 
 PASS_t PASS;
 const char MAC_AUI[]="00000001";     // 64 bits de MAC
 
 //----semaforos------------------------------
 
 bool Sem_ModBus_Free=false;    //bandera de Modbus libre 
 bool Buff_NRM_MB=    false;
 bool Buff_RTCC_MB=   false;
 bool Buff_CTRL_MB=   false;
 bool Buff_CTRL_TCP=  false;
 bool Buff_ASO_PN_MB= false;
 bool Buff_ASO_PN_TCP= false;
 /* bandera de trabajo listo sobre una trama TCP Cuando llega una trama se 
  * atiende o coloca en buffer luego se puede volver a ingresar  */
 bool Sem_TCP_DONE= false;        //bandera de trabajo listo sobre una trama TCP
 bool Sem_MB_DONE=  false;        //
 
 bool Sem_Tx_CTRL_MB   = false;
 bool Sem_Resp_CTRL_MB = false;
         
 bool Sem_Free_App= true;
 bool Sem_Free_App1=true;

 uint16_t Id_GW;                //Id de GW asiganda unica vez durante la autenticacion
 uint16_t Id_GW_AU;             //Id direccion recibida en cada trama
 uint8_t  GW_Sec_TCP;           //Secuencia de transmicion actual TCP 
 uint8_t  GW_Sec_MB;            //Secuencia de transmicion actual MB
 DATE_t   GW_RTCC;              //variable de intercambio de RTCC 

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    // initialize the device
    SYSTEM_Initialize();
    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    TCP_Client_Initialize();   
    
    modbus_init();
    
    //InitApp();
   
    /* TODO <INSERT USER APPLICATION CODE HERE> */

    while(1)
    {   
    /******************************************************************************/
    /* Llama a administrar el socket Ethernet en busca de nuevos paquetes         */
    /******************************************************************************/
        Network_Manage();
    /******************************************************************************/
    /* Llama a administrar la apliaciones                                         */
    /******************************************************************************/        
        Serv_APP();        
    /******************************************************************************/
    /* Llama a administrar la conexion MODBUS en busca de nuevos paquetes arma el
     *  buffer                                                                    */
    /******************************************************************************/ 
        if (Sem_ModBus_Free) ModBus_Manage();
        
    }

}

