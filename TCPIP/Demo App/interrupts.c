/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#include <p18f67j60.h>    /* C18 General Include File */



/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* High-priority service */


    
/*    
#pragma interruptlow LowISR
void LowISR(void)
    {
        TickUpdate();
    }

#pragma interrupt HighISR
    void HighISR(void)
    {

    }
#pragma code lowVector=0x18
    void LowVector(void){_asm goto LowISR _endasm}

#pragma code highVector=0x8
    void HighVector(void){_asm goto HighISR _endasm}
#pragma code // Return to default code section
 
*/