/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

//#include <stdint.h>         /* For uint8_t definition */

#include <stdlib.h>        /* For true/false definition */
#include <stddef.h>
#include <string.h>
#include <timers.h>

#include "TCPIP Stack/TCPIP.h"
#include "TCP-IP_APPs.h"
#include "App.h"
//#include "APP_MIWI_ModBus.h"
//#include "mcc_generated_files/tmr0.h"
#include "APL_Fun_TCP.h"
/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

//The function reverse used above is implemented two pages earlier:

/* Genero Pass_GW                      */
void Gen_Pass(char*p){
    BYTE i;
    //@TODO verificar el uso de la biblioteca timer
    srand((unsigned)ReadTimer0());
    //@TODO comprobar uso de TMR0
    for (i=0;i<LENGTH_PASS;i++)        
        *(p++)=(char)rand();
    
    *p = '\0';
    return;
}

/* reverse:  reverse string s in place */
 void reverse(char *s){
     int i, j;
     char c;
 
     for (i = 0,j = strlen(s)-1; i<j; i++, j--) 
     {
         c = *(s+i);
         *(s+i) = *(s+j);
         *(s+j) = c;
     }
     return;
 }


 
void PutFecha(BYTE *p, BYTE f) {

    BYTE i = 0;
    
    char s[2] ={0,'\0'};
    char str[10]="\0"; 
    switch (f) {
        case 1: 
            for (i = 0; i < 3; i++) {

                s[0]=(char)(*p & 0xf0);
                s[0]=(char)(s[0] >> 4);
                s[0]= (char)(s[0]+48);            //paso de bcd a ascii

                strcat(str, (const char*)s);

                s[0] = (char)(*p & 0x0f);
                s[0]= (char)(s[0]+48);
                strcat(str, (const char*)s);                   
                
                if (i != 2){ 
                    s[0] = ':';
                    strcat(str, (const char*)s);
                }    
                //} else
                //    s[0] = '-';                                
               //strcat(str, (const char*)s);
                p++;
            }
        break;    
        case 2:
            for (i = 0; i < 3; i++) {

                s[0]=(char)(*p & 0xf0);
                s[0]=(char)(s[0] >> 4);
                s[0]= (char)(s[0]+48);            //paso de bcd a ascii

                strcat(str, (const char*)s);

                s[0] = (char)(*p & 0x0f);
                s[0]= (char)(s[0]+48);
                strcat(str, (const char*)s);
                
                if (i != 2){
                    
                    if (i == 0)
                        s[0] = '-'; 
                    else 
                        s[0] = '/';
                    
                    strcat(str, (const char*)s);
                }
                
                p++;
            }
        break;    
    }
    //strcat(txdataPort60, (const char*)str);
    //utilizar la api tcp
    return;
}

/*****************************************************************************
  Function:
    void PutJSON(TCP_SOCKET)

  Summary:
    Implements JSON (over TCP).

  Description:
	

  Precondition:
	

  Parameters:
    None

  Returns:
    None
 ***************************************************************************/
void PutJSON( BYTE TCPSOC ) {
    /*  //ESTO NO VA SOLO REFERENCIA
        uint16_t  {Id_GW } // identificador dispositivo asignado en la asociacion
                  {Pass:}
                  {Pass:}  
     *  uint8_t   Secuencia;
     *            Tk_Srv
     *              
        uint8_t   {Tipo:}; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Aso_GW ||T=5: Control
        
        uint8_t   Nro_intentos;
        uint8_t   Noise;
        uint8_t   Signal;
        uint8_t   Bat;
        uint16_t  Slot;
        DATE_t    DateTx;     
        MED_t     Medida[24];
        uint8_t   SlotDif;
     */
    #define Toma_1  (TOMAS -1)

    char s[21];
    BYTE i,j;
    BYTE *p;
    WORD sum= 0;    
    
    /* Coloco la longitud del paquete JASON en la cabecera MIME*/
    switch (App.Tipo) // Send the packet: T=1: Normal ||T=2: Asociacion || T=3: Time ||T=5: AsocGW ||T=4: Control
    {   
        case nOP:
                itoa(Lngth_Json_nOP, s);
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);                
        break;
        case OP_NORMAL:
                j= 0;                
                while ( j<Buffer_Nrm.point ) 
                {
                 sum = Buffer_Nrm.Buffer[j++].MedidaDif.Index;  
                }
                itoa( (Lngth_Json_Norm + j + sum),s );
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);                
        break;
        case OP_ASOCIACION:
                itoa( Lngth_Json_Aso, s );
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);
        break;
        case OP_ASOCIACION_PAN:
                itoa( Lngth_Json_Aso_PN, s);
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);
        break;
        case OP_ASOCIACION_GW:
                itoa( Lngth_Json_Aso_GW, s);
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);
        break;
        case OP_CONTROL:
            
                itoa( Lngth_Json_Ctrl + App.DatCtrl.Index , s);
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);
        break;
        case OP_TIME:
                itoa( Lngth_Json_Time, s);
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*) s);
        break;
    }  
    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"\r\n\r\n{\"Id_GW\":");        //6  ID GateWay en asociacion envia MAC
    //utoa((char *) s, (unsigned) Id_GW, 5);           //5
    //utoa( s ,PASS.Id_GW.Val);                        //   ID GateWay en asociacion envia MAC 
    itoa(App.ID_GW, s);
    TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
    
    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Pass_SRV\":\"");          //9  // acceso al servidor   
    //itoa(App.Pass_SRV, s);                  //10
    TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)App.Pass_SRV);
    
    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Pass_GW\":\"");           //9  // acceso al servidor   
    //itoa( App.Pass_GW, s);                  //10 // permite acceso al GW ante la respuesta
    TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)App.Pass_GW);
    
    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"\",\"Secuencia\":");          //10    
    itoa((WORD) App.Sec_GW, s);
    TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
    
    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tk_Srv\":");               //10   
    itoa( App.TK_SrvGw , s );                  //Token es un numero
    TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
     /* 
      *                         Objeto JSON PRINCIPAL
      * {
      *     "Id_GW"      :0001,
      *     "Pass_SRV"      :"12345abcde",                   //acceso al servidor
      *     "Pass_GW"       :"12345abcde",                   //acceso al GW futura
      *     "Secuencia"  :003,                          // SECUENCIA DEL GW 
      *     "Tk_Srv"     :0100,                         // LLAVE DE APLICACION GW-Serv
      *
      *                ARMO A CONTINUCACION SEGUN EL TIPO DE SERVICIO        
      */
    switch (App.Tipo) // Send the packet: T=1: Normal ||T=2: Asociacion || T=3: Time ||T=5: AsocGW ||T=4: Control
    {                    
        case nOP://--------------------nOP
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":0}"); 
          break;
        case OP_NORMAL://--------------------Normal
        /*  "Tipo":1,
         *  "Datos":{
                     "Length":2,
                     "Medidores":[
                          
                            {   "Id_M":0001,"Token":0007,"Slot":0000,"Nro_intentos":000,
                                "Pot":00,"Noise":00,"Signal":00,"Bat":00,"DateTx":"ss:mm:hh",
                                "MedidaRE":{"Mesure":00000,"DateMesu":"hh:dd:mm"},
                                "MedidaDif":{"Index":03,"MedidaD":[00,00,00]} },
                            {   "Id_M":0002,"Token":0005,"Slot":0000,"Nro_intentos":000,
                                "Pot":00,"Noise":00,"Signal":00,"Bat":00,"DateTx":"ss:mm:hh",
                                "MedidaRE":{"Mesure":00000,"DateMesu":"hh:dd:mm"},
                                "MedidaDif":{"Index":03,"MedidaD":[00,00,00]} }
                                ]
                    } # llave de Datos        
        *  }   # llave de elemento json HTTP         
        */
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":1,\"Datos\":{\"Length\":");                    
            itoa((WORD) Buffer_Nrm.point, s);
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Medidores\":[");                        //8 
            
            j= 0;             
            while ( j<Buffer_Nrm.point ) 
            { //BufferRX.point apunta a uno vacio o libre
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"{\"Id_M\":"); //6 
                
                itoa( Buffer_Nrm.Buffer[j].ID, s);                    //5
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Token\":");                         //10
                
                itoa( Buffer_Nrm.Buffer[j].TOKEN, s);                 //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Slot\":");                          //10
                
                itoa( Buffer_Nrm.Buffer[j].Slot, s);                  //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Nro_intentos\":");                  //10
                
                itoa( (WORD) Buffer_Nrm.Buffer[j].Nro_intentos, s);          //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Pot\":");                           //10
                
                itoa( (WORD) Buffer_Nrm.Buffer[j].Pot, s);                   //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Noise\":");                         //10
                
                itoa( (WORD) Buffer_Nrm.Buffer[j].Noise, s);                 //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);

                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Signal\":");                        //10
                
                itoa( (WORD) Buffer_Nrm.Buffer[j].Signal, s);                //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);

                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Bat\":");                           //10
                
                itoa( (WORD) (Buffer_Nrm.Buffer[j].Bat>> 4), s);                   //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",");
                itoa( (WORD) (Buffer_Nrm.Buffer[j].Bat & 0x0F), s);
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);                               
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"DateTx\":\"");                      //10
                p = &Buffer_Nrm.Buffer[j].DateTx.Seg;
                PutFecha(p,1);                              // solo seg min hora
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"\",\"MedidaRE\":{\"Mesure\":");        //10 MedidaRE
                  
                itoa( Buffer_Nrm.Buffer[j].MedidaRE.Mesure, s);       //2  
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"DateMesu\":\"");                    //10                
                p = &(Buffer_Nrm.Buffer[j].MedidaRE.DateMesu.Hou);
                PutFecha(p,2);                              // solo hora dia mes
                    
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"\"},\"MedidaDif\":{\"Index\":");       //10
                itoa( (WORD) Buffer_Nrm.Buffer[j].MedidaDif.Index, s);       //2  
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
                
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"MedidaD\":[");                      //10                                   
                                
                for (i = 0; i < Buffer_Nrm.Buffer[j].MedidaDif.Index; i++) 
                {                    
                    itoa( (WORD) Buffer_Nrm.Buffer[j].MedidaDif.MedidaD[i], s);//2
                    TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);                                        
                    //sigue otro elemento?
                    if (i != (Buffer_Nrm.Buffer[j].MedidaDif.Index -1)) 
                        TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",");
                    //else 
                    //    strcat(txdataPort60,"\"}]}");
                }
                //cierro vector:MedidaD, objeto MedidasDif y elemento de vector Medidores
                TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"]}}");
                
                Buffer_Nrm.send_point=j;                
                
                if ((j+1)<Buffer_Nrm.point)
                    //continuo agregando elementos al vector de Medidores
                    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",");
                else 
                    //Cierro el Vector Medidores, objeto Datos y cierro objeto json de HTTP
                    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"]}}");
                
                j++;
            }
          break;
        
        case OP_ASOCIACION://-------------------(Asociacion)
      
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":2,\"Id_M\":");   //6  Aqui es la MAC del dispositivo a asociar            
            
            itoa( App.ID, s);                       //5  //es la MAC
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);                                         
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"}");                       //1 # llave de elemento json HTTP 
            
          break;
            
        case OP_TIME://-------------------------(TIME)

            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":3}");            //8
                                               
          break;

        case OP_ASOCIACION_PAN:// --------------(ASOCIACION PAN)

            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":4,\"Id_PAN\":"); //6  Aqui es la MAC del dispositivo a asociar            
            itoa( App.ID, s);                       //5  //es la MAC
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);                                         
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"}");                       //1 # llave de elemento json HTTP 8 

          break;
            
        case OP_CONTROL:// ---------------------(CONTROL)
            
            /*  "Tipo":5,
             *  "Id_Dvc":0000",
             *  "Token":0000,       //llave mimia->APPserv
             *  "TokenMM":0000,     //llave APPserv->mimia
             *  "Subtipo":5,                         
             *  "DatCtrl":{
                           "Index": 3,                            
                           "Data":[ "dato1", "dato2", "dato3" ]
                          }
             *  } # llave de elemento json HTTP             
             */                        
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":5");                         //8 
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Id_Dvc\":"); //6 
            //utoa((char *) s, (unsigned) BufferRX.Buffer[j].ID, 5);    //5
            itoa( App.ID, s);                                   //5
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Token\":");                         //10
            //utoa((char *) s, BufferRX.Buffer[j].Slot, 2);             //2
            itoa( App.TOKEN, s);                                //2
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"TokenMM\":");                       //10
            //utoa((char *) s, BufferRX.Buffer[j].Slot, 2);             //2
            itoa( App.TKMM, s);                                 //2
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Subtipo\":");                       //10
            itoa( (WORD) App.SubTipo, s);                              //2
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
            
            /*
             * ",DatCtrl":{
                            "Index": 3,                            
                            "Data":[ 'dato1', 'dato2', 'dato3' ] //elementos char= 1 BYTE
                         } 
             */
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Datctrl\":{\"Index\":");                //2       
            itoa( (WORD) App.DatCtrl.Index, s);                                 
            TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);
            
            TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Data\":[");
            
            for (i = 0; i < App.DatCtrl.Index; i++)
            {                                        
                itoa( (WORD) App.DatCtrl.Data[i], s);      //2
                TCPPutString((TCP_SOCKET)TCPSOC,(BYTE*)s);

                if (i != (App.DatCtrl.Index -1)) 
                    //continuo agregando elementos al vector Data
                    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",");
                else 
                    //Cierro el Vector Data, cierro objeto Datactrl y json de HTTP
                    TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)"]}}");                    
            }                                  
          break;
        case OP_ASOCIACION_GW:// -------------(ASOCIACION GATEWAY)

          TCPPutROMString((TCP_SOCKET)TCPSOC, (ROM BYTE*)",\"Tipo\":6}");                        //8 

          break;
    }
    
    return;
}
/*****************************************************************************
  Function:
    Get_Obj_Json_TCP_ChartoInt(TCP_SOCKET TCPS,str,len)

  Summary:
    Extrae elementos Json a int.
 * 
 * OTROS:
 * Get_Obj_Json_TCP_Array(); entrega Array
   Get_Obj_Json_TCP_Int();  
   Get_Obj_Json_TCP_Char(TCPSOC,(ROM BYTE*) "\"Tipo\"",len);
 * Get_Obj_Json_TCP_ChartoInt(TCP_SOCKET TCPS,str,len);
*****************************************************************************/
WORD Get_Obj_Json_TCP_ChartoInt(BYTE TCPS , ROM BYTE* str){    
    
    char D[21];  // Mayor longitud de campo              
    WORD a,l,len;
    BYTE i;
    char *b;
    
    l= strlen((const char*)str);
    len = TCPIsGetReady((TCP_SOCKET)TCPS);                                      // faltantes por leer 
    
    a = TCPFindROMArrayEx((TCP_SOCKET)TCPS,(ROM BYTE*)"}",l,0,(WORD)len, FALSE); // consulto fin de algun objeto Json
     
    if (a!= 0xffff){
        
        a = TCPFindROMArrayEx((TCP_SOCKET)TCPS,(ROM BYTE*)str,l,0,(WORD)len, FALSE);
        if (a!=0xffff)  // ubicacion de la cadena y que entre completo el objeto Json   
        {
            if((a+40)<=len)
            {                
                a= a+l+2; //cero + (l-1)+(:_)
                //length("Tipo":")+(datos)   {"variable":"cadena",}
                 while (a >= 20) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*) D, 20);
                    a -= 20;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D);
                    #endif 
                }
                while (a >= 10) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*) D, 10);
                    a -= 10;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D);
                    #endif
                }
                while (a> 0) {
                    TCPGet((TCP_SOCKET)TCPS,(BYTE*) D);
                    a--;               //estoy ubicado sobre el primer caracter del dato
                    #if defined(STACK_USE_UART)
                    putcUART(D[0]);
                    #endif
                }                
                //length("Tipo":")+(datos)   {"variable":"cadena",}
                
                i= 0;
                TCPGet((TCP_SOCKET)TCPS,(BYTE*)D);
                while((D[i]!='\"') && (i<21))
                {
                    i++;
                    TCPGet((TCP_SOCKET)TCPS,(BYTE*)(D+i));                        
                }
                //TCPGetArray(TCPS,D,20); //variable ubicacion 7
                D[i] = '\0';
                
                #if defined(STACK_USE_UART)
                putsUART((char*)D);
                #endif

                return( (WORD)atoi((char *)D) );     //Salida Valida
                
            }else {           //hago que evolucione la fifo
                
                while (a >= 20) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 20);
                    a -= 20;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D);
                    #endif
                }
                while (a >= 10) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 10);
                    a -= 10;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D);
                    #endif
                }
                while (a> 0) {
                    TCPGet((TCP_SOCKET)TCPS,(BYTE*) D);
                    a--;     //estoy ubicado sobre el primer " para que luego al iterar lo encuentr FIND
                    #if defined(STACK_USE_UART)
                    putcUART(D[0]);
                    #endif
                }
            } 
        }else {
                            //hago que evolucione la fifo
            a = TCPFindROMArrayEx((TCP_SOCKET)TCPS,(ROM BYTE*)"{\"",l,0,(WORD)len, FALSE); // consulto inicio de algun objeto Json
            
            while (a >= 20) { // libero FIFO de a 20 elementos y termino en 40
                TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 20);
                a -= 20;
                #if defined(STACK_USE_UART)
                putsUART((char*)D);
                #endif
            }
            while (a >= 10) {
                TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 10);
                a -= 10;
                #if defined(STACK_USE_UART)
                putsUART((char*)D);
                #endif
            }
            while (a> 0) {
                TCPGet((TCP_SOCKET)TCPS,(BYTE*) D);
                a--;     //estoy ubicado sobre el primer " para que luego al iterar lo encuentr FIND
                #if defined(STACK_USE_UART)
                putcUART(D[0]);
                #endif
            }
            
            return (0xFFFF);
        }    
        
    }else
        
        return (0xFFFF);
        
    return(0xFFFF);
}

CHAR *Get_Obj_Json_TCP_Array( BYTE TCPS, ROM BYTE* str){    
    
    static CHAR D[21];  // Mayor longitud de campo              
    WORD a,l,len;
    BYTE i;
    CHAR *b;
    
    l= strlen((const char*)str);
    len = TCPIsGetReady((TCP_SOCKET)TCPS);                                      // faltantes por leer 
    
    a = TCPFindROMArrayEx((TCP_SOCKET)TCPS,(ROM BYTE*)"\"}",l,0,(WORD)len, FALSE); // consulto fin de algun objeto Json
    
    if (a!= 0xffff){
        
        a = TCPFindROMArrayEx((TCP_SOCKET)TCPS,(ROM BYTE*)str,l,0,(WORD)len, FALSE);
        if (a!=0xffff)  // ubicacion de la cadena y que entre completo el objeto Json   
        {
            if((a+40)<=len)
            {                
                a= a+l+3; //cero + (l-1)+(:"_)
                //length("Tipo":")+(datos)   {"variable":"cadena",}
                 while (a >= 20) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*) D, 20);
                    a -= 20;
                    #if defined(STACK_USE_UART)                    
                    putsUART((char*)D);
                    #endif
                    
                }
                while (a >= 10) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*) D, 10);
                    a -= 10;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D); 
                    #endif
                }
                while (a> 0) {
                    TCPGet((TCP_SOCKET)TCPS,(BYTE*) D);
                    a--;               //estoy ubicado sobre el primer caracter del dato
                    #if defined(STACK_USE_UART)
                    putcUART(D[0]);
                    #endif
                }                
                //length("Tipo":")+(datos)   {"variable":"cadena",}
                
                i= 0;
                TCPGet((TCP_SOCKET)TCPS,(BYTE*)D);
                while((D[i]!='\"') && (i<21))
                {
                    i++;
                    TCPGet((TCP_SOCKET)TCPS,(BYTE*)(D+i));                        
                }
                //TCPGetArray(TCPS,D,20); //variable ubicacion 7
                D[i] = '\0';
                #if defined(STACK_USE_UART)
                putsUART((char*)D);
                #endif

                return((CHAR*)D );     //Salida Valida
                
            }else {           //hago que evolucione la fifo
                
                while (a >= 20) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 20);
                    a -= 20;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D);
                    #endif
                }
                while (a >= 10) {
                    TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 10);
                    a -= 10;
                    #if defined(STACK_USE_UART)
                    putsUART((char*)D);
                    #endif
                }
                while (a> 0) {
                    TCPGet((TCP_SOCKET)TCPS,(BYTE*) D);
                    a--;     //estoy ubicado sobre el primer " para que luego al iterar lo encuentr FIND
                    #if defined(STACK_USE_UART)
                    putcUART((char)D[0]);
                    #endif
                }
            } 
        }else {
                            //hago que evolucione la fifo
            a = TCPFindROMArrayEx((TCP_SOCKET)TCPS,(ROM BYTE*)"{\"",l,0,(WORD)len, FALSE); // consulto inicio de algun objeto Json
            
            while (a >= 20) { // libero FIFO de a 20 elementos y termino en 40
                TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 20);
                a -= 20;
                #if defined(STACK_USE_UART)
                putsUART((char*)D);
                #endif
            }
            while (a >= 10) {
                TCPGetArray((TCP_SOCKET)TCPS,(BYTE*)D, 10);
                a -= 10;
                #if defined(STACK_USE_UART)
                putsUART((char*)D);
                #endif
            }
            while (a> 0) {
                TCPGet((TCP_SOCKET)TCPS,(BYTE*) D);
                a--;     //estoy ubicado sobre el primer " para que luego al iterar lo encuentr FIND
                #if defined(STACK_USE_UART)
                putcUART((char)D[0]);
                #endif
            }
            
            return (NULL);
        }    
        
    }else
        
        return (NULL);
        
    return(NULL);
}

/*****************************************************************************
  Function:
    void GetJSON(TCP_SOCKET TCPSOC)

  Summary:
    Retrieve data from JSON (over TCP).

  Description:
  -Tramas:     
       -General
            
            uint16_t  {IdGW: } // identificador dispositivo asignado en la asociacion
                      {Pass:}
            uint8_t    Secuencia;
            uint8_t   {Tipo:}; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
       -T=1: Normal          
  
       -T=2: Asociacion
            uint16_t  {Id } 
            uint16_t  Slot;
            Note: el gw agrega el set point de date
       -T=3: Time 
             DATE_t   DateTx;     
       -T=4: Asociacion GW 
             Id_GW
       -T=5: Control 
            uint16_t  Slot;      

  Precondition:
	
  Parameters:
    None

  Returns:
    None
 ***************************************************************************/
TCPRxState GetJSON(BYTE TCPSOC){
     
    static TCPRxState RxState = AUTEN_RX;   
    static BOOL st= FALSE;
    
    WORD  a= 0;
    BYTE  i;    
    CHAR  *p;  
    BYTE  *p1;        
    WORD  len;
    char D[21];  // Mayor longitud de campo              
    
    
    /* 
                               Objeto JSON PRINCIPAL       
     AUTEN:
           "Id_GW"      :0001,
           "Pass"       :"1234abcd",                   //ya enviada acceso al GW 
           "Pass"       :"1234abcd",                   //nueva acceso al servidor futura
     CONTROL
           "Secuencia"  :003,                          // SECUENCIA DEL GW para machear
           "Tk_Srv"     :0100,                         // Nueva LLAVE DE APLICACION GW-Serv              
     TIPO
           "Tipo"
     PROCESS
           
           -T=0: nOP   
                      } 
           -T=1: Normal 
                      }   
           -T=2: Asociacion
                 "Id_M"
                 "Token"
                 "Slot"
                 "Id_A":0000}
     
           -T=3: Time 
                 "Date":"00:...:00"}
     
           -T=4: Asociacion PAN
                 "Id_M"
                 "Token"                
                 "Id_PAN":0000}
     
           -T=5: Control 
                  "Id_M":0000",
                  "Token":0000,                         //llave mimia->APPserv
                  "TokenMM":0000,                       //llave APPserv->mimia
                  "Subtipo":5,                         
                  "DatCtrl":{
                              "Index": 3,                            
                              "Data":[ "dato1", "dato2", "dato3" ]
                            } }      
     *      -T=6: Asociacion GW                                  
                 "Id_GW":0000} 
     *          
                        ARMO A CONTINUCACION SEGUN EL TIPO DE SERVICIO
    */     
    len = TCPIsGetReady((TCP_SOCKET)TCPSOC);
    
    switch (RxState) 
    {
        case AUTEN_RX:

            if (len >100)
            {                                            
                if (!st)
                {
                   a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_GW\"");     //Filtro ID
                   if (0xffff!=a) 
                   {
                       App.ID_GW= a;
                       st++;
                   }else          // restan al menos 66 elementos que retirar de tcp
                        return(EVOLUCION_RX);//hago que evolucione el buffer recepcion al retornar a TCPCLiente
                }
            } 
            
            if(st)
            {                                
                p= Get_Obj_Json_TCP_Array(TCPSOC,(ROM BYTE*)"\"Pass_GW\""); 
                 
                if (p!= NULL)
                {
                    RxState= ERR_RX;
                    
                    if ((strcmp((char*)PASS.Pass_GW,(char*)p) == 0) && (PASS.Id_GW.Val == App.ID_GW))
                        RxState = CONTROL_RX; // trama valida analizo secuencias                    
                        
                    p= Get_Obj_Json_TCP_Array(TCPSOC,(ROM BYTE*)"\"Pass_SRV\"");                    
                    if ((p!= NULL) && (RxState == CONTROL_RX))
                    {                                                                    
                        strncpy((char*)App.Pass_SRV,(char*)p,LENGTH_PASS); //Salvo token para proxima comunicacion                                                    
                    }else
                        RxState= ERR_RX;                    
                }
                else
                    RxState= ERR_RX;
                st--;
            }      
            
        break;

        case CONTROL_RX:
            
            if (len >30) 
            {    
                a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Secuencia\"");               
                
                if (a!= 0xffff)
                {
                    RxState= ERR_RX;                    
                    if (App.Sec_GW == (BYTE) a)
                        RxState = TIPO_RX; // trama valida, analizo secuencias
                }
                
                a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Tk_Srv\""); // token que llega del Srv para el proxima comunicacion
                
                if (a!= 0xffff && (RxState == TIPO_RX))                                                       
                    App.TK_SrvGw = (BYTE) a;
                else
                    RxState= ERR_RX;
            }    

        break;
        
        case TIPO_RX:          
            if (len > 20) 
            {                
                a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Tipo\"");
                if (0xffff != a)
                {
                    RxState= ERR_RX;
                    if ((0 == (BYTE)a)||(1 == (BYTE)a) ||(2 == (BYTE)a) ||(3 == (BYTE)a) 
                        ||(4 == (BYTE)a) ||(5 == (BYTE)a)||(5 == (BYTE)a))
                    {
                        App.Tipo= (BYTE)a;
                        RxState= PROCESS_RX ;
                    }
                }                 
            }                

        break;

        case PROCESS_RX:        
            
            switch (App.Tipo) 
            {   
                case nOP : //-----------------nOP 
                    
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"}",1,0,(WORD)len, FALSE);// fin de objetos Json http
                    
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;

                    RxState= DONE_RX;
                    
                break;
                
                case OP_NORMAL : //-----------------NORMAL 
                    
                    //p = strstr(rxdataPort60,"}"); // fin de objetos Json http
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"}",1,0,(WORD)len, FALSE);// fin de objetos Json http
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;                    
                    
                    RxState= ERR_RX;
                    a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_PN\"");     // ID MAC a la que va dirigido 
                    if (a != 0xffff )
                    {
                        App.ID= a;

                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Token\""); //token mimia-serv
                        if (a!=0xffff )
                        {
                            App.TOKEN= a;
                            RxState= DONE_RX;
                        }
                    }
                                        
                break;

                case OP_ASOCIACION: // -----------------ASOCIA
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"}",1,0,(WORD)len, FALSE);// fin de objetos Json http
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;

                    RxState= ERR_RX;
                    a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_M\""); // ID MAC a la que va dirigido 
                    if (a!=0xffff )
                    {
                        App.ID= a;

                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Token\""); //token mimia-serv
                        if (a!=0xffff )
                        {
                            App.TOKEN= a;
                            RxState= DONE_RX;
                        }

                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Slot\"");  //ranura de contienda
                        if ((a!=0xffff) && (RxState == DONE_RX))
                            App.Slot= a;
                        else
                            RxState= ERR_RX;

                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_Aut\"");  //ID de asignacion del Serv  
                        if ((a!=0xffff) && (RxState == DONE_RX))
                            App.ID_AUT= a;
                        else
                            RxState= ERR_RX;
                    } 
                     
                break;

                case OP_TIME: //-----------------CLOCK                                            
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"\"}",2,0,(WORD)len, FALSE);// fin de objetos Json http
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;

                    RxState= ERR_RX;
                    p= Get_Obj_Json_TCP_Array(TCPSOC,(ROM BYTE*)"\"Date\"");

                    if (p!= NULL )//&&(p!=0xffff))
                    {                        
                        RxState= DONE_RX;
                        /*
                         * D=hh:mm:ss-DD/MM/AA
                            uint8_t Seg;
                            uint8_t Min;
                            uint8_t Hou;
                            uint8_t Day;
                            uint8_t Mon;
                            uint8_t Yea;
                        }DATE_t;*/
                        p1= &App.DateTx.Seg; //GW_RTCC.Seg; 

                        for(i=0;i<17;i+=3)   //12:45:78:ab:de:gh
                        {
                            *p1 = (BYTE)(*(p+i)-'0');
                            *p1 <<= 4; 
                            *p1|= (BYTE)(*(p+1+i)-'0');

                            p1++;
                        }
                    }                        
                break;

                case OP_ASOCIACION_PAN:// -----------------ASOCIA PAN
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"}",1,0,(WORD)len, FALSE);// fin de objetos Json http
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;

                    RxState= ERR_RX;
                    a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_PN\"");
                    if (a!= 0xffff )
                    {
                        App.ID= a;
                        
                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Token\""); //token mimia-serv
                        if (a!=0xffff && (RxState == DONE_RX))
                        {
                            App.TOKEN= a;
                            RxState = DONE_RX;
                        }
                        
                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_Aut\"");  //ID de asignacion del Serv  
                        if ((a!=0xffff) && (RxState == DONE_RX))
                            App.ID_AUT= a; 
                        else
                            RxState= ERR_RX;
                    }         
                                        
                break;

                case OP_CONTROL: //-----------------CONTROL
                    /*  
                     * "Tipo":5,
                    *  "Id_M":0000",
                    *  "Token":0000,       //llave mimia->APPserv
                    *  "TokenMM":0000,     //llave APPserv->mimia
                    *  "Subtipo":5,                         
                    *  "DatCtrl":{
                                  "Index": 3,(CONTENIO INCLUSO LLAVES Y FORMATOS xml JSON O PROPIO)                            
                                  "Data":[ "dato1", "dato2", "dato3" ]
                                 }
                    *  } # llave de elemento json HTTP   
                    */                    
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"\"]}}",4,0,(WORD)len, FALSE);// fin de objetos Json http
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;

                    RxState= ERR_RX;
                    a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_Dvc\""); //device
                    if (a!= 0xffff)
                    {
                        App.ID=a;
                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Token\"");   //token que envia el servidor
                        if (a!=0xffff)
                        {
                            App.TOKEN= a;
                            RxState= DONE_RX;
                        }
                        
                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"TokenMM\""); //token enviado por el mm previo
                        if((a!=0xffff) && (RxState == DONE_RX))
                        {
                            App.TKMM= a;                            
                        }
                        else
                            RxState= ERR_RX;
                        
                        a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Subtipo\"");
                        if ((a!=0xffff) && (RxState == DONE_RX))
                        {
                            App.SubTipo= a;                            
                        }
                        else
                            RxState= ERR_RX;
                        
                        //a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,"\"DatCtrl\"");
                        len = TCPIsGetReady((TCP_SOCKET)TCPSOC);
                        a= TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"\"DatCtrl\"",9,0,(WORD)len, FALSE); //Encuentra la estructura del objeto
                       
                        if ((a!=0xffff) && (RxState == DONE_RX))
                        {
                            a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Index\"");
                            if ((a != 0xffff) && (a < CTRL_LNGTH))
                            {
                                App.DatCtrl.Index= a;                                    
                                len = TCPIsGetReady((TCP_SOCKET)TCPSOC);
                                a= TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"\"Data\"",6,0,(WORD)len, FALSE); //Encuentra la estructura del objeto
                                
                                if (a!=0xffff)  // ubicacion de la cadena y que entre completo el objeto Json   
                                {                                         
                                    a= a+8; //cero + (l-1)+(:[_)
                                    /*  
                                        "DatCtrl":{
                                                    "Index": 3,(CONTENIO INCLUSO LLAVES Y FORMATOS xml JSON O PROPIO)
                                                    "Data":[

                                         length("Tipo":")+(datos)   {"variable":"cadena",}
                                    */                                                                                
                                    while (a >= 20) {
                                        TCPGetArray((TCP_SOCKET)TCPSOC,(BYTE*) D, 20);
                                        a -= 20;
                                        #if defined(STACK_USE_UART)                    
                                        putsUART((char*)D);
                                        #endif

                                    }
                                    while (a >= 10) {
                                        TCPGetArray((TCP_SOCKET)TCPSOC,(BYTE*) D, 10);
                                        a -= 10;
                                        #if defined(STACK_USE_UART)
                                        putsUART((char*)D); 
                                        #endif
                                    }
                                    while (a> 0) {
                                        TCPGet((TCP_SOCKET)TCPSOC,(BYTE*) D);
                                        a--;               //estoy ubicado sobre el primer caracter del dato
                                        #if defined(STACK_USE_UART)
                                        putcUART(D[0]);
                                        #endif
                                    }                                                                                                                                        
                                    /*"DatCtrl":{
                                                "Index": 3,(CONTENIO INCLUSO LLAVES Y FORMATOS xml JSON O PROPIO)                                                        
                                                "Data":["dato1", "dato2", "dato3"]
                                                        ^estoy aqui(luego del corchete)
                                                }
                                     */                                    
                                    p1= &App.DatCtrl.Data[0];            
                                    
                                    for(i= 0; i<=App.DatCtrl.Index; i++)                                        
                                        TCPGet((TCP_SOCKET)TCPSOC,p1++);
                                        
                                }else
                                     RxState= ERR_RX;
                            }
                            else
                                RxState= ERR_RX;
                        }
                        else
                            RxState= ERR_RX;
                    }
                    
                break;
                
                case OP_ASOCIACION_GW:// -----------------ASOCIA GW
                                        
                    a = TCPFindROMArrayEx((TCP_SOCKET)TCPSOC,(ROM BYTE*)"}",1,0,(WORD)len, FALSE);// fin de objetos Json http
                    if (a == 0xFFFF)// || (p==0xffff))
                        break;                                 
                    
                    RxState= ERR_RX;
                    a= Get_Obj_Json_TCP_ChartoInt(TCPSOC,(ROM BYTE*)"\"Id_Aut\"");
                    if (a!= 0xffff )
                    {
                        App.ID_AUT= a;
                        RxState = DONE_RX;
                    }         
                                        
                break;

                default : //-----------------caso de error
                    
                    App.Tipo=0;
                    RxState=ERR_RX;
                    
                break;
            }
                        
        break;
         
        case DONE_RX:
               RxState=AUTEN_RX; //vuelve a la analizar desde cero trama valida            
               return(AUTEN_RX);        
        break;
        
        case EXIT_RX:
               RxState=AUTEN_RX;            
               return(AUTEN_RX);        
        break;    
        
        case ERR_RX:
               RxState =AUTEN_RX; //vuelve a la analizar desde cero trama invalida
               return(AUTEN_RX);
        break;                  
    }
    return(RxState);
}


