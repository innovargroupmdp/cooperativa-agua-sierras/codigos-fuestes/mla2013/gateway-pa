#ifndef TCP_clientDemo_H
#define TCP_clientDemo_H

enum State_tcp_ip_Def_t{
    SM_SOCKET_OBTAINED=0,
    SM_PROCESS_RESPONSE
};

enum state_tcp_Client_Def_t{
        OBTAINED=0,
        TRANS,
        AUTEN,
        CONTROL,
        TIPO,        
        PROCESS,
        DONE_st,
        EXIT_st,
        ERR_st        
};

typedef enum state_tcp_Client_Def_t State_tcp_Client_Def ;
typedef enum State_tcp_ip_Def_t State_tcp_ip_Def ;

State_tcp_Client_Def TCP_Client(void);

void TCP_Client_Initialize(void);

#endif