/*********************************************************************
 *
 *  Generic TCP Client Example Application
 *  Module for Microchip TCP/IP Stack
 *   -Implements an example HTTP client and should be used as a basis 
 *	  for creating new TCP client applications
 *	 -Reference: None.  Hopefully AN833 in the future.
 *
 *********************************************************************
 * FileName:        GenericTCPClient.c
 * Dependencies:    TCP, DNS, ARP, Tick
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     8/01/06	Original
 ********************************************************************/
#define __GENERICTCPCLIENT_C

#include <string.h>
#include "../TCPIPConfig.h"

#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)

#include "TCPIP Stack/TCPIP.h"

#include "App.h"
#include "TCP-IP_APPs.h"
#include "GenericTCPClient.h"


// Defines the server to be accessed for this application

char ServerName[] =	"192.168.1.20";

// Defines the port to be accessed for this application
#if defined(STACK_USE_SSL_CLIENT)
    WORD ServerPort = HTTPS_PORT;
	// Note that if HTTPS is used, the ServerName and URL 
	// must change to an SSL enabled server.
#else
    WORD ServerPort = HTTP_PORT;
#endif

// Defines the URL to be requested by this HTTP client
char RemoteURL[] = "/CSPA/ServZ0";


/*****************************************************************************
  Function:
	void TCPClient(void)

  Summary:
	Implements a simple HTTP client (over TCP).

  Description:
	This function implements a simple HTTP client, which operates over TCP.  
	The function is called periodically by the stack, and waits for BUTTON1 
	to be pressed.  When the button is pressed, the application opens a TCP
	connection to an Internet search engine, performs a search for the word
	"Microchip" on "microchip.com", and prints the resulting HTML page to
	the UART.
	
	This example can be used as a model for many TCP and HTTP client
	applications.

  Precondition:
	TCP is initialized.

  Parameters:
	None

  Returns:
  	None
  ***************************************************************************/
State_tcp_Client_Def TCP_Client(void)
{
	BYTE 				i;
	WORD				w;
	BYTE				vBuffer[9];
	static BOOL 		s;
    static DWORD		Timer;
	static TCP_SOCKET	MySocket= INVALID_SOCKET;
    static TCPRxState   St_G= AUTEN_RX;
	static enum _GenericTCPExampleState
	{
		SM_HOME = 0,
		SM_SOCKET_OBTAINED,
		#if defined(STACK_USE_SSL_CLIENT)
    	SM_START_SSL,
    	#endif
		SM_PROCESS_RESPONSE,
		SM_DISCONNECT,
		SM_DONE
	} GenericTCPState = SM_DONE;

	switch(GenericTCPState)
	{
		case SM_HOME:
			// Connect a socket to the remote TCP server
			MySocket = TCPOpen((DWORD)(PTR_BASE)&ServerName[0], TCP_OPEN_RAM_HOST, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
			
			// Abort operation if no TCP socket of type TCP_PURPOSE_GENERIC_TCP_CLIENT is available
			// If this ever happens, you need to go add one to TCPIPConfig.h
			if(MySocket == INVALID_SOCKET)
				break;

			#if defined(STACK_USE_UART)
			putrsUART((ROM char*)"\r\n\r\nConnecting using Microchip TCP API...\r\n");
			#endif

			GenericTCPState++;
			Timer = TickGet();
			break;

		case SM_SOCKET_OBTAINED:
			// Wait for the remote server to accept our connection request
			if(!TCPIsConnected(MySocket))
			{
				// Time out if too much time is spent in this state
				if(DIFF_TICK(TickGet(),Timer) > 5*TICK_SECOND)
				{
					// Close the socket so it can be used by other modules
					TCPDisconnect(MySocket);
					MySocket = INVALID_SOCKET;
					GenericTCPState--;
				}
				break;
			}

			Timer = TickGet();
			
    #if defined (STACK_USE_SSL_CLIENT)
            if(!TCPStartSSLClient(MySocket,(void *)"thishost"))
                break;
			GenericTCPState++;
			break;

        case SM_START_SSL:
            if (TCPSSLIsHandshaking(MySocket)) 
            {
				if(TickGet()-Timer > 10*TICK_SECOND)
				{
					// Close the socket so it can be used by other modules
					TCPDisconnect(MySocket);
					MySocket = INVALID_SOCKET;
					GenericTCPState=SM_HOME;
				}
                break;
            }
    #endif

			// Make certain the socket can be written to
			if(TCPIsPutReady(MySocket) < 125u)
				break;
			
			// Place the application protocol data into the transmit buffer.  For this example, we are connected to an HTTP server, so we'll send an HTTP GET request.
			TCPPutROMString(MySocket, (ROM BYTE*)"POST ");
			TCPPutString(MySocket,(BYTE*) RemoteURL);
			TCPPutROMString(MySocket, (ROM BYTE*)" HTTP/1.0\r\nHost: ");
			TCPPutString(MySocket,(BYTE*) ServerName);
            TCPPutROMString(MySocket, (ROM BYTE*)"\r\nAccept: application/json\r\nContent-Type: application/json\r\nContent-Length: ");
			// Send the packet: T=0: nOP || T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Aso_PN ||T=5: control ||T=6: Aso_Gw
            PutJSON(MySocket);
            // Close anuncio al servidor transmition luego de la respuesta
            TCPPutROMString(MySocket, (ROM BYTE*)"\r\nConnection: close\r\n\r\n");
			// Send the packet
			TCPFlush(MySocket);
			GenericTCPState++;
			break;

		case SM_PROCESS_RESPONSE:
			// Check to see if the remote node has disconnected from us or sent us any application data
			// If application data is available, write it to the UART
			if(!TCPIsConnected(MySocket))
			{
				GenericTCPState = SM_DISCONNECT;
				// Do not break;  We might still have data in the TCP RX FIFO waiting for us
			}
            
	        if (s)
            {
                strncpy((char*)App.Pass_GW,PASS.Pass_GW,LENGTH_PASS);
                s--;
            }            				
			// Obtian and print the server reply            
            St_G= GetJSON((BYTE)MySocket);
            
            #if defined(STACK_USE_UART)
                switch(St_G){
                    case AUTEN_RX:
                        putrsUART("AUTEN_RX");
                    break;
                    case CONTROL_RX:
                        putrsUART("CONTROL_RX");
                    break;
                    case TIPO_RX:
                        putrsUART("TIPO_RX");
                    break;
                    case PROCESS_RX:
                        putrsUART("PROCESS_RX");
                    break;
                    case DONE_RX:
                        putrsUART("DONE_RX");
                    break;
                    case EXIT_RX:
                        putrsUART("EXIT_RX");
                    break;
                    case ERR_RX:
                        putrsUART("ERR_RX");
                    break;
                    case EVOLUCION_RX:
                        putrsUART("EVOLUCION_RX");
                    break;
                }                                    
            #endif
            
            switch(St_G){
            
                case DONE_RX:
                            //DONE:Rutina terminada de recepcion datos completos de App
                            strncpy((char*)App.Pass_SRV,PASS.Pass_SRV,LENGTH_PASS);
                            GenericTCPState= SM_DISCONNECT;                                  
                break;
                
                case ERR_RX:
                            //ERR: Algo ocurrio datos fuera de protocolo
                            GenericTCPState= SM_DISCONNECT;                                     
                break;
                
                default:    
                            #if defined(STACK_USE_UART)
                                    switch(St_G){
                                        case AUTEN_RX:
                                            putrsUART("AUTEN_RX");
                                        break;
                                        case CONTROL_RX:
                                            putrsUART("CONTROL_RX");
                                        break;
                                        case TIPO_RX:
                                            putrsUART("TIPO_RX");
                                        break;
                                        case PROCESS_RX:
                                            putrsUART("PROCESS_RX");
                                        break;
                                        case DONE_RX:
                                            putrsUART("DONE_RX");
                                        break;
                                        case EXIT_RX:
                                            putrsUART("EXIT_RX");
                                        break;
                                        case ERR_RX:
                                            putrsUART("ERR_RX");
                                        break;
                                        case EVOLUCION_RX:
                                            putrsUART("EVOLUCION_RX");
                                        break;
                                    }
                                    
                            #endif
                                
            }    
            
		case SM_DISCONNECT:
			// Close the socket so it can be used by other modules
			// For this application, we wish to stay connected, 
            // but this state will still get entered if the remote server decides to disconnect
			TCPDisconnect(MySocket);
            
            switch(St_G){
            
                case DONE_RX:
                            //DONE:Rutina terminada de recepcion datos completos de App
                                                        
                            St_G = AUTEN_RX;                            
                            
                            MySocket = INVALID_SOCKET;
                            GenericTCPState = SM_DONE;
                            return(DONE_st);        
                            
                break;
                
               default:
                            //ERR: Algo ocurrio datos fuera de protocolo                            
                            St_G = AUTEN_RX;
                            
                            MySocket = INVALID_SOCKET;
                            GenericTCPState = SM_DONE;
                            return(ERR_st);         
                                                
            } 
            
			break;
	
		case SM_DONE:
			// Do nothing unless the user pushes BUTTON1 and wants to restart the whole connection/download process
			if(BUTTON0_IO == 0u)
				GenericTCPState = SM_HOME;
			break;
	}
    
    return(EXIT_st);
}

#endif	//#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)
