/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

#include "GenericTypeDefs.h"

#define Lngth_Json_nOP    3
#define Lngth_Json_Norm   24
#define Lngth_Json_Aso    13
#define Lngth_Json_Aso_PN 13
#define Lngth_Json_Aso_GW 13
#define Lngth_Json_Time   0
#define Lngth_Json_Ctrl   20

enum TCPRxState_t {
        
        AUTEN_RX=0,
        CONTROL_RX,
        TIPO_RX,        
        PROCESS_RX,
        DONE_RX,
        EXIT_RX,
        ERR_RX,
        EVOLUCION_RX        //estado de operacion transitoria inter estado
    };
 
typedef enum TCPRxState_t TCPRxState;
    
/* TODO Application specific user parameters used in user.c may go here */

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

// Defines the server to be accessed for this application
//const char ServerName[] = "192.168.1.20";

// Note that if HTTPS is used, the ServerName and URL 
// must change to an SSL enabled server.

//static WORD ServerPort = HTTP_PORT;


// Defines the URL to be requested by this HTTP client
//const char RemoteURL[] = "/CSPA/ServZ0";

#define length_buffer_RX 200
#define length_buffer_TX 200


void Gen_Pass(char*);

void reverse(char*);

//void itoa(char*, WORD );

//void utoa(char*, WORD );

void PutFecha(BYTE *, BYTE);

void PutJSON( BYTE );

WORD  Get_Obj_Json_TCP_ChartoInt( BYTE, ROM BYTE*);

CHAR *Get_Obj_Json_TCP_Array( BYTE, ROM BYTE*);

TCPRxState GetJSON( BYTE ); 

