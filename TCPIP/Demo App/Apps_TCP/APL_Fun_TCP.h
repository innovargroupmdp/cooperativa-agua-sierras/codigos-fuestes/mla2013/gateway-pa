/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H
#endif

//#include "../App.h"

void Clr_App(BYTE*); //generica

void Set_App_From_APP_TCP(void);

BOOL Proc_First_TCP(BOOL);

/*--------RUTINAS PARA RTCC-----*/
void ReadRTCC(void);              //generico 
void RTCC_Set(void);              //genrecio 
void RTCC_Get(BYTE*);             //generico 

/*----RUTINAS PARA EEPROM, NVM--*/
BOOL Set_From_NVM(void);          //generico  
void Set_ASO_NVM(void);           //generico  

//--- buffer de tramas -----

BOOL Put_Base(void);             //generico
void Clear_Buffer_Norm_TCP(void);

BYTE Put_Buffer_Ctrl_TCP(void);       //generico

//BOOL UpDate_BufferCtrl_MW(Ctrl_Buffer_t*);
BOOL Clr_Buffer_Ctrl_TCP(Ctrl_Buffer_t*); //generico

/*----RUTINAS APP -------------*/
void Search_Control_TCP(void);          //generico
BOOL APP_Ctrl_TCP(void);

//COM_State_t Resp_CTRL_MB(void);
//COM_State_t Tx_CTRL_MB(void);

//*******ruteo por ID y Add MB sonre PAN
WORD Get_TK_BufferTK_TCP(BYTE);
void Put_TK_BufferTK_TCP(void);

BYTE  Indx_IdPan_from_AddMB(void);          //generico
BYTE  Indx_from_IdPan(BYTE);                //generico

BOOL Put_Buffer_Aso_TCP(void);

BOOL Put_Buffer_PN_TCP(void);

//bool Get_Buffer_MIWI(void); No existe analogia

BOOL Get_Buffer_Aso_TcpIp(void);
BOOL Get_Buffer_PN_TcpIp(void);
//bool Get_BufferTx_MIWI(void); No existe analogia
 
