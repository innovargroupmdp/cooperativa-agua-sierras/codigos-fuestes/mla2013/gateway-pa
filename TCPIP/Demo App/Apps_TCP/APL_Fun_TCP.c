/*
 * File:   APL_Fun.c
 * Author: cba
 *
 * Created on 23 de mayo de 2018, 11:39
 */


#include <stddef.h>
#include <string.h>
#include <rtcc.h>
//#include <pic18.h>

//#include "mcc_generated_files/memory.h"
//#include "mcc_generated_files/TCPIPLibrary/tcpip_config.h"
//#include "mcc_generated_files/TCPIPLibrary/rtcc.h"

//#include "APP_MIWI_ModBus.h"
#include "../App.h"
#include "APL_Fun_TCP.h"
#include "Tick.h"

//#include "MIWI.h"  se encuentra en APL_FUN
//#include "App.h"  se encuentra en APL_FUN

extern BOOL Nw_Frm_ASO ;
extern BOOL Nw_Frm_CTRL;
extern BOOL Nw_Frm_ASO_PN;
       BOOL Sem_Ctrl_GW= FALSE;

void Clr_App(BYTE *p){
    
    WORD i,f;
    
    //APP TCPIP p= (uint8_t*)&App.Pass_SRV;
    //APP1 MP   p= (uint8_t*)&App1.Sec_GW; 
    f= sizeof(App);    
    for (i=0;i< f;i++)
        *(p++)=0;    
    
    return;    
}

/**********************************************
 seteo las variables adecuadamente segun el Tipo
 * Requisitos: debe estar seteado App.Tipo
 ************************************************/
void Set_App_From_APP_TCP(void){
    BYTE *p1= NULL;
    BYTE *p=  NULL;
    BYTE  i,d,k;
    BYTE  j= 0; 
      
    App.ID_GW=    PASS.Id_GW.Val; 
    strncpy((char*)App.Pass_SRV,PASS.Pass_SRV,LENGTH_PASS); //Salvo token para proxima comunicacion                                                    
    //App.Pass_SRV= PASS.Pass_SRV;    
    //App.Pass_GW=  PASS.Pass_GW; //segenera dentro de TCP CLIENTE
    App.Sec_GW=   PASS.Sec;
    App.TK_SrvGw= PASS.TK_SRV.Val;
           
    switch (App.Tipo)
    {   
        case OP_NORMAL:              
            //completo en tcp put json
        break;

        case OP_ASOCIACION:
                       
            j= 0;
            while(j< BUFFER_LNGTH)
            { 
                if (!Buffer_Aso[j].Send && !Buffer_Aso[j].Rece && (!Buffer_Aso[j].ID_AUT) )
                { //consulta a la buffer
                    App.ID= Buffer_Aso[j].Element.ID_AUI;   //ID MAC para que se reconzca el mimia                     
                    Buffer_Aso[j].Sec_GW= App.Sec_GW;
                    Buffer_Aso[j].Send=   TRUE;                          
                    break;               
                }
                j++; 
            }  
            
        break;

        case OP_ASOCIACION_PAN:
            
            j= 0;
            while(j< N_PAN)
            { 
                if (!Buffer_PN[j].Send && !Buffer_PN[j].Rece && !Buffer_PN[j].ID_AUT )
                { //consulta a la buffer                            

                    App.ID= Buffer_PN[j].Element.ID_AUI;   //ID MAC para que se reconzca el mimia                                                      
                    Buffer_PN[j].Sec_GW= App.Sec_GW;
                    Buffer_PN[j].Send=   TRUE;
                    break;          
                }
                j++; 
            }
            
        break;
        case OP_TIME:
            // No corresponde                            

        break;
        case OP_CONTROL:
            
            /*Desde Ctrl_Buffer_R leo las tramas 
             * enviadas por los dispositivos*/ 
            
            // @TODO ( PARA VERSION POSTERIOR ) completar segun el buffes por PAM
            //       tambien para completar si es para el caso de ser control del GW 
            if (!Sem_Ctrl_GW)
            {    
                i= Ctrl_Buffer_R[0].index_to_send;
                /*-HEADER-*/
                App.ID=      Ctrl_Buffer_R[0].Ctrl_Base[i].ID; 
                App.TOKEN=   Ctrl_Buffer_R[0].Ctrl_Base[i].TKSRV; 
                App.TKMM=    Ctrl_Buffer_R[0].Ctrl_Base[i].TKMM;
                App.SubTipo= Ctrl_Buffer_R[0].Ctrl_Base[i].SubTipo;
                /*-DATA OF LAYER-*/
                p=  &App.DatCtrl.Index;
                p1= &Ctrl_Buffer_R[0].Ctrl_Base[i].DatCtrl.Index;

                for(j= 0; j<= App.DatCtrl.Index+1 ; j++) // de sec -> bat
                {                   
                    *p= *p1;
                    p++;
                    p1++;
                }
            }    
            else{
                /*PROVIENE DE LA APP DE CONTROL DE GW*/
                App.ID=      PASS.Id_GW.Val; 
                App.TOKEN=   PASS.TK_SRV.Val;
                //@TODO ( PARA VERSION POSTERIOR ) generar con el timer el TK
                App.TKGW=    PASS.TK_GW.Val;
                App.SubTipo= App_Ctrl.SubTipo;
                /*-DATA OF LAYER-*/
                p=  &App.DatCtrl.Index;
                p1= &App_Ctrl.DatCtrl.Index;

                for(j= 0; j<= App.DatCtrl.Index +1 ; j++) // de sec -> bat
                {                   
                    *p= *p1;
                    p++;
                    p1++;
                }
                //@TODO ( PARA VERSION POSTERIOR ) ver donde ubicar el blanqueo del semaforo
            }
                

        break;
        case OP_ASOCIACION_GW:
            
            // No corresponde 

        break;   
    }   
    /*-DATA OF LAYER-*/
    /* Rago de control :
    * Sub codigos App.Tipo>=5;
    * 00-09: De RED:Cambio de Slot, RFD, FFD.
    * 10-19: De Monitoreo Interno: temperatura, humedad, scaneo, consumo, generacion solar, velociadad viento.
    * 20-29: De Control Interno: reset POW, reset blank, cambio PAN, cambio de canal, cambio de banda, control potencia, T de sleep.
    * 30-39: De Control Externo: Control IOs, USB, CAN, USART,Display.
    * 40-49: De Control Datos: Escritura de Variable, Lectura de Variable.
    * 50-59: De Control Programa: Control de flujo de programa(vector de punteros a funciones).
    * 60-69:
    * 70-79:
    * 80-89:
    * 90-99:
    */                                             
}




BOOL Proc_First_TCP(BOOL ini){
        
    static DWORD t1;
    static BOOL  b= FALSE;   
    
    Clr_App((BYTE *)App.Pass_SRV);
    /*LO QUE HAY DESIGNADO SEGUN LAS TAREAS PENDIENTES EN EL SISTEMA*/
                
    if ( !ini ){
        App.Tipo =   OP_ASOCIACION_GW;   //----Asociacion PANCO entra solo una vez 
        App.Tipo_st= OP_ASOCIACION_GW; 
        /* Asociacion  que se incia una vez que levanta alimentacion de tension,
         * debo leer EEPROM para saber si es la primer vez. Si se requiere por falla
         * utilizar el TOKEN_INI para pedir nuevo Token ya que el dispositivo 
         * ya esta asociado por MAC y tiene ID asociado */    
    }
    else
        if ( Get_Buffer_PN_TcpIp() ){
            App.Tipo =   OP_ASOCIACION_PAN;
            App.Tipo_st= OP_ASOCIACION_PAN;    
        }         
    else    
        if ( Get_Buffer_Aso_TcpIp() ){ /* Prioridad !!!*/
        // solo entra una vez que obtego el  
        // busco en Buffer sin enviar  PRIORIDAD 1: TIP.2 ( Asociacion-Nodo)                                        
            App.Tipo=    OP_ASOCIACION;
            App.Tipo_st= OP_ASOCIACION;    
        }
    else 
        if ( Buff_NRM_MW ){
        // Alarma de NORM para sobre escribir, BufferRX(vienen de MIWI) // PRIORIDAD 2: TIP.1
        
            App.Tipo   = OP_NORMAL;       //  CUANDO ENTRE AL APP SERVIDOR ENVIA BUFFER TRAMAS NORMARLES
            App.Tipo_st= OP_NORMAL;    
        }
    else 
        if ( Buff_CTRL_MW ){
            App.Tipo =   OP_CONTROL;
            App.Tipo_st= OP_CONTROL;
        }
    else 
        if ( Buff_RTCC_MW ){
            App.Tipo =   OP_TIME;
            App.Tipo_st= OP_TIME;    
        }
    else{
            App.Tipo =  nOP;             //  Informo que no hay nada en GW.
            App.Tipo_st=nOP;
            if (!b)
            {        
                t1= TickGet();
                b++;
            }
            if((TickGet() - t1) > (TICK_SECOND * 5))
            {   
                b--;
                return TRUE;            //  Entrego al Serv si hay algo pendiente                
            }    
            return FALSE;               //  Libero para que el GW haga otras operaciones
        }                
    // EL GateWay puede enviar tramas almacenadas
    /*Armo las respuestas*/
    //COMPLETO LAS TRAMAS CORTAS                    
    //SetApp_Short();
    
    return TRUE;
}

/*------------RUTINAS PARA RTCC-----------------------------*/

void ReadRTCC(void){
}
void RTCC_Set(void){
    
    BYTE *p,*p1;
    BYTE i;
    
    
    p=  &App.DateTx.Seg;    
    p1= &GW_RTCC.Seg;                   //GW_RTCC.Seg;
    
    for(i=0;i<6;i++) //12:45:78:ab:de:gh
    {
        *p1 = ((*p>>4)*10) +(*p & 0x0f);
        p++;
        p1++;
    }
    
    return;
}

void RTCC_Get(BYTE *app ){
    BYTE *p1,i;
    APP_t *p;
    
    ReadRTCC();
    p1= &GW_RTCC.Seg;
    
    //@TODO
    //p= app;
    //App1.DateTx.Seg;
    /*
    p= &(p->DateTx.Seg);
    
    for(i=0;i<6;i++){ 
        *p=*p1;
        p++;
        p1++;
    }
    */
    return;
}

/*------------RUTINAS PARA EEPROM, NVM-----------------------------*/

/*Ajusta variables de PASS ID y TOKEN desde NVM si es que estan seteadas 
 * previamente al verificar el bit init                                      */
BOOL Set_From_NVM(void){
    DWORD i;
    BYTE Datum;
    //NVMRead(&p , NVM_ADD_INI, 1);     
        
    // Read the NVMValidation record and AppConfig struct out of EEPROM/Flash
    i= sizeof(NVM_VALIDATION_STRUCT) + sizeof(AppConfig);
    
    #if defined(EEPROM_CS_TRIS)
    {           
        XEEReadArray((i+ NVM_ADD_INI) ,(BYTE*)&Datum , 1);
        
        if ( Datum & 0x01 )                // verifico si ya fue asociado
        {   //caso de recuperse de un reset levanta los datos de la asociacion
            XEEReadArray((i+ NVM_ADD_TOKEN_INI) ,(BYTE*)&PASS.TK_SRV.byte.HB , 2);
            //NVMRead( PASS.TOKEN.v , NVM_ADD_TOKEN_INI, 2);
            //PASS.TK_SRV.byte.HB = DATAEE_ReadByte(NVM_ADD_TOKEN_INI);
            //PASS.TK_SRV.byte.LB = DATAEE_ReadByte(NVM_ADD_TOKEN_INI +1);
            
            XEEReadArray((i+ NVM_ADD_ID) ,(BYTE*)&PASS.Id_GW.byte.HB , 2);
            //NVMRead( PASS.Id_PAN.v, NVM_ADD_ID, 2);                        
            //PASS.Id_GW.byte.HB = DATAEE_ReadByte(NVM_ADD_ID);
            //PASS.Id_GW.byte.LB = DATAEE_ReadByte(NVM_ADD_ID +1);
            
            XEEReadArray((i+ NVM_ADD_PASS_INI) ,(BYTE*)PASS.Pass_SRV , LENGTH_PASS);
            //for(i= 0; i<LENGTH_PASS; i++)
            //    PASS.Pass_SRV[i] = DATAEE_ReadByte(NVM_ADD_PASS_INI +i);            
            //PASS.Pass_SRV[i]='\0';
            PASS.Pass_SRV[LENGTH_PASS]='\0';
            
            return TRUE;
        }  
    }
    #elif defined(SPIFLASH_CS_TRIS)
    {   
        SPIFlashReadArray((i+ NVM_ADD_INI), (BYTE*)&Datum, 1);
        
         if ( Datum & 0x01 )     // verifico si ya fue asociado
        {   //caso de recuperse de un reset levanta los datos de la asociacion
            SPIFlashReadArray((i+ NVM_ADD_TOKEN_INI) ,&PASS.TK_SRV.byte.HB , 2);
            //NVMRead( PASS.TOKEN.v , NVM_ADD_TOKEN_INI, 2);
            //PASS.TK_SRV.byte.HB = DATAEE_ReadByte(NVM_ADD_TOKEN_INI);
            //PASS.TK_SRV.byte.LB = DATAEE_ReadByte(NVM_ADD_TOKEN_INI +1);
            
            SPIFlashReadArray((i+ NVM_ADD_ID) ,&PASS.Id_GW.byte.HB , 2);
            //NVMRead( PASS.Id_PAN.v, NVM_ADD_ID, 2);
            //PASS.Id_GW.byte.HB = DATAEE_ReadByte(NVM_ADD_ID);
            //PASS.Id_GW.byte.LB = DATAEE_ReadByte(NVM_ADD_ID +1);
            
            SPIFlashReadArray((i+ NVM_ADD_PASS_INI) ,(BYTE*)PASS.Pass_SRV , LENGTH_PASS);
            //for(i= 0; i<LENGTH_PASS; i++)
            //    PASS.Pass_SRV[i] = DATAEE_ReadByte(NVM_ADD_PASS_INI +i);            
            //PASS.Pass_SRV[i]='\0';
            PASS.Pass_SRV[LENGTH_PASS]='\0';
            
            return TRUE;
        }        
    }
    #endif
            
    return FALSE;
}

/*Establece variables ID y TOKEN inicial en NVM luego de GW_ASOCI                    
 * ID queda fijado.
 * TOKEN se configura como inicial ante un reboot solicitar aplicacion acceso.
 * Seteo PASS inicial para acceso al servidor en caso de reboot.
 * Seteo Variable de inicio.    
 *                                      */
void Set_ASO_NVM(void){
    DWORD i;
    BYTE Datum;
    //NVMRead(&p , NVM_ADD_INI, 1);
    
    i= sizeof(NVM_VALIDATION_STRUCT) + sizeof(AppConfig);
    
    #if defined(EEPROM_CS_TRIS)

        XEEReadArray((i+ NVM_ADD_INI) ,(BYTE*)&Datum , 1);
                        
        if (!( Datum & 0x01 ))        // verifico si ya fue asociado
        {   //El distositivo almacena datos de asociacion      
            XEEBeginWrite(i + NVM_ADD_TOKEN_INI);
            XEEWriteArray((BYTE*)&PASS.TK_SRV.byte.HB, 2);
            //NVMWrite(PASS.TOKEN.v , NVM_ADD_TOKEN_INI, 2);
            //DATAEE_WriteByte(NVM_ADD_TOKEN_INI,    PASS.TK_SRV.byte.HB); // TOKEN inicial ante reboot
            //DATAEE_WriteByte(NVM_ADD_TOKEN_INI +1, PASS.TK_SRV.byte.LB);  
            
            XEEBeginWrite(i + NVM_ADD_ID);
            XEEWriteArray((BYTE*)&PASS.Id_GW.byte.HB, 2);
            //NVMWrite(PASS.Id_PAN.v, NVM_ADD_ID, 2);
            //DATAEE_WriteByte(NVM_ADD_ID,    PASS.Id_GW.byte.HB);
            //DATAEE_WriteByte(NVM_ADD_ID +1, PASS.Id_GW.byte.LB);
            
            XEEBeginWrite(i + NVM_ADD_PASS_INI);
            XEEWriteArray((BYTE*)PASS.Pass_SRV, LENGTH_PASS);            
            //for(i= 0; i<LENGTH_PASS; i++)
            //    DATAEE_WriteByte(NVM_ADD_PASS_INI +i,PASS.Pass_SRV[i]);        

            //Seteo el registro de Asociacion
            XEEBeginWrite(i + NVM_ADD_INI);
            Datum=0x01;
            XEEWriteArray((BYTE*)&Datum, 1);            
            //NVMWrite( TRUE , NVM_ADD_INI, 1);
            //DATAEE_WriteByte(NVM_ADD_INI, 0x01);               
        }   
     
    #elif defined(SPIFLASH_CS_TRIS)
                
        SPIFlashReadArray((i+ NVM_ADD_INI) ,(BYTE*)&Datum , 1);
                        
        if (!( Datum & 0x01 ))        // verifico si ya fue asociado
        {   //El distositivo almacena datos de asociacion      
            SPIFlashBeginWrite(i + NVM_ADD_TOKEN_INI);
            SPIFlashWriteArray((BYTE*)&PASS.TK_SRV.byte.HB, 2);
            //NVMWrite(PASS.TOKEN.v , NVM_ADD_TOKEN_INI, 2);
            //DATAEE_WriteByte(NVM_ADD_TOKEN_INI,    PASS.TK_SRV.byte.HB); // TOKEN inicial ante reboot
            //DATAEE_WriteByte(NVM_ADD_TOKEN_INI +1, PASS.TK_SRV.byte.LB);  
            
            SPIFlashBeginWrite(i + NVM_ADD_ID);
            SPIFlashWriteArray((BYTE*)&PASS.Id_GW.byte.HB, 2);
            //NVMWrite(PASS.Id_PAN.v, NVM_ADD_ID, 2);
            //DATAEE_WriteByte(NVM_ADD_ID,    PASS.Id_GW.byte.HB);
            //DATAEE_WriteByte(NVM_ADD_ID +1, PASS.Id_GW.byte.LB);
            
            SPIFlashBeginWrite(i + NVM_ADD_PASS_INI);
            SPIFlashWriteArray((BYTE*)PASS.Pass_SRV, LENGTH_PASS);            
            //for(i= 0; i<LENGTH_PASS; i++)
            //    DATAEE_WriteByte(NVM_ADD_PASS_INI +i,PASS.Pass_SRV[i]);        

            //Seteo el registro de Asociacion
            SPIFlashBeginWrite(i + NVM_ADD_INI);
            Datum=0x01;
            SPIFlashWriteArray((BYTE*)&Datum, 1);            
            //NVMWrite( TRUE , NVM_ADD_INI, 1);
            //DATAEE_WriteByte(NVM_ADD_INI, 0x01);               
        }         

    #endif
    
    return;
}

//--------------- BUFFER de tramas ----------------------------

BOOL Put_Base(void){
    
    BYTE i=0;        
    //Data{ID_H,ID_L,Slot_H,Slot_L}
    while((Base_Slot[i][0]!= 0) && (i < (BASE_SLT_LNGTH-1)))
        i++;
    
    if ((i < (BASE_SLT_LNGTH-1)) && !Base_Slot[i][0]){
            
        Base_Slot[i][0]= bytesToWord(App.DatCtrl.Data[0],App.DatCtrl.Data[1]); //ID
        Base_Slot[i][1]= bytesToWord(App.DatCtrl.Data[2],App.DatCtrl.Data[3]); //SLOT            
        
    }else
        return FALSE;
    
    return TRUE;
} 

void Clear_Buffer_Norm_TCP(void){
    
    BYTE *p,*p1,i;
    BYTE j= 0; 
           
    while ((Buffer_Nrm.Buffer[ j +Buffer_Nrm.send_point +1 ].ID!= 0) && ((j+Buffer_Nrm.send_point+1)<BFFR_RX_LNGTH))
    {
        /*-HEADER-*/        
        p=  (BYTE *)&Buffer_Nrm.Buffer[j].ID;
        p1= (BYTE *)&Buffer_Nrm.Buffer[ j+ Buffer_Nrm.send_point + 1 ].ID; //Siguiente elemento sin enviar.                
        /*-DATA OF LAYER-*/  
                
        for(i=0;i<18;i++)
        {
            *p=*p1;
            p++;
            p1++;
        } //cubre inclusive Mesure
        
        /*
        BufferRX.Buffer[j].DateTx.Seg=BufferRX.Buffer[j+d].DateTx.Seg;
        BufferRX.Buffer[j].DateTx.Min=BufferRX.Buffer[j+d].DateTx.Min;
        BufferRX.Buffer[j].DateTx.Hou=BufferRX.Buffer[j+d].DateTx.Hou;
    
        BufferRX.Buffer[j].MedidaRE.DateMesu.Hou=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Hou; //ultima medida
        BufferRX.Buffer[j].MedidaRE.DateMesu.Day=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Day; //ultima medida
        BufferRX.Buffer[j].MedidaRE.DateMesu.Mon=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Mon; //ultima medida
        
        BufferRX.Buffer[j].MedidaRE.Mesure=BufferRX.Buffer[j+d].MedidaRE.Mesure; //ultima medida
        
        BufferRX.Buffer[j].Slot=BufferRX.Buffer[j+d].Slot;
                          
        //p1=&BufferRX[j+10].DateTx.Seg; 
        //p=&BufferRX[j].DateTx.Seg; //GW_RTCC.Seg;

        p=App1.MedidaDif;
        p1=BufferRX.Buffer[j].MedidaDif;
        */
        
        /*------------Todas las medidas---------------------------------------*/
        Buffer_Nrm.Buffer[j].MedidaDif.Index= Buffer_Nrm.Buffer[j+Buffer_Nrm.send_point+1].MedidaDif.Index;
        
        p=  Buffer_Nrm.Buffer[j].MedidaDif.MedidaD;
        p1= Buffer_Nrm.Buffer[j+Buffer_Nrm.send_point+1].MedidaDif.MedidaD;

        for (i = 0; (i < TOMAS)  && (i <= Buffer_Nrm.Buffer[j].MedidaDif.Index); i++) 
        {
            *p= *p1;
            p++;
            p1++;
        }
        
        Buffer_Nrm.Buffer[j+Buffer_Nrm.send_point+1].ID=0; //BLANQUEO  DE SEGURIDAD!!!         
        
        j++;// incremento el buffer.
    }
    
    Buffer_Nrm.point=j;//apunto al proximo elemento libre en el BuffeRX
    
    for (i=j; i<BFFR_RX_LNGTH ;i++)
        Buffer_Nrm.Buffer[i].ID= 0; //BLANQUEO  DE SEGURIDAD!!!
    
    Buffer_Nrm.send_point= 0;
    
    Buff_NRM_MW= FALSE;
    
    return;
}

/*******************************************************************************
 * Control de buffer generico tanto para MIWI y MB
 * para escribir tramas de TIPO Control
 * Argumentos :
 * *p : punero a estructura Buffer.
 * *t : punero a variable de intercambio.
 ******************************************************************************/


BYTE Put_Buffer_Ctrl_TCP(void){
    
    /*Punteros a Byte*/       
    BYTE *a,*b;    
    BYTE j,l,k;
    //@TODO cuidadao con K
    //k= Indx_from_IdPan(highByte(App1.ID));
    j= Ctrl_Buffer_R[k].index; //ubico en lugar libre
    Ctrl_Buffer_R[k].Ctrl_Base[j].Send=    FALSE;
    Ctrl_Buffer_R[k].Ctrl_Base[j].ID=      App1.ID;
    Ctrl_Buffer_R[k].Ctrl_Base[j].TKSRV=   App1.TOKEN;
    Ctrl_Buffer_R[k].Ctrl_Base[j].TKMM=    App1.TKMM;
    Ctrl_Buffer_R[k].Ctrl_Base[j].SubTipo= App1.SubTipo;
    /*Punteros a Byte*/       
    b=&App1.DatCtrl.Index;
    a=&(Ctrl_Buffer_R[k].Ctrl_Base[j].DatCtrl.Index);
    
    l= *b+1;
    
    for( j=0; j<=l; j++)
    {
        *a= *b;
        b++;
        a++;
    }
    
    if (Ctrl_Buffer_R[k].index < BS_CTRL_LNGTH-1)
        Ctrl_Buffer_R[k].index++;
    else
        Ctrl_Buffer_R[k].index=0;
    
    return(Ctrl_Buffer_R[k].index);
}

/******************************************************************************
    Actualiza el buffer de Ctrl Q o R segun lo operado anteriormente de setear 
    Sem_MIWI_DONE.
 ******************************************************************************/
BOOL UpDate_BufferCtrl(Ctrl_Buffer_t *q){
    
    
    q->index_to_send++;                    //el indice apunta a Trama enviada.
    if (q->index_to_send++ == BS_CTRL_LNGTH)
        q->index_to_send= 0;
    
    if (q->index == q->index_to_send)      // indice de colocacion igual al enviado
        return (FALSE);                    // No hay mas tramas
    /*    
    if ((q->index > q->index_send) && (q->index - q->index_send)>0) 
    {
        return(TRUE);
    }else
        if(q->index < q->index_send) // pego la vuelta arribados, enviados apunto superior. 
    {
        return(TRUE);
    }
    */
    return (TRUE);                        //hay mas tramas
}

BOOL Clr_Buffer_Ctrl_TCP(Ctrl_Buffer_t* p){
    
    BYTE j,k;
    //@TODO implementar la index
    //k= Indx_IdPan_from_AddMB();
    //->Ctrl_Base[i].DatCtrl.Index;
    for(j=0; j< k; j++)
    p++; 
    
    j= p->index_to_send;
    
    //--------blanqueo-----------------------------
    if ( p->Ctrl_Base[j].Send )   //consulta al bufferQ para su ID
    {
        p->Ctrl_Base[j].ID=   0;
        p->Ctrl_Base[j].Send= TRUE;            
        
    }
    return(UpDate_BufferCtrl(p)); //arroja si existen mas tramas sin envira                
}

/*------------RUTINAS PARA APPs -----------------------------*/
void Search_Control_TCP(void){
               
    BYTE i=0;           
    // Uso indice ID=0 indica elemento vector sin uso.
    while(Base_Slot[i][0]!=App1.ID && i<(BASE_SLT_LNGTH-1) && Base_Slot[i][0]!=0 ) 
        i++;
    
    if (Base_Slot[i][0]!=App1.ID) { // descarto el elemento actual si llega al final
        App1.Slot=0;// no habra cambios
        return;    
    }
        
    App1.Slot=Base_Slot[i][1]; // elemento actual filtrado

    // limpio la base y ordeno
        
    while(i<49){ //|| BaseSlot[i][1]!=0) { // caso de ser cualquiera

        Base_Slot[i][0]=Base_Slot[i+1][0];
        Base_Slot[i][1]=Base_Slot[i+1][1];        
        i++;
    }
    Base_Slot[i][0]=0;          // caso de ser el ultimo elemento 49
    Base_Slot[i][1]=0;
        
    return;    
} 

BOOL APP_Ctrl_TCP(void){
    //@TODO (proxima version) implementar
    switch (App.SubTipo & 0xf0)
    {
        case Ctrl_NET:
            /* 00-09: De RED:Cambio de Slot, RFD, FFD.*/
           //SlotFromRx_To_App();
            switch (App.SubTipo & 0x0F)
            {
                case Ctrl_NET_SLOT:
                    Put_Base();
                  break;
                case Ctrl_NET_CHG_PAN:
                    
                  break;
                case Ctrl_NET_CHG_CHN:
                    
                  break;
                case Ctrl_NET_CHG_BND:
                    
                  break;  
                case Ctrl_NET_CHG_PTX:
                    
                  break; 
                  
                default:
                    return FALSE;
            }        
         break;
        case Ctrl_VIEW:
           /* 10-19: De Monitoreo Interno: temperatura, humedad,
            scaneo, consumo, generacion solar, velociadad viento.*/
            switch (App.SubTipo & 0x0F)
            {
                case Ctrl_VIEW_TEMP:
                    
                  break;
                case Ctrl_VIEW_SCN:
                    
                  break;
                case Ctrl_VIEW_PWR:
                   
                  break;
                case Ctrl_VIEW_GNS:
                    
                  break;
                case Ctrl_VIEW_WND:
                    
                  break;  
                default:
                    return FALSE ;
            }
         break;
        case Ctrl_IN:
           /* 20-29: De Control Interno: reset POW, reset blank,
            cambio PAN, cambio de canal, cambio de banda, control potencia, T de sleep*/
            switch (App.SubTipo & 0x0F)
            {
                case Ctrl_IN_POR:
                   
                  break;
                case Ctrl_IN_RSTM:
                    
                  break;                                           
                case Ctrl_IN_T_SLP:
                    
                  break;  
                default:
                    return FALSE;
            }

         break;
        case Ctrl_OUT:
           switch (App.SubTipo & 0x0F)
            {
                case Ctrl_OUT_IO:
                    
                  break;
                case Ctrl_OUT_USB:
                    
                  break;
                case Ctrl_OUT_CAN:
                    
                  break;
                case Ctrl_OUT_SPI:
                    
                  break;
                case Ctrl_OUT_I2C:
                    
                  break;  
                case Ctrl_OUT_USART:
                    
                  break;
                case Ctrl_OUT_DSPLY:
                    
                  break;  
                default:
                    return FALSE;
            }
         break;
        case Ctrl_DAT:
           switch (App.SubTipo & 0x0F)
            {
                case Ctrl_DAT_EEP:
                    
                  break;
                case Ctrl_DAT_SST:
                    
                  break;
                case Ctrl_DAT_FLH:
                    
                  break;
              
                default:
                    return FALSE;
            }
         break;
        case Ctrl_PRG:
           switch (App.SubTipo & 0x0F)
            {
                case Ctrl_PRG_PTR:
                    
                  break;           
                default:
                    return FALSE;
            }
         break;
        
        default:
            return FALSE;
    }     
         /* Sub codigos App.Tipo>=5;
            * 00-09: De RED:Cambio de Slot, RFD, FFD.
            * 10-19: De Monitoreo Interno: temperatura, humedad, scaneo, consumo, generacion solar, velociadad viento.
            * 20-29: De Control Interno: reset POW, reset blank, cambio PAN, cambio de canal, cambio de banda, control potencia, T de sleep.
            * 30-39: De Control Externo: Control IOs, USB, CAN, USART,Display.
            * 40-49: De Control Datos: Escritura de Variable, Lectura de Variable.
            * 50-59: De Control Programa: Control de flujo de programa(vector de punteros a funciones).
            * 60-69:
            * 70-79:
            * 80-89:
            * 90-99:
         */
    
    return TRUE;
}


/*****************************************************
*  Busca Token para grupo siguiente que 
 * complete el Buffer de norm, viene en la 
 * respuesta del servidor en NORM
******************************************************/
WORD Get_TK_BufferTK_TCP(BYTE id){

    BYTE  i;
    WORD tk;
    for(i=0; i<N_PAN ; i++)
    { 
        if (TK_Buffer_Nrm[i].ID == id )
        {   
            TK_Buffer_Nrm[i].Base.Tables[ TK_Buffer_Nrm[i].Base.index_to_send ].Send= TRUE;
                        
            tk= TK_Buffer_Nrm[i].Base.Tables[ TK_Buffer_Nrm[i].Base.index_to_send ].TKSRV;
            
            TK_Buffer_Nrm[i].Base.index_to_send++;
            if (TK_Buffer_Nrm[i].Base.index_to_send == BS_TK_LNGTH)
                TK_Buffer_Nrm[i].Base.index_to_send= 0;
            
            break;
        }
    }    
    
    return(tk);
}
/*****************************************************
*  Coloca Token para grupo siguiente que 
 * complete el Buffer de norm, viene en la 
 * respuesta del servidor en NORM
******************************************************/
void Put_TK_BufferTK_TCP(void){
    
    BYTE  i;    
    App.ID>>=8;
    
    for(i=0; i<N_PAN ; i++)
    { 
        if (TK_Buffer_Nrm[i].ID == (App.ID & 0xff))
        {   
            TK_Buffer_Nrm[i].Base.Tables[ TK_Buffer_Nrm[i].Base.index ].Send= FALSE;
                        
            TK_Buffer_Nrm[i].Base.Tables[ TK_Buffer_Nrm[i].Base.index ].TKSRV= App.TOKEN;
            
            TK_Buffer_Nrm[i].Base.index++;
            if (TK_Buffer_Nrm[i].Base.index == BS_TK_LNGTH)
                TK_Buffer_Nrm[i].Base.index= 0;
            
            break;
        }
    }    
        
    return;
}


/********************************************************
 busco el indice en el buffer de crtl para el PAN segun su 
 direccion de esclavo MB
 ********************************************************/
BYTE Indx_IdPan_from_AddMB(void){
            
    BYTE j=0;
    
    while ( j< TBL_ID_MB_LNGTH )
    {
        if ( Table_ID_AddMB.Table[j].Act && (Table_ID_AddMB.Table[j].Add_MB == App1.Add[0])) //N_PAN
        
            return (Table_ID_AddMB.Table[j].Inx);                                
        j++;
    }
    
    return(0xff);
}   
BYTE Indx_from_IdPan(BYTE id){
            
    BYTE j=0;
    
    while ( j< TBL_ID_MB_LNGTH )
    {
        if ( Table_ID_AddMB.Table[j].Act && (Table_ID_AddMB.Table[j].ID == id)) //N_PAN
        
            return(Table_ID_AddMB.Table[j].Inx);                                
        j++;
    }
    
    return(0xff);
}



BOOL Put_Buffer_Aso_TCP(void){ // lo coloca APP TCP PARa que MIWI o MB opere
    BYTE j=0;  
    while(j<BUFFER_LNGTH){ 
        
        if (!Buffer_Aso[j].Rece && Buffer_Aso[j].Send && (Buffer_Aso[j].Sec_GW == App.Sec_GW) && (Buffer_Aso[j].Element.ID_AUI == App.ID) )
        {
            Buffer_Aso[j].Rece=         TRUE;
            Buffer_Aso[j].ID_AUT=       App.ID_AUT; //id que llega del servidor, el elemen.ID es la MAC queda inalterado en el RFD pero autenticado en el servidor
            Buffer_Aso[j].Element.TK=   App.TOKEN;  //TK asociado acceso futuro al servidor 
            Buffer_Aso[j].Element.Slot= App.Slot;   //slot asociado
            
            return(TRUE);
        }       
        j++;        
    }
    //paquete duplicado o sin coincidencia
    return(FALSE);
}

BOOL Put_Buffer_PN_TCP(void){ // lo coloca APP TCP PARa que MIWI o MB opere
    BYTE j=0;  
    while(j<N_PAN)
    {         
        if (!Buffer_PN[j].Rece && Buffer_PN[j].Send && (Buffer_PN[j].Sec_GW == App.Sec_GW) && (Buffer_PN[j].Element.ID_AUI == App.ID) )
        {
            Buffer_PN[j].Rece=         TRUE;
            Buffer_PN[j].ID_AUT=       App.ID_AUT; //id que llega del servidor, el elemen.ID es la MAC queda inalterado en el RFD pero autenticado en el servidor
            Buffer_PN[j].Element.TK=   App.TOKEN;  //TK asociado acceso futuro al servidor 
                     
            return(TRUE);
        }       
        j++;        
    }
    //paquete duplicado o sin coincidencia
    return(FALSE);
}

/* bool Get_Buffer_MIWI(void)
 *  para casos de control no se usa en el primer proyecto
 * 
 *  */
/*
bool Get_Buffer_MIWI(void){// lo usa TCP para operar y colocar en su TCP TX
    
    uint8_t i,q=0;
    static uint8_t j=0;
    uint8_t *p=NULL,*p1=NULL;
    
    
    while  (q<Bufferlength){
    
        if  (!(*p1)){            //Sent=0
            
            App.ID=Buffer[q].Element.ID;
            p=&App.Tipo;
            p1=&Buffer[0].Element.Tipo;

            for(i=0;i<6;i++){                    
                *p=*p1;
                p++;
                p1++;
            }

            BufferRX.Buffer[j].Slot=App1.Slot;
            
            p=&App1.DateTx.Seg; 
            p1=&BufferRX.Buffer[j].DateTx.Seg; //GW_RTCC.Seg;

            for(i=0;i<6;i++){ 
                *p=*p1;
                p++;
                p1++;
            }
            
            p=&App1.MedidaRE.DateMesu.Hou;
            p1 =&BufferRX.Buffer[j].MedidaRE.DateMesu.Hou;
            
            for (i = 0; i <3 ; i++) {
                    *p= *p1 ;
                    p++;
                    p1++;
            }        

            for (i = 0; i < Tomas; i++) {

                App1.MedidaDif[i]=BufferRX.Buffer[j].MedidaDif[i];

            }
            
            
            return(true);
        }
       p1++;
    }   
    return(false);
} 

*/



BOOL Get_Buffer_Aso_TcpIp(void){
    
    BYTE i=0;
    while  (i<BUFFER_LNGTH)
    {    
        if  (!Buffer_Aso[i].Send && (Buffer_Aso[i].Element.ID_AUI!=0) )
        { //Sent=0            
            App.ID= Buffer_Aso[i].Element.ID_AUI;      // MAC
            //App.Tipo=2;
            Buffer_Aso[i].Sec_GW= GW_Sec_TCP; 
            Buffer_Aso[i].Send=   TRUE; 
            return(TRUE);
        }
        i++;
    }   
    return(FALSE);
}
 
BOOL Get_Buffer_PN_TcpIp(void){
    
    BYTE i=0;
    while  (i<N_PAN)
    {    
        if  (!Buffer_PN[i].Send && (Buffer_PN[i].Element.ID_AUI!=0) )
        { //Sent=0            
            App.ID= Buffer_PN[i].Element.ID_AUI;      // MAC            
            Buffer_PN[i].Sec_GW= GW_Sec_TCP; 
            Buffer_PN[i].Send=   TRUE; 
            return(TRUE);
        }
        i++;
    }   
    return(FALSE);
}